INTREPID_BUILD_PATH=intrepid_build
INTREPID_DEBUG_BUILD_PATH=intrepid_build_debug
INTREPID_SRC_PATH=intrepid
INTREPYD_SRC_PATH=intrepyd
APP_SRC_PATH=app
WINDOWS_BUILD_MACHINE=roberto.bruttomesso@gmail.com@fanlessbeast

all: initialize_intrepid build_intrepid all_linters all_tests

all_linux: build_and_test_linux

all_win: build_and_test_win

all_linters: linter_python

all_tests: tests_cpp tests_python

bootstrap_linux:
	@sudo apt update && sudo apt install -y clang ninja-build cmake python3.9-dev python3.9-venv ccache pip3
	@python3.9 -m venv venv
	@source venv/bin/activate
	@pip3 install -r requirements
	@echo '*** Add the following to your shell rc e.g., .zshrc ***'
	@echo 'export PYTHONPATH="/path/to/this/directory:$PYTHONPATH"'

bootstrap_mac:
	@brew install python@3.9
	@brew install swig@3
	@python3.9 -m venv venv
	@source venv/bin/activate
	@pip3 install -r requirements
	@echo '*** Add the following to your shell rc e.g., .zshrc ***'
	@echo 'export PYTHONPATH="/path/to/this/directory:$PYTHONPATH"'

regenerate_wrapper:
	@swig -version
	@echo "*** Make sure to use SWIG 3.0.12 ***"
	@cd $(INTREPID_SRC_PATH) && \
		swig -python src/api/Intrepid.i

initialize_intrepid:
	@echo "# Initializing repository"
	@rm -fr $(INTREPID_BUILD_PATH)
	@mkdir $(INTREPID_BUILD_PATH)
	@cp $(INTREPID_SRC_PATH)/src/api/api.py $(INTREPYD_SRC_PATH)

build_intrepid:
	@echo "# Building intrepid backend"
	@rm -fr intrepid/deps
	@tar xzf intrepid/deps.tgz && \
	  mv deps $(INTREPID_SRC_PATH) && \
		cd $(INTREPID_BUILD_PATH) && \
		export CC=/usr/bin/clang && \
		export CXX=/usr/bin/clang++ && \
		cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ../$(INTREPID_SRC_PATH) && \
		ninja && \
		rm -fr ../$(INTREPID_SRC_PATH)/deps

build_intrepid_debug:
	@echo "# Building intrepid backend debug"
	@rm -fr $(INTREPID_DEBUG_BUILD_PATH)
	@mkdir $(INTREPID_DEBUG_BUILD_PATH)
	@rm -fr intrepid/deps
	@tar xzf intrepid/deps.tgz && \
	  mv deps $(INTREPID_SRC_PATH) && \
		cd $(INTREPID_DEBUG_BUILD_PATH) && \
		export CC=/usr/bin/clang && \
		export CXX=/usr/bin/clang++ && \
		cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ../$(INTREPID_SRC_PATH) && \
		ninja && \
		rm -fr ../$(INTREPID_SRC_PATH)/deps

linter_python:
	@pylint intrepyd

tests_cpp: $(INTREPID_BUILD_PATH)/test/unittests
	@echo "# Testing intrepid backend"
	@cd $(INTREPID_BUILD_PATH) && \
		test/unittests -v

tests_python:
	@echo "# Testing intrepyd and apis"
	@cd $(INTREPYD_SRC_PATH) && ln -sf ../$(INTREPID_BUILD_PATH)/_api.so .
	@python3 -m unittest discover -v

coverage_python:
	@echo "# Testing intrepyd and apis"
	@cd $(INTREPYD_SRC_PATH) && ln -sf ../$(INTREPID_BUILD_PATH)/_api.so .
	@coverage run -m unittest discover && coverage report && coverage html

install_intrepyd:
	@echo "# Locally installing intrepyd"
	@python3 setup.py install --user

build_and_test_osx: build_intrepid tests_cpp tests_python
	@cp $(INTREPID_BUILD_PATH)/_api.so libs/osx/

build_and_test_win:
	@echo "# Building and testing under windows 10"
	@ssh $(WINDOWS_BUILD_MACHINE) "devel\intrepyd\build_and_test_win.bat"
	@echo "# Updating lib"
	@scp $(WINDOWS_BUILD_MACHINE):devel/intrepyd/$(INTREPID_BUILD_PATH)/Release/_api.pyd libs/windows

build_and_test_linux:
	@echo "# Building and testing under linux"
	@make -f Makefile.docker docker_build
	@echo "# Updating lib"
	@make -f Makefile.docker docker_run
	@docker exec intrepid find -name _api.so -exec cp {} . \;
	@docker cp intrepid:/home/intrepid/_api.so libs/linux64/_api.so
	@docker container stop intrepid
	@make -f Makefile.docker docker_prune

build_docs:
	@echo "# Generate docs"
	@[ -d docs ] || mkdir docs
	@pdoc3 -f -o docs intrepyd
	@rm -fr docs/intrepyd/tests

release_intrepyd_pip:
	@echo "# Packaging intrepyd"
	@rm -fr dist build intrepyd.egg-info
	@python3 -m pip install --user --upgrade setuptools wheel
	@python3 setup.py sdist
	@twine upload --repository pypi dist/*
	@rm -fr dist build intrepyd.egg-info

release_intrepid_github:
	@echo "# Releasing intrepid on github"
	@./release_intrepid_github.sh
