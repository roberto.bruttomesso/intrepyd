@echo off
cd devel\intrepyd
echo Updating repo
git.exe pull
rmdir /S /Q intrepid\deps
echo Adding env variables
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
echo Unzipping deps
cd intrepid
tar.exe xzf deps.tgz
cd ..
echo Preparing compilation dirs
rmdir intrepid_build /s /q
mkdir intrepid_build
cd intrepid_build
cmake -G "Visual Studio 16 2019" ../intrepid
msbuild Intrepid.sln /property:Configuration=Release
copy Release\_api.pyd ..
cd ..
copy libs\windows\libz3.dll .
python.exe -m unittest discover -v
