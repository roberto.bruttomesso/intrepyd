/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "deps/gtest/include/gtest/gtest.h"

#include "src/net/Net.h"

using namespace net;

namespace test {

class MySeqNet : public Net {
public:
    MySeqNet(unsigned id) : Net(id) {}
};

class MyComNet : public Net {
public:
    MyComNet(unsigned id) : Net(id) {}
};

template<class Net>
void
testEqual() {
    Net myNet1(0);
    Net myNet2(0);
    ASSERT_EQ(myNet1, myNet2);
}

TEST(NetTest, TestEqual) {
    testEqual<MyComNet>();
    testEqual<MySeqNet>();
}

template<class Net>
void
testDiff() {
    Net myNet1(0);
    Net myNet2(1);
    ASSERT_NE(myNet1, myNet2);
}

TEST(NetTest, TestDiff) {
    testDiff<MyComNet>();
    testDiff<MySeqNet>();
}

}  // namespace test
