/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "deps/gtest/include/gtest/gtest.h"

#include "src/net/Z3SeqNetStore.h"
#include "src/net/Z3ComNetStore.h"
#include "test/fixture/Z3Fixture.h"

using namespace net;

namespace test {

class Z3NetStoreTest : public Z3Fixture {
};

/*
 * True, False, Undef
 */
template<class Store>
void
testTrueFalseUndef(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;

    Net netTrue = store.mkTrue();
    Net netFalse = store.mkFalse();
    ASSERT_TRUE(store.isTrue(netTrue));
    ASSERT_TRUE(store.isFalse(netFalse));
    ASSERT_FALSE(store.isTrue(netFalse));
    ASSERT_FALSE(store.isFalse(netTrue));
}

TEST_F(Z3NetStoreTest, TestTrueFalseUndef) {
    testTrueFalseUndef<Z3ComNetStore>(getContext());
    testTrueFalseUndef<Z3SeqNetStore>(getContext());
}

/*
 * Variable creation works fine
 */
template<class Store>
void
testVariable(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;

    Net i1    = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Net i2    = store.mkVariable("i2", NetTypeInfo::mkBooleanType());
    Net i1Dup = store.mkVariable("i1", NetTypeInfo::mkBooleanType());

    ASSERT_NE(i1, i2);
    ASSERT_EQ(i1, i1Dup);
}

TEST_F(Z3NetStoreTest, TestVariable) {
    testVariable<Z3ComNetStore>(getContext());
    testVariable<Z3SeqNetStore>(getContext());
}

/*
 * "is" functions are correct
 */
template<class Store>
void
testIs(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;

    Net i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Net i2 = store.mkVariable("i2", NetTypeInfo::mkBooleanType());

    ASSERT_TRUE(store.isVariable(i1));
    ASSERT_TRUE(store.isVariable(i2));
    ASSERT_TRUE(store.isNot(store.mkNot(i1)));
    ASSERT_TRUE(store.isAnd(store.mkAnd(i1, i2)));
    ASSERT_TRUE(store.isOr(store.mkOr(i1, i2)));
    ASSERT_TRUE(store.isIff(store.mkIff(i1, i2)));
    // ASSERT_TRUE(store.isXor(store.mkXor(i1, i2)));
    // Because of Z3 simplifications, xor is rewritten
    ASSERT_TRUE(store.isIff(store.mkXor(i1, i2)));
}

TEST_F(Z3NetStoreTest, TestIs) {
    testIs<Z3ComNetStore>(getContext());
    testIs<Z3SeqNetStore>(getContext());
}

/*
 * Children are retrieved correctly
 */
template<class Store>
void
testChildren(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;

    ASSERT_EQ(0u, store.getNofChildren(store.mkTrue()));
    ASSERT_EQ(0u, store.getNofChildren(store.mkFalse()));

    Net i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Net i2 = store.mkVariable("i2", NetTypeInfo::mkBooleanType());

    ASSERT_EQ(0u, store.getNofChildren(i1));

    Net notOp = store.mkNot(i1);
    ASSERT_EQ(1u, store.getNofChildren(notOp));
    ASSERT_EQ(i1, store.getChild(notOp, 0));

    Net andOp = store.mkAnd(i1, i2);
    ASSERT_EQ(2u, store.getNofChildren(andOp));
    ASSERT_EQ(i1, store.getChild(andOp, 0));
    ASSERT_EQ(i2, store.getChild(andOp, 1));
}

TEST_F(Z3NetStoreTest, TestChildren) {
    testChildren<Z3ComNetStore>(getContext());
    testChildren<Z3SeqNetStore>(getContext());
}

/*
 * Numbers work correctly
 */
template<class Store>
void testNumbers(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;

    Net num1 = store.mkNumber("1/2", NetTypeInfo::mkRealType());
    ASSERT_TRUE(store.isNumber(num1));
    ASSERT_FALSE(store.isVariable(num1));
    ASSERT_EQ("(/ 1.0 2.0)", store.toString(num1));

    Net num2 = store.mkNumber("0.3", NetTypeInfo::mkRealType());
    ASSERT_TRUE(store.isNumber(num2));
    ASSERT_EQ("(/ 3.0 10.0)", store.toString(num2));

    Net num3 = store.mkNumber("13", NetTypeInfo::mkIntType(8));
    ASSERT_TRUE(store.isNumber(num3));
    ASSERT_EQ("#x0d", store.toString(num3));

    Net num4 = store.mkNumber("22", NetTypeInfo::mkIntType(16));
    ASSERT_TRUE(store.isNumber(num4));
    ASSERT_EQ("#x0016", store.toString(num4));

    Net num5 = store.mkNumber("533", NetTypeInfo::mkIntType(32));
    ASSERT_TRUE(store.isNumber(num5));
    ASSERT_EQ("#x00000215", store.toString(num5));

    Net num6 = store.mkNumber("0.3", NetTypeInfo::mkFloatType(64));
    ASSERT_TRUE(store.isNumber(num6));
    ASSERT_EQ("(fp #b0 #b01111111101 #x3333333333333)", store.toString(num6));

    Net num7 = store.mkNumber("1/2", NetTypeInfo::mkFloatType(64));
    ASSERT_TRUE(store.isNumber(num7));
    ASSERT_EQ("(fp #b0 #b01111111110 #x0000000000000)", store.toString(num7));

    Net num8 = store.mkNumber("123.123",
                              NetTypeInfo::mkFloatType(32));
    ASSERT_TRUE(store.isNumber(num8));
    ASSERT_EQ("(fp #b0 #x85 #b11101100011111011111001)",
              store.toString(num8));

    Net num9 = store.mkNumber("(fp #b0 #x85 #b11101100011111011111001)",
                              NetTypeInfo::mkFloatType(32));
    ASSERT_TRUE(store.isNumber(num9));
    ASSERT_EQ("(fp #b0 #x85 #b11101100011111011111001)",
              store.toString(num9));

    Net num10 = store.mkNumber("(_ +zero 8 24)",
                              NetTypeInfo::mkFloatType(32));
    ASSERT_TRUE(store.isNumber(num10));
    ASSERT_EQ("(_ +zero 8 24)", store.toString(num10));

    Net num11 = store.mkNumber("(_ -zero 8 24)",
                              NetTypeInfo::mkFloatType(32));
    ASSERT_TRUE(store.isNumber(num11));
    ASSERT_EQ("(_ -zero 8 24)", store.toString(num11));

    Net num12 = store.mkNumber("-123.123",
                               NetTypeInfo::mkFloatType(32));
    ASSERT_TRUE(store.isNumber(num12));
    ASSERT_EQ("(fp #b1 #x85 #b11101100011111011111001)",
              store.toString(num12));

    Net num13 = store.mkNumber("123", NetTypeInfo::mkInfIntType());
    ASSERT_TRUE(store.isNumber(num13));
    ASSERT_FALSE(store.isVariable(num13));
    ASSERT_EQ("123", store.toString(num13));

    Net num14 = store.mkNumber("-123", NetTypeInfo::mkInfIntType());
    ASSERT_TRUE(store.isNumber(num14));
    ASSERT_FALSE(store.isVariable(num14));
    ASSERT_EQ("(- 123)", store.toString(num14));
}

TEST_F(Z3NetStoreTest, TestNumbers) {
    testNumbers<Z3ComNetStore>(getContext());
    testNumbers<Z3SeqNetStore>(getContext());
}

/*
 * Test for mkNet
 */
template<class Store>
void
testMkNet(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net in1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Net in2 = store.mkVariable("i2", NetTypeInfo::mkBooleanType());

    Net not_ = store.mkNot(in1);
    Net and_ = store.mkAnd(in1, in2);
    Net or_ = store.mkOr(in1, in2);
    Net xor_ = store.mkXor(in1, in2);
    Net iff_ = store.mkIff(in1, in2);

    ASSERT_EQ(not_, store.mkNet(NetKind::Not, in1));
    ASSERT_EQ(and_, store.mkNet(NetKind::And, in1, in2));
    ASSERT_EQ(or_, store.mkNet(NetKind::Or, in1, in2));
    ASSERT_EQ(xor_, store.mkNet(NetKind::Xor, in1, in2));
    ASSERT_EQ(iff_, store.mkNet(NetKind::Iff, in1, in2));

    ASSERT_EQ(NetKind::Not, std::get<0>(store.getNetKind(not_)));
    ASSERT_EQ(NetKind::And, std::get<0>(store.getNetKind(and_)));
    ASSERT_EQ(NetKind::Or, std::get<0>(store.getNetKind(or_)));
    ASSERT_EQ(NetKind::Iff, std::get<0>(store.getNetKind(iff_)));
    // ASSERT_EQ(NetKind::Xor, std::get<0>(store.getNetKind(xor_)));
    // Because of Z3 simplifications, iff is rewritten into xor
    ASSERT_EQ(NetKind::Iff, std::get<0>(store.getNetKind(xor_)));
}

TEST_F(Z3NetStoreTest, TestMkNet) {
    testMkNet<Z3ComNetStore>(getContext());
    testMkNet<Z3SeqNetStore>(getContext());
}

/*
 * Test for comparisons
 */
template<class Store>
void
testMkTheoryNets(Z3_context context, NetTypeInfo type) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net in1 = store.mkVariable("i1", type);
    Net in2 = store.mkVariable("i2", type);

    Net add = store.mkAdd(in1, in2);
    ASSERT_EQ(add, store.mkNet(NetKind::Add, in1, in2));
    ASSERT_EQ(NetKind::Add, std::get<0>(store.getNetKind(add)));
    ASSERT_TRUE(store.isAdd(add));

    Net sub = store.mkSub(in1, in2);
    ASSERT_EQ(sub, store.mkNet(NetKind::Sub, in1, in2));
    // Z3 simplifies (sub x) into (add neg x)
    ASSERT_EQ(NetKind::Add, std::get<0>(store.getNetKind(add)));
    ASSERT_TRUE(store.isAdd(add));

    Net min = store.mkMinus(in1);
    ASSERT_EQ(min, store.mkNet(NetKind::Minus, in1));
    ASSERT_EQ(NetKind::Minus, std::get<0>(store.getNetKind(min)));
    ASSERT_TRUE(store.isMinus(min));

    Net mul = store.mkMul(in1, in2);
    ASSERT_EQ(mul, store.mkNet(NetKind::Mul, in1, in2));
    ASSERT_EQ(NetKind::Mul, std::get<0>(store.getNetKind(mul)));
    ASSERT_TRUE(store.isMul(mul));

    Net div = store.mkDiv(in1, in2);
    ASSERT_EQ(div, store.mkNet(NetKind::Div, in1, in2));
    ASSERT_EQ(NetKind::Div, std::get<0>(store.getNetKind(div)));
    ASSERT_TRUE(store.isDiv(div));

    Net leq = store.mkLeqFloat(in1, in2);
    ASSERT_EQ(leq, store.mkNet(NetKind::LeqFloat, in1, in2));
    ASSERT_EQ(NetKind::LeqFloat, std::get<0>(store.getNetKind(leq)));
    ASSERT_TRUE(store.isLeq(leq));

    Net num = store.mkNumber("15.0", type);
    Net inp = store.mkVariable("carSpeed", type);
    Net geq = store.mkGeqFloat(num, inp);
    ASSERT_TRUE(store.isLeq(geq));
}

TEST_F(Z3NetStoreTest, TestMkTheoryNets) {
    testMkTheoryNets<Z3ComNetStore>(getContext(), NetTypeInfo::mkFloatType(16));
    testMkTheoryNets<Z3ComNetStore>(getContext(), NetTypeInfo::mkFloatType(32));
    testMkTheoryNets<Z3ComNetStore>(getContext(), NetTypeInfo::mkFloatType(64));
}

template<class Store>
void
testGetBit01(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net num = store.mkNumber("1", NetTypeInfo::mkIntType(8));
    Net bit = store.mkGetBit(num, 0);
    ASSERT_TRUE(store.isTrue(bit));
    for (unsigned i = 1; i < 8; i++) {
        Net bit = store.mkGetBit(num, i);
        ASSERT_TRUE(store.isFalse(bit));
    }
}

template<class Store>
void
testGetBit02(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net num = store.mkNumber("128", NetTypeInfo::mkIntType(8));
    Net bit = store.mkGetBit(num, 7);
    ASSERT_TRUE(store.isTrue(bit));
    for (unsigned i = 0; i < 7; i++) {
        Net bit = store.mkGetBit(num, i);
        ASSERT_TRUE(store.isFalse(bit));
    }
}

template<class Store>
void
testGetBit03(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net num = store.mkNumber("68", NetTypeInfo::mkIntType(8));
    Net bit = store.mkGetBit(num, 0);
    ASSERT_TRUE(store.isFalse(bit));
    bit = store.mkGetBit(num, 1);
    ASSERT_TRUE(store.isFalse(bit));
    bit = store.mkGetBit(num, 2);
    ASSERT_TRUE(store.isTrue(bit));
    bit = store.mkGetBit(num, 3);
    ASSERT_TRUE(store.isFalse(bit));
    bit = store.mkGetBit(num, 4);
    ASSERT_TRUE(store.isFalse(bit));
    bit = store.mkGetBit(num, 5);
    ASSERT_TRUE(store.isFalse(bit));
    bit = store.mkGetBit(num, 6);
    ASSERT_TRUE(store.isTrue(bit));
    bit = store.mkGetBit(num, 7);
    ASSERT_TRUE(store.isFalse(bit));
}

TEST_F(Z3NetStoreTest, TestGetBit) {
    testGetBit01<Z3ComNetStore>(getContext());
    testGetBit01<Z3SeqNetStore>(getContext());
    testGetBit02<Z3ComNetStore>(getContext());
    testGetBit02<Z3SeqNetStore>(getContext());
    testGetBit03<Z3ComNetStore>(getContext());
    testGetBit03<Z3SeqNetStore>(getContext());
}

template<class Store>
void
testSetBit01(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net num = store.mkNumber("0", NetTypeInfo::mkIntType(8));
    Net actual = store.mkSetBit(num, 0, store.mkTrue());
    auto int8type = NetTypeInfo::mkIntType(8);
    Net expected = store.mkNumber("1", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 1, store.mkTrue());
    expected = store.mkNumber("3", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 2, store.mkTrue());
    expected = store.mkNumber("7", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 3, store.mkTrue());
    expected = store.mkNumber("15", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 4, store.mkTrue());
    expected = store.mkNumber("31", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 5, store.mkTrue());
    expected = store.mkNumber("63", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 6, store.mkTrue());
    expected = store.mkNumber("127", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 7, store.mkTrue());
    expected = store.mkNumber("255", int8type);
    ASSERT_EQ(expected, actual);
}

template<class Store>
void
testSetBit02(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    auto int8type = NetTypeInfo::mkIntType(8);
    Net num = store.mkNumber("255", int8type);
    Net actual = store.mkSetBit(num, 0, store.mkFalse());
    Net expected = store.mkNumber("254", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 1, store.mkFalse());
    expected = store.mkNumber("252", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 2, store.mkFalse());
    expected = store.mkNumber("248", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 3, store.mkFalse());
    expected = store.mkNumber("240", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 4, store.mkFalse());
    expected = store.mkNumber("224", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 5, store.mkFalse());
    expected = store.mkNumber("192", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 6, store.mkFalse());
    expected = store.mkNumber("128", int8type);
    ASSERT_EQ(expected, actual);
    actual = store.mkSetBit(actual, 7, store.mkFalse());
    expected = store.mkNumber("0", int8type);
    ASSERT_EQ(expected, actual);
}

TEST_F(Z3NetStoreTest, TestSetBit) {
    testSetBit01<Z3ComNetStore>(getContext());
    testSetBit01<Z3SeqNetStore>(getContext());
    testSetBit02<Z3ComNetStore>(getContext());
    testSetBit02<Z3SeqNetStore>(getContext());
}

TEST_F(Z3NetStoreTest, TestAddUInt) {
    Z3ComNetStore store(getContext());
    auto input1 = store.mkVariable("input1", net::NetTypeInfo::mkUintType(16));
    auto input2 = store.mkVariable("input2", net::NetTypeInfo::mkUintType(16));
    ASSERT_NO_FATAL_FAILURE(store.mkAdd(input1, input2));
}

TEST_F(Z3NetStoreTest, TestUIntNumbersHaveTypeInt) {
    Z3SeqNetStore store(getContext());
    auto uint8Type = NetTypeInfo::mkUintType(8);
    auto int8Type = NetTypeInfo::mkIntType(8);
    auto num_1_8 = store.mkNumber("1", uint8Type);
    ASSERT_EQ(store.getNetTypeInfo(num_1_8), int8Type);
}

template<class Store>
void
testCasts(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    auto int8Type = NetTypeInfo::mkIntType(8);
    auto int16Type = NetTypeInfo::mkIntType(16);
    auto uint16Type = NetTypeInfo::mkUintType(16);
    Net num_1_8 = store.mkNumber("1", int8Type);
    Net num_1_16 = store.mkNumber("1", int16Type);
    Net casted1 = store.castToType(num_1_8, int16Type);
    ASSERT_EQ(num_1_16, casted1);
    Net num_m1_8 = store.mkNumber("-1", int8Type);
    Net num_m1_16 = store.mkNumber("-1", int16Type);
    Net casted2 = store.castToType(num_m1_8, int16Type);
    ASSERT_EQ(num_m1_16, casted2);
    Net casted3 = store.castToType(num_1_16, int8Type);
    ASSERT_EQ(num_1_8, casted3);
    Net casted4 = store.castToType(num_m1_16, int8Type);
    ASSERT_EQ(num_m1_8, casted4);
}

TEST_F(Z3NetStoreTest, Casts) {
    testCasts<Z3ComNetStore>(getContext());
}

template<class Store>
void
testExtract(Z3_context context) {
    Store store(context);
    using Net = typename Store::NetDataType;
    Net x = store.mkVariable("x", NetTypeInfo::mkIntType(16));
    Net extract = store.mkExtract(3, 1, x);
    ASSERT_EQ(3u, store.getMsb(extract));
    ASSERT_EQ(1u, store.getLsb(extract));
}

TEST_F(Z3NetStoreTest, TestExtract) {
    testExtract<Z3ComNetStore>(getContext());
    testExtract<Z3SeqNetStore>(getContext());
}

template<class Store>
void
testBvNumbers(Z3_context context) {
    Store store(context);
    auto n8 = store.mkNumber("#x02", NetTypeInfo::mkIntType(8));
    auto n2 = store.mkExtract(1, 0, n8);
    auto nti = store.getNetTypeInfo(n2);
    auto n2_x = store.mkNumber("#x02", nti);
    auto n2_b = store.mkNumber("#b10", nti);
    ASSERT_EQ("#b10", store.toString(n2_x));
    ASSERT_EQ("#b10", store.toString(n2_b));
}

TEST_F(Z3NetStoreTest, TestBvNumbers) {
    testBvNumbers<Z3ComNetStore>(getContext());
    testBvNumbers<Z3SeqNetStore>(getContext());
}

TEST_F(Z3NetStoreTest, TestIsAtom) {
    Z3SeqNetStore store(getContext());
    auto num = store.mkNumber("#x02", NetTypeInfo::mkIntType(8));
    ASSERT_FALSE(store.isAtom(num));
    auto var = store.mkVariable("var", net::NetTypeInfo::mkBooleanType());
    ASSERT_TRUE(store.isAtom(var));
    auto notVar = store.mkNot(var);
    ASSERT_FALSE(store.isAtom(notVar));
    auto var1 = store.mkVariable("var1", net::NetTypeInfo::mkBooleanType());
    auto andVar = store.mkAnd(var, var1);
    ASSERT_FALSE(store.isAtom(andVar));
    auto var2 = store.mkVariable("var2", net::NetTypeInfo::mkIntType(8));
    auto pred = store.mkLeqIntSigned(var2, num);
    ASSERT_TRUE(store.isAtom(pred));
}

}  // namespace test
