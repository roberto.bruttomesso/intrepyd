/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "deps/gtest/include/gtest/gtest.h"

#include "test/fixture/Z3Fixture.h"

#include "src/net/Z3ComNet.h"
#include "src/net/Z3SeqNet.h"

using namespace net;

namespace test {

class Z3NetTest : public Z3Fixture {
};

/*
 * True is always true
 */
template<class Net>
void
testTrue(Z3_context context) {
    Z3_ast ast1 = Z3_mk_true(context);
    unsigned id1 = Z3_get_ast_id(context, ast1);
    Net z3NetTrue1(id1, ast1);

    Z3_ast ast2 = Z3_mk_true(context);
    unsigned id2 = Z3_get_ast_id(context, ast2);
    Net z3NetTrue2(id2, ast2);

    ASSERT_EQ(z3NetTrue1, z3NetTrue2);
    ASSERT_EQ(z3NetTrue1.getZ3Ast(), z3NetTrue2.getZ3Ast());
}

TEST_F(Z3NetTest, TestSeqTrue) {
    Z3_context context = getContext();
    testTrue<Z3SeqNet>(context);
}

TEST_F(Z3NetTest, TestComTrue) {
    Z3_context context = getContext();
    testTrue<Z3ComNet>(context);
}

/*
 * False is always false
 */
template<class Net>
void
testFalse(Z3_context context) {
    Z3_ast ast1 = Z3_mk_false(context);
    unsigned id1 = Z3_get_ast_id(context, ast1);
    Net z3NetFalse1(id1, ast1);

    Z3_ast ast2 = Z3_mk_false(context);
    unsigned id2 = Z3_get_ast_id(context, ast2);
    Net z3NetFalse2(id2, ast2);

    ASSERT_EQ(z3NetFalse1, z3NetFalse2);
    ASSERT_EQ(z3NetFalse1.getZ3Ast(), z3NetFalse2.getZ3Ast());
}

TEST_F(Z3NetTest, TestSeqFalse) {
    Z3_context context = getContext();
    testFalse<Z3SeqNet>(context);
}

TEST_F(Z3NetTest, TestComFalse) {
    Z3_context context = getContext();
    testFalse<Z3ComNet>(context);
}

}  // namespace test
