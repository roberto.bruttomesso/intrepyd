/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include "src/circuit/Z3SeqCircuit.h"
#include "src/net/Z3SeqNetStore.h"
#include "test/fixture/Z3Fixture.h"

using namespace net;
using namespace circuit;

namespace test {

class Z3SeqCircuitTest : public Z3Fixture {
};


/*
 * Latch creation and retrival works for booleans
 */
TEST_F(Z3SeqCircuitTest, TestLatchBoolean) {
    Z3SeqNetStore store(context_);
    Z3SeqNet init = store.mkFalse();
    Z3SeqNet next = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqCircuit circuit("circ");
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, init, next);
    ASSERT_TRUE(circuit.isLatch(latch));
    ASSERT_EQ(init, circuit.getLatchInit(latch));
    ASSERT_EQ(next, circuit.getLatchNext(latch));
}

/*
 * Latch creation and retrival works for integers
 */
TEST_F(Z3SeqCircuitTest, TestLatchInt) {
    Z3SeqNetStore store(context_);
    NetTypeInfo type = NetTypeInfo::mkIntType(8);
    Z3SeqNet init  = store.mkNumber("23", type);
    Z3SeqNet next  = store.mkVariable("i1", type);
    Z3SeqNet latch = store.mkVariable("latch", type);
    Z3SeqCircuit circuit("circ");
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, init, next);
    ASSERT_TRUE(circuit.isLatch(latch));
    ASSERT_EQ(init, circuit.getLatchInit(latch));
    ASSERT_EQ(next, circuit.getLatchNext(latch));
}

/*
 * Latch creation and retrival works for float32
 */
TEST_F(Z3SeqCircuitTest, TestLatchFloat32) {
    Z3SeqNetStore store(context_);
    NetTypeInfo type = NetTypeInfo::mkFloatType(32);
    Z3SeqNet init  = store.mkNumber("1.1", type);
    Z3SeqNet next  = store.mkVariable("i1", type);
    Z3SeqNet latch = store.mkVariable("latch", type);
    Z3SeqCircuit circuit("circ");
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, init, next);
    ASSERT_TRUE(circuit.isLatch(latch));
    ASSERT_EQ(init, circuit.getLatchInit(latch));
    ASSERT_EQ(next, circuit.getLatchNext(latch));
}

}  // namespace test
