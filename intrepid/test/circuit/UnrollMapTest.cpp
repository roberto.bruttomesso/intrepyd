/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include <functional>

#include "src/net/Net.h"
#include "src/circuit/UnrollMap.h"

using namespace net;
using namespace circuit;

class MySeqNet final : public Net {
public:
    MySeqNet() : Net() {}
    explicit MySeqNet(unsigned id) : Net(id) {}
};

namespace std {
template<> struct hash<MySeqNet> {
    size_t operator()(MySeqNet n) const {
        const unsigned nVal = n;
        return hash<unsigned>()(nVal);
    }
};
}  // namespace std

class MyComNet final : public Net {
public:
    MyComNet() : Net() {}
    MyComNet(unsigned id) : Net(id) {}
};

namespace std {
template<> struct hash<MyComNet> {
    size_t operator()(MyComNet n) const {
        const unsigned nVal = n;
        return hash<unsigned>()(nVal);
    }
};
}  // namespace std

namespace test {

TEST(UnrollMap, Test01) {
    UnrollMap<MySeqNet, MyComNet> unrollMap;
    MySeqNet s1(3);
    MySeqNet s2(4);
    MyComNet c1(13);
    MyComNet c2(14);
    MyComNet c3(15);
    MyComNet c4(16);
    MyComNet c5(21);

    unrollMap.setMapping({s1, 0}, c1);
    unrollMap.setMapping({s1, 1}, c2);
    unrollMap.setMapping({s1, 5}, c3);
    unrollMap.setMapping({s2, 0}, c4);

    ASSERT_TRUE(unrollMap.hasFwdMapping({s1, 0}));
    ASSERT_TRUE(unrollMap.hasFwdMapping({s1, 1}));
    ASSERT_FALSE(unrollMap.hasFwdMapping({s1, 2}));
    ASSERT_FALSE(unrollMap.hasFwdMapping({s1, 3}));
    ASSERT_FALSE(unrollMap.hasFwdMapping({s1, 4}));
    ASSERT_TRUE(unrollMap.hasFwdMapping({s1, 5}));
    ASSERT_FALSE(unrollMap.hasFwdMapping({s1, 6}));
    ASSERT_TRUE(unrollMap.hasFwdMapping({s2, 0}));

    ASSERT_TRUE(unrollMap.hasBwdMapping(c1));
    ASSERT_TRUE(unrollMap.hasBwdMapping(c2));
    ASSERT_TRUE(unrollMap.hasBwdMapping(c3));
    ASSERT_TRUE(unrollMap.hasBwdMapping(c4));
    ASSERT_FALSE(unrollMap.hasBwdMapping(c5));

    ASSERT_EQ(c1, unrollMap.getFwdMapping({s1, 0}));
    ASSERT_EQ(c2, unrollMap.getFwdMapping({s1, 1}));
    ASSERT_EQ(c3, unrollMap.getFwdMapping({s1, 5}));
    ASSERT_EQ(c4, unrollMap.getFwdMapping({s2, 0}));

    {
    auto range = unrollMap.getBwdMapping(c1);
    ASSERT_EQ(s1, ((*(range.first)).second).getSeqNet());
    ASSERT_EQ(0u,  (((*(range.first)).second)).getTime());
    }

    {
    auto range = unrollMap.getBwdMapping(c2);
    ASSERT_EQ(s1, (((*(range.first)).second)).getSeqNet());
    ASSERT_EQ(1u,  (((*(range.first)).second)).getTime());
    }

    {
    auto range = unrollMap.getBwdMapping(c3);
    ASSERT_EQ(s1, (((*(range.first)).second)).getSeqNet());
    ASSERT_EQ(5u,  (((*(range.first)).second)).getTime());
    }

    {
    auto range = unrollMap.getBwdMapping(c4);
    ASSERT_EQ(s2, (((*(range.first)).second)).getSeqNet());
    ASSERT_EQ(0u,  (((*(range.first)).second)).getTime());
    }
}

}  // namespace test
