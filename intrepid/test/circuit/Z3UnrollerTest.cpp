/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include "test/fixture/Z3Fixture.h"
#include "src/net/Z3SeqNetStore.h"
#include "src/circuit/Z3Unroller.h"
#include "src/circuit/Z3ComCircuit.h"
#include "src/circuit/Z3SeqCircuit.h"

using namespace net;
using namespace circuit;

namespace test {

class Z3UnrollerTest : public Z3Fixture {
};

/*
 * A single latch initialized to false, is unrolled to false at depth 0
 * if initial states are inlined
 */
TEST_F(Z3UnrollerTest, Test01) {
    Z3SeqNetStore store(context_);
    Z3SeqNet i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());

    Z3SeqCircuit circuit("circ");
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkFalse(), i1);
    circuit.mkOutput(latch);

    Z3ComNetStore comStore(context_);
    Z3ComCircuit comCircuit("comCirc");

    Z3Unroller unroller(store, circuit, &comStore, &comCircuit);

    unroller.unroll(0,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(1u, circuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comStore.isFalse(output));
    }
}

/*
 * As Testt01, but initial states are not inlined, resulting in a new input
 */
TEST_F(Z3UnrollerTest, Test02) {
    Z3SeqNetStore store(context_);
    Z3SeqNet i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());

    Z3SeqCircuit circuit("");
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkFalse(), i1);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(0,
                    UnrollerOptions::Initialize::DoNotInitialize);

    ASSERT_EQ(1u, circuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comCircuit.isInput(output));
    }
}

/*
 * Constantly false latch (here we test 10 iterations), because
 * - it is initialized to false
 * - it has (latch AND input) as next function
 */
TEST_F(Z3UnrollerTest, Test03) {
    Z3SeqNetStore store(context_);
    Z3SeqNet i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next = store.mkAnd(latch, i1);

    Z3SeqCircuit circuit("");
    circuit.mkInput(i1);
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkFalse(), next);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(10,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(1u, circuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comNetStore.isFalse(output));
    }
}

/*
 * Constantly true latch (here we test 10 iterations), because
 * - it is initialized to true
 * - it has (latch OR input) as next function
 */
TEST_F(Z3UnrollerTest, Test04) {
    Z3SeqNetStore store(context_);
    Z3SeqNet i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next = store.mkOr(latch, i1);

    Z3SeqCircuit circuit("");
    circuit.mkInput(i1);
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkTrue(), next);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(10,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(1u, circuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comNetStore.isTrue(output));
    }
}

/*
 * Cascade of 3 latches in a loop, TRUE is propagated correctly
 */
TEST_F(Z3UnrollerTest, Test05) {
    Z3SeqNetStore store(context_);
    Z3SeqNet l1 = store.mkVariable("l1", NetTypeInfo::mkBooleanType());
    Z3SeqNet l2 = store.mkVariable("l2", NetTypeInfo::mkBooleanType());
    Z3SeqNet l3 = store.mkVariable("l3", NetTypeInfo::mkBooleanType());

    Z3SeqCircuit circuit("");
    circuit.mkLatch(l1);
    circuit.mkLatch(l2);
    circuit.mkLatch(l3);
    circuit.setLatchInitNext(l1, store.mkTrue(), l3);
    circuit.setLatchInitNext(l2, store.mkFalse(), l1);
    circuit.setLatchInitNext(l3, store.mkFalse(), l2);

    circuit.mkOutput(l3);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(0,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(1u, comCircuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comNetStore.isFalse(output));
    }

    unroller.unroll(1,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(2u, comCircuit.getOutputs().size());
    for (auto output : comCircuit.getOutputs()) {
        ASSERT_TRUE(comNetStore.isFalse(output));
    }

    unroller.unroll(2,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(3u, comCircuit.getOutputs().size());
}

/*
 * A test added after a bug was found
 */
TEST_F(Z3UnrollerTest, Test06) {
    Z3SeqNetStore store(context_);
    Z3SeqNet ft = store.mkVariable("__first_tick", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("pre_env_always_ok",
                                      NetTypeInfo::mkBooleanType());
    Z3SeqNet i0 = store.mkVariable("i0", NetTypeInfo::mkBooleanType());
    Z3SeqNet i1 = store.mkVariable("i1", NetTypeInfo::mkBooleanType());

    Z3SeqCircuit circuit("");

    circuit.mkInput(i0);
    circuit.mkInput(i1);

    circuit.mkLatch(ft);
    circuit.setLatchInitNext(ft, store.mkTrue(), store.mkFalse());

    circuit.mkLatch(latch);
    Z3SeqNet env_ok = store.mkIte(ft, store.mkNot(i1), store.mkTrue());
    Z3SeqNet pre_env_always_ok = latch;
    Z3SeqNet __l4 = store.mkAnd(env_ok, pre_env_always_ok);
    Z3SeqNet env_always_ok = store.mkIte(ft, env_ok, __l4);
    circuit.setLatchInitNext(latch, store.mkFalse(), env_always_ok);

    Z3SeqNet prop_ok = store.mkOr(store.mkNot(i1), i0);
    Z3SeqNet OK = store.mkOr(store.mkNot(env_always_ok), prop_ok);
    Z3SeqNet target = store.mkNot(OK);
    circuit.mkOutput(target);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(0,
                    UnrollerOptions::Initialize::InlineInitialStates);

    ASSERT_EQ(1u, comCircuit.getOutputs().size());
    auto outputs = comCircuit.getOutputs();
    ASSERT_TRUE(comNetStore.isFalse(outputs[0]));
}

}  // namespace test
