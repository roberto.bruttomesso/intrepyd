/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include "test/fixture/Z3Fixture.h"
#include "src/circuit/Z3SeqCircuit.h"
#include "src/circuit/Z3ComCircuit.h"
#include "src/circuit/Z3Unroller.h"
#include "src/solver/Z3ComSolver.h"
#include "src/solver/Z3SeqSolver.h"

using namespace net;
using namespace circuit;
using namespace solver;

namespace test {

class Z3SolverTest : public Z3Fixture {
};

/*
 * A constantly false latch is unrolled (but not inlined), so when
 * we assume the initial state to false the output when we solve
 * will result as unsatisfiable
 */
TEST_F(Z3SolverTest, Test01) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet i1    = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = store.mkAnd(latch, i1);

    circuit.mkInput(i1);
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkFalse(), next);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(10,
                    UnrollerOptions::Initialize::DoNotInitialize);

    Z3ComSolver comSolver(context_);

    for (auto latch: circuit.getLatches()) {
        Z3ComNet comLatch = unroller.getNetAtTime(latch, 0);
        Z3SeqNet init = circuit.getLatchInit(latch);
        if (!init.isValid()) {
            continue;
        }
        comSolver.addAssumption(comLatch, store.isFalse(init));
    }

    for (auto output: comCircuit.getOutputs()) {
        const auto result = comSolver.solve(output);
        ASSERT_EQ(SolverResult::Unsatisfiable, result);
    }
}

/*
 * A constantly true latch is unrolled (but not inlined), so when
 * we assume the initial state to true the output when we solve
 * will result as satisfiable
 */
TEST_F(Z3SolverTest, Test02) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet i1    = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = store.mkOr(latch, i1);

    circuit.mkInput(i1);
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, store.mkTrue(), next);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(10,
                    UnrollerOptions::Initialize::DoNotInitialize);

    Z3ComSolver comSolver(context_);

    for (auto latch: circuit.getLatches()) {
        Z3ComNet comLatch = unroller.getNetAtTime(latch, 0);
        Z3SeqNet init = circuit.getLatchInit(latch);
        if (!init.isValid()) {
            continue;
        }
        comSolver.addAssumption(comLatch, store.isFalse(init));
    }

    for (auto output: comCircuit.getOutputs()) {
        const auto result = comSolver.solve(output);
        ASSERT_EQ(SolverResult::Satisfiable, result);
    }
}

/*
 * Like Test02, but latch is uninitialized, so after unrolling
 * it is still possible to satisfy the output
 */
TEST_F(Z3SolverTest, Test03) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet i1    = store.mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = store.mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = store.mkOr(latch, i1);

    circuit.mkInput(i1);
    circuit.mkLatch(latch);
    circuit.setLatchInitNext(latch, Z3SeqNet(), next);
    circuit.mkOutput(latch);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(10,
                    UnrollerOptions::Initialize::DoNotInitialize);

    Z3ComSolver comSolver(context_);

    for (auto latch: circuit.getLatches()) {
        Z3ComNet comLatch = unroller.getNetAtTime(latch, 0);
        Z3SeqNet init = circuit.getLatchInit(latch);
        if (!init.isValid()) {
            continue;
        }
        comSolver.addAssumption(comLatch, store.isFalse(init));
    }

    for (auto output: comCircuit.getOutputs()) {
        const auto result = comSolver.solve(output);
        ASSERT_EQ(SolverResult::Satisfiable, result);
    }
}

/*
 * Simple counter, unrolled 32 times, initialized to 0, results in final value
 * of 31
 */
TEST_F(Z3SolverTest, Test04) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet counter = store.mkVariable("latch", NetTypeInfo::mkIntType(8));
    Z3SeqNet next    = store.mkAdd(counter, store.mkNumber("1", NetTypeInfo::mkIntType(8)));
    Z3SeqNet init    = store.mkNumber("0", NetTypeInfo::mkIntType(8));

    circuit.mkLatch(counter);
    circuit.setLatchInitNext(counter, init, next);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(32,
                    UnrollerOptions::Initialize::DoNotInitialize);

    Z3ComSolver comSolver(context_);

    ASSERT_EQ(1u, circuit.getLatches().size());
    for (auto latch: circuit.getLatches()) {
        Z3ComNet comLatch = unroller.getNetAtTime(latch, 0);
        if (!init.isValid()) {
            continue;
        }
        Z3ComNet initValue = unroller.getComNumber(init);
        Z3ComNet latchInit = comNetStore.mkEq(comLatch, initValue);
        comSolver.addAssumption(latchInit);
    }

    Z3ComNet finalCounter = unroller.getNetAtTime(counter, 31);

    // We create a fake input to allow a boolean predicate in the solve query
    Z3ComNet fakeInput = comNetStore.mkVariable("fake", NetTypeInfo::mkIntType(8));
    comCircuit.mkInput(fakeInput);
    Z3ComNet query = comNetStore.mkEq(finalCounter, fakeInput);

    const auto result = comSolver.solve(query);
    ASSERT_EQ(SolverResult::Satisfiable, result);

    Z3ComNet finalValue = comSolver.evaluate(finalCounter);
    ASSERT_EQ(comNetStore.mkNumber("31", NetTypeInfo::mkIntType(8)), finalValue);
}

/*
 * Enumerates all the cubes (2) from a simple or gate
 */
TEST_F(Z3SolverTest, Test05) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet in1    = store.mkVariable("in1", NetTypeInfo::mkBooleanType());
    Z3SeqNet in2    = store.mkVariable("in2", NetTypeInfo::mkBooleanType());
    Z3SeqNet target = store.mkOr(in1, in2);

    circuit.mkInput(in1);
    circuit.mkInput(in2);

    Z3SeqSolver seqSolver(context_);

    seqSolver.allSmtAddWatchedNet(in1);
    seqSolver.allSmtAddWatchedNet(in2);
    seqSolver.allSmtSetTarget(target);

    seqSolver.allSmtSolve(&store);
    seqSolver.allSmtSolve(&store);
    auto cube = seqSolver.allSmtSolve(&store);

    ASSERT_TRUE(store.isTrue(cube));
}

/*
 * Enumerates all the cubes (1) from a simple and gate
 */
TEST_F(Z3SolverTest, Test06) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    Z3SeqNet in1    = store.mkVariable("in1", NetTypeInfo::mkBooleanType());
    Z3SeqNet in2    = store.mkVariable("in2", NetTypeInfo::mkBooleanType());
    Z3SeqNet target = store.mkAnd(in1, in2);

    circuit.mkInput(in1);
    circuit.mkInput(in2);

    Z3SeqSolver seqSolver(context_);

    seqSolver.allSmtAddWatchedNet(in1);
    seqSolver.allSmtAddWatchedNet(in2);
    seqSolver.allSmtSetTarget(target);

    seqSolver.allSmtSolve(&store);
    auto cube = seqSolver.allSmtSolve(&store);

    ASSERT_TRUE(store.isTrue(cube));
}

TEST_F(Z3SolverTest, MkLeqUint01) {
    Z3ComNetStore store(context_);
    Z3ComCircuit circuit("");

    auto t = NetTypeInfo::mkUintType(8);
    auto x = store.mkVariable("x", t);
    auto n = store.mkNumber("2", t);
    auto leq = store.mkLeqIntUnsigned(x, n);
    auto eq = store.mkEq(x, n);
    auto leqAndEq = store.mkAnd(eq, leq);

    Z3ComSolver comSolver(context_);

    const auto result = comSolver.solve(leqAndEq);
    ASSERT_EQ(SolverResult::Satisfiable, result);
}

TEST_F(Z3SolverTest, MkLeqUint02) {
    Z3SeqNetStore store(context_);
    Z3SeqCircuit circuit("");

    auto t = NetTypeInfo::mkUintType(8);
    auto x = store.mkVariable("x", t);
    auto n = store.mkNumber("2", t);
    auto leq = store.mkLeqIntUnsigned(x, n);
    auto eq = store.mkEq(x, n);
    auto leqAndEq = store.mkAnd(eq, leq);

    circuit.mkInput(x);
    circuit.mkOutput(leqAndEq);

    Z3ComNetStore comNetStore(context_);
    Z3ComCircuit comCircuit("");

    Z3Unroller unroller(store, circuit, &comNetStore, &comCircuit);

    unroller.unroll(0,
                    UnrollerOptions::Initialize::DoNotInitialize);

    Z3ComSolver comSolver(context_);

    for (auto output: comCircuit.getOutputs()) {
        const auto result = comSolver.solve(output);
        ASSERT_EQ(SolverResult::Satisfiable, result);
    }
}

}  // namespace test
