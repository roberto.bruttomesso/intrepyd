/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include "src/utils/Z3Utils.h"

namespace utils {

TEST(Z3Utils, Test01) {
    Z3_config config = Z3_mk_config();
    Z3_context context = Z3_mk_context(config);

    // Testing typeToSort
    ASSERT_EQ(Z3_mk_bool_sort(context),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkBooleanType()));
    ASSERT_EQ(Z3_mk_bv_sort(context, 8),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkIntType(8)));
    ASSERT_EQ(Z3_mk_bv_sort(context, 16),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkIntType(16)));
    ASSERT_EQ(Z3_mk_bv_sort(context, 32),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkIntType(32)));
    ASSERT_EQ(Z3_mk_bv_sort(context, 8),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkUintType(8)));
    ASSERT_EQ(Z3_mk_bv_sort(context, 16),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkUintType(16)));
    ASSERT_EQ(Z3_mk_bv_sort(context, 32),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkUintType(32)));
    ASSERT_EQ(Z3_mk_real_sort(context),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkRealType()));
    ASSERT_EQ(Z3_mk_fpa_sort_16(context),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkFloatType(16)));
    ASSERT_EQ(Z3_mk_fpa_sort_32(context),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkFloatType(32)));
    ASSERT_EQ(Z3_mk_fpa_sort_64(context),
              Z3Utils::typeToSort(context, net::NetTypeInfo::mkFloatType(64)));

    Z3_del_context(context);
    Z3_del_config(config);
}

}  // namespace utils
