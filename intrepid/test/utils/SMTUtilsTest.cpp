/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include <string>

#include "gtest/gtest.h"

#include "src/utils/SMTUtils.h"

using namespace utils;

namespace test {

TEST(SMTUtils, Test01) {
    const std::string input = "(fp #b0 #b01111111101 #x3333333333333)";
    bool sign;
    uint64_t exp, sig;
    ASSERT_TRUE(SMTUtils::isFpNumber(input));
    std::tie(sign, exp, sig) = SMTUtils::getFpNumberComponents(input);
    ASSERT_FALSE(sign);
    ASSERT_EQ(exp, 1021);
    ASSERT_EQ(sig, 900719925474099);
}

TEST(SMTUtils, Test02) {
    const std::string input = "(fp #b0 #x7f #b00000000000000000000000)";
    bool sign;
    uint64_t exp, sig;
    ASSERT_TRUE(SMTUtils::isFpNumber(input));
    std::tie(sign, exp, sig) = SMTUtils::getFpNumberComponents(input);
    ASSERT_FALSE(sign);
    ASSERT_EQ(exp, 127);
    ASSERT_EQ(sig, 0);
}

}  // namespace test
