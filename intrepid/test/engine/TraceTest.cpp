/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "deps/gtest/include/gtest/gtest.h"

#include "src/engine/Trace.h"

using namespace engine;

class MyTrace final : public Trace<char, std::string> {
public:
    MyTrace() = default;
};

namespace test {

TEST(TraceTest, Test01) {
    MyTrace cex;
    cex.setData('a', 5, "false");
    cex.setData('b', 1, "true");

    ASSERT_EQ("false", cex.getData('a', 5));
    ASSERT_EQ("true", cex.getData('b', 1));
    ASSERT_EQ(5u, cex.getMaxDepth());
}

}  // namespace test
