/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "deps/gtest/include/gtest/gtest.h"

#include <cassert>

#include "test/fixture/Z3Fixture.h"
#include "src/net/Z3SeqNet.h"
#include "src/net/Z3ComNet.h"
#include "src/net/Z3SeqNetStore.h"
#include "src/circuit/Z3SeqCircuit.h"
#include "src/factory/Z3Factory.h"
#include "src/engine/BackwardReach.h"

using namespace net;
using namespace solver;
using namespace engine;
using namespace circuit;
using namespace factory;

namespace test {

/*
 * Finds trace immediately at step 0
 */
TEST(BackwardReachTest, Test01) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("circ");

    Z3SeqNet i1    = seqNetStore->mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkOr(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkFalse(), next);
    seqCircuit->mkOutput(latch);

    BackwardReach<net::Z3SeqNet, net::Z3ComNet>
        backwardReach(&factory, seqNetStore.get(), seqCircuit.get());
    backwardReach.addTarget(latch);
    backwardReach.prepareForSolving();

    EngineResult result = backwardReach.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

/*
 * Finds trace at step 1
 */
TEST(BackwardReachTest, Test02) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("circ");

    Z3SeqNet i1    = seqNetStore->mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkOr(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkFalse(), next);
    seqCircuit->mkOutput(latch);

    BackwardReach<net::Z3SeqNet, net::Z3ComNet>
        backwardReach(&factory, seqNetStore.get(), seqCircuit.get());
    backwardReach.addTarget(latch);
    backwardReach.prepareForSolving();

    EngineResult result = backwardReach.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

/*
 * No trace exists
 */
TEST(BackwardReachTest, Test03) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("circ");

    Z3SeqNet i1    = seqNetStore->mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkAnd(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkFalse(), next);
    seqCircuit->mkOutput(latch);

    BackwardReach<net::Z3SeqNet, net::Z3ComNet>
        backwardReach(&factory, seqNetStore.get(), seqCircuit.get());
    backwardReach.addTarget(latch);
    backwardReach.prepareForSolving();

    EngineResult result = backwardReach.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Unreachable, result);
}

/*
 * We can reach number 100, by counting from 0, step 1
 */
TEST(BackwardReachTest, Test04) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("circ");

    Z3SeqNet counter = seqNetStore->mkVariable("counter", NetTypeInfo::mkIntType(8));
    Z3SeqNet one     = seqNetStore->mkNumber("1", NetTypeInfo::mkIntType(8));
    Z3SeqNet next    = seqNetStore->mkAdd(counter, one);
    Z3SeqNet init    = seqNetStore->mkNumber("0", NetTypeInfo::mkIntType(8));
    Z3SeqNet hundred = seqNetStore->mkNumber("100", NetTypeInfo::mkIntType(8));
    Z3SeqNet target  = seqNetStore->mkEq(counter, hundred);

    seqCircuit->mkLatch(counter);
    seqCircuit->setLatchInitNext(counter, init, next);
    seqCircuit->mkOutput(target);

    BackwardReach<net::Z3SeqNet, net::Z3ComNet>
        backwardReach(&factory, seqNetStore.get(), seqCircuit.get());
    backwardReach.addTarget(target);
    backwardReach.prepareForSolving();

    EngineResult result = backwardReach.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

/*
 * Finds trace at depth 3, non-boolean reasoning
 */
TEST(BackwardReachTest, Test05) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("circ");

    Z3SeqNet i1    = seqNetStore->mkVariable("i1", NetTypeInfo::mkRealType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkRealType());
    Z3SeqNet next  = seqNetStore->mkAdd(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch,
                                 seqNetStore->mkNumber("0",
                                                       NetTypeInfo::mkRealType()),
                                 next);
    Z3SeqNet out = seqNetStore->mkGeqReal(latch,
                                          seqNetStore->mkNumber("3",
                                                                NetTypeInfo::mkRealType()));
    seqCircuit->mkOutput(out);

    BackwardReach<net::Z3SeqNet, net::Z3ComNet>
        backwardReach(&factory,
                      seqNetStore.get(),
                      seqCircuit.get());
    backwardReach.addTarget(out);
    backwardReach.prepareForSolving();

    EngineResult result = backwardReach.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

}  // namespace test
