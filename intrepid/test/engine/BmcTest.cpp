/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "deps/gtest/include/gtest/gtest.h"

#include "src/factory/Z3Factory.h"
#include "src/net/Z3SeqNetStore.h"
#include "src/circuit/Z3SeqCircuit.h"
#include "src/circuit/Z3ComCircuit.h"
#include "src/engine/Bmc.h"
#include "src/engine/Simulator.h"

using namespace net;
using namespace circuit;
using namespace engine;
using namespace factory;

namespace test {

TEST(Z3BmcTest, Test01) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");
    auto comNetStore = factory.buildComNetStore();

    const std::string i1Name = "i1";

    Z3SeqNet i1    = seqNetStore->mkVariable(i1Name, NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkOr(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkTrue(), next);
    seqCircuit->mkOutput(latch);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(latch);
    bmc.prepareForSolving();
    bmc.setCurrentDepth(1);
    bmc.setAllowTargetsOnlyAtLastDepth(false);

    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
    bmc.removeLastReachedTargets();
    ASSERT_EQ(0u, bmc.getNumberOfRemainingTargets());
    ASSERT_EQ(1u, bmc.getLastReachedTargets().size());

    for (Z3SeqNet t: bmc.getLastReachedTargets()) {
        const Trace<Z3SeqNet, Z3ComNet>& trace = bmc.getTraceForTarget(t);
        auto prefix = Unroller<Z3SeqNet, Z3ComNet>::depthPrefixedName(0, i1Name);
        ASSERT_EQ(prefix, comNetStore->toString(trace.getData(i1, 0)));

        // Resimulation
        Simulator<Z3SeqNet, Z3ComNet> simulator(*seqNetStore,
                                                *seqCircuit,
                                                comNetStore.get());
        simulator.addTarget(t);
        simulator.simulate(trace, 0);
        ASSERT_EQ(1u, simulator.getLastReachedTargets().size());
        ASSERT_EQ(t, simulator.getLastReachedTargets()[0]);
    }
}

TEST(Z3BmcTest, Test01a) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");
    auto comNetStore = factory.buildComNetStore();

    const std::string i1Name = "i1";

    Z3SeqNet i1    = seqNetStore->mkVariable(i1Name, NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkOr(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkTrue(), next);
    seqCircuit->mkOutput(latch);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(latch);
    bmc.prepareForSolving();
    bmc.setCurrentDepth(1);

    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Unknown, result);
}

TEST(Z3BmcTest, Test02) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    Z3SeqNet i1    = seqNetStore->mkVariable("i1", NetTypeInfo::mkBooleanType());
    Z3SeqNet latch = seqNetStore->mkVariable("latch", NetTypeInfo::mkBooleanType());
    Z3SeqNet next  = seqNetStore->mkOr(latch, i1);

    seqCircuit->mkInput(i1);
    seqCircuit->mkLatch(latch);
    seqCircuit->setLatchInitNext(latch, seqNetStore->mkFalse(), next);
    seqCircuit->mkOutput(latch);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(latch);
    bmc.prepareForSolving();

    bmc.setCurrentDepth(0);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Unknown, result);

    bmc.setCurrentDepth(1);
    // EngineResult result = bmc.findFirstReachableTarget();
    bmc.findFirstReachableTarget();
    // ASSERT_EQ(EngineResult::Reachable, result);
}

TEST(Z3BmcTest, Test03) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    Z3SeqNet counter = seqNetStore->mkVariable("counter", NetTypeInfo::mkIntType(8));
    Z3SeqNet next    = seqNetStore->mkAdd(counter, seqNetStore->mkNumber("1", NetTypeInfo::mkIntType(8)));
    Z3SeqNet init    = seqNetStore->mkNumber("0", NetTypeInfo::mkIntType(8));
    Z3SeqNet fakeInput = seqNetStore->mkVariable("fake", NetTypeInfo::mkIntType(8));
    Z3SeqNet target = seqNetStore->mkEq(counter, fakeInput);

    seqCircuit->mkInput(fakeInput);
    seqCircuit->mkLatch(counter);
    seqCircuit->setLatchInitNext(counter, init, next);
    seqCircuit->mkOutput(target);

    const unsigned depth = 10;
    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(target);
    bmc.prepareForSolving();

    for (unsigned i = 0; i < depth; i++) {
        bmc.setCurrentDepth(i);
        EngineResult result = bmc.findFirstReachableTarget();
        ASSERT_EQ(EngineResult::Reachable, result);
    }
}

TEST(Z3BmcTest, Test04) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    auto type = NetTypeInfo::mkFloatType(32);
    Z3SeqNet in1 = seqNetStore->mkVariable("in1", type);
    Z3SeqNet one = seqNetStore->mkNumber("1.0", type);
    Z3SeqNet eq = seqNetStore->mkEq(in1, one);
    seqCircuit->mkInput(in1);
    seqCircuit->mkOutput(eq);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(eq);
    bmc.prepareForSolving();

    bmc.setCurrentDepth(0);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

TEST(Z3BmcTest, Test05) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    auto type = NetTypeInfo::mkFloatType(32);
    Z3SeqNet in1 = seqNetStore->mkVariable("in1", type);
    Z3SeqNet in2 = seqNetStore->mkVariable("in2", type);
    Z3SeqNet eq = seqNetStore->mkEq(in1, in2);
    seqCircuit->mkInput(in1);
    seqCircuit->mkInput(in2);
    seqCircuit->mkOutput(eq);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(eq);
    bmc.prepareForSolving();

    bmc.setCurrentDepth(0);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

TEST(Z3BmcTest, MkLeqUint) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    auto t = NetTypeInfo::mkUintType(8);
    auto x = seqNetStore->mkVariable("x", t);
    auto n = seqNetStore->mkNumber("2", t);
    auto leq = seqNetStore->mkLeqIntUnsigned(x, n);
    auto eq = seqNetStore->mkEq(x, n);
    auto leqAndEq = seqNetStore->mkAnd(eq, leq);
    seqCircuit->mkInput(x);
    seqCircuit->mkOutput(leqAndEq);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(leqAndEq);
    bmc.prepareForSolving();

    bmc.setCurrentDepth(0);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

TEST(Z3BmcTest, MkBmcJumpsDepth01) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    auto t = NetTypeInfo::mkBooleanType();
    auto x = seqNetStore->mkVariable("x", t);
    seqCircuit->mkInput(x);

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(x);
    bmc.prepareForSolving();

    bmc.setCurrentDepth(3);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}

TEST(Z3BmcTest, MkBmcJumpsDepth02) {
    Z3Factory factory;
    auto seqNetStore = factory.buildSeqNetStore();
    auto seqCircuit = factory.buildSeqCircuit("");

    auto t = NetTypeInfo::mkBooleanType();
    auto x = seqNetStore->mkVariable("x", t);
    seqCircuit->mkLatch(x);
    seqCircuit->setLatchInitNext(x, seqNetStore->mkTrue(), seqNetStore->mkNot(x));

    Bmc<Z3SeqNet, Z3ComNet> bmc(&factory, seqNetStore.get(), seqCircuit.get());

    bmc.addTarget(x);
    bmc.prepareForSolving();

    bmc.setAllowTargetsOnlyAtLastDepth(false);
    bmc.setCurrentDepth(2);
    EngineResult result = bmc.findFirstReachableTarget();
    ASSERT_EQ(EngineResult::Reachable, result);
}



}  // namespace test
