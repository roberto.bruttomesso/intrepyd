#include "gtest/gtest.h"

#include "src/net/Z3Net.h"

namespace test {

class Z3Fixture : public ::testing::Test {
public:
    Z3Fixture() {
        Z3_config config = Z3_mk_config();
        context_ = Z3_mk_context(config);
        Z3_del_config(config);
    }

    virtual ~Z3Fixture() {
        Z3_del_context(context_);
        Z3_finalize_memory();
    }

    Z3_context getContext() const {
        return context_;
    }

protected:
    Z3_context context_;
};

}  // namespace test
