/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include "gtest/gtest.h"

#include <stdexcept>
#include <array>

#include "test/fixture/Z3Fixture.h"
#include "src/exception/IntrepidException.h"

namespace test {

class Z3 : public Z3Fixture {};

class Z3Solver : public Z3Fixture {
public:
    Z3Solver()
        : solver_(Z3_mk_solver(context_)) {
        Z3_solver_inc_ref(context_, solver_);
    }

    virtual ~Z3Solver() {
        Z3_solver_dec_ref(context_, solver_);
    }

protected:
    Z3_solver solver_;
};

#ifndef _MSC_VER
TEST_F(Z3Fixture, Error01) {
    Z3_set_error_handler(context_, [](Z3_context ctx, Z3_error_code c) {
        throw exception::IntrepidException("Exception", "filename", 0);
    });
    bool caugth = false;
    try {
        Z3_mk_numeral(context_, "a$r%!#", Z3_mk_real_sort(context_));
    } catch (exception::IntrepidException&) {
        caugth = true;
    }
    ASSERT_TRUE(caugth);
}
#endif

/*
 * Z3 generates always the same true and false ast values
 */
TEST_F(Z3, Test01) {
    ASSERT_EQ(Z3_mk_true(context_), Z3_mk_true(context_));
    ASSERT_EQ(Z3_mk_false(context_), Z3_mk_false(context_));
}

/*
 * Z3 does not simplify expressions by default
 */
TEST_F(Z3, Test02) {
    Z3_ast z3True = Z3_mk_true(context_);
    Z3_ast z3False = Z3_mk_false(context_);
    ASSERT_NE(z3False, Z3_mk_not(context_, z3True));
}

/*
 * Z3 simplifies expressions correctly
 */
TEST_F(Z3, Test03) {
    Z3_ast z3True = Z3_mk_true(context_);
    Z3_ast z3False = Z3_mk_false(context_);
    ASSERT_EQ(z3False, Z3_simplify(context_, Z3_mk_not(context_, z3True)));
}

/*
 * Create an enumeration in Z3
 */
TEST_F(Z3Solver, TestEnumeration) {
    const unsigned domainSize = 3;

    Z3_symbol enumSymb = Z3_mk_string_symbol(context_, "myEnum");

    std::array<Z3_symbol, domainSize> valueNames = { {
        Z3_mk_string_symbol(context_, "red"),
        Z3_mk_string_symbol(context_, "blue"),
        Z3_mk_string_symbol(context_, "green")
    } };

    std::array<Z3_func_decl, domainSize> valueConsts;
    std::array<Z3_func_decl, domainSize> valueTesters;

    Z3_mk_enumeration_sort(context_,
                           enumSymb,
                           domainSize,
                           valueNames.data(),
                           valueConsts.data(),
                           valueTesters.data());

    Z3_ast red = Z3_mk_app(context_, valueConsts[0], 0, nullptr);
    Z3_ast blue = Z3_mk_app(context_, valueConsts[1], 0, nullptr);
    Z3_ast green = Z3_mk_app(context_, valueConsts[2], 0, nullptr);

    Z3_ast red_eq_blue = Z3_mk_eq(context_, red, blue);
    Z3_ast red_eq_green = Z3_mk_eq(context_, red, green);
    Z3_ast green_eq_blue = Z3_mk_eq(context_, green, blue);

    Z3_lbool result;
    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, red_eq_blue);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_FALSE, result);

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, red_eq_green);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_FALSE, result);

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, green_eq_blue);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_FALSE, result);

    Z3_ast red_eq_red = Z3_mk_eq(context_, red, red);
    Z3_ast blue_eq_blue = Z3_mk_eq(context_, blue, blue);
    Z3_ast green_eq_green = Z3_mk_eq(context_, green, green);

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, red_eq_red);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_TRUE, result);

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, blue_eq_blue);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_TRUE, result);

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, green_eq_green);
    result = Z3_solver_check(context_, solver_);
    Z3_solver_pop(context_, solver_, 1);
    ASSERT_EQ(Z3_L_TRUE, result);
}

TEST_F(Z3Solver, Test01) {

    Z3_sort sort = Z3_mk_bool_sort(context_);

    Z3_symbol symb1 = Z3_mk_string_symbol(context_, "v1");
    Z3_ast v1 = Z3_mk_const(context_, symb1, sort);

    Z3_symbol symb2 = Z3_mk_string_symbol(context_, "v2");
    Z3_ast v2 = Z3_mk_const(context_, symb2, sort);

    Z3_ast iff = Z3_mk_iff(context_, v1, v2);
    Z3_solver_assert(context_, solver_, iff);
    Z3_lbool result = Z3_solver_check(context_, solver_);

    ASSERT_EQ(Z3_L_TRUE, result);

    Z3_model model = Z3_solver_get_model(context_, solver_);
    Z3_model_inc_ref(context_, model);

    Z3_ast iffEval, v1Eval, v2Eval;
    Z3_bool modelEvalRes = Z3_model_eval(context_, model, iff, Z3_FALSE, &iffEval);
    ASSERT_EQ(Z3_L_TRUE, modelEvalRes);
    ASSERT_EQ(Z3_mk_true(context_), iffEval);

    modelEvalRes = Z3_model_eval(context_, model, v1, Z3_FALSE, &v1Eval);
    ASSERT_EQ(Z3_L_TRUE, modelEvalRes);

    modelEvalRes = Z3_model_eval(context_, model, v2, Z3_FALSE, &v2Eval);
    ASSERT_EQ(Z3_L_TRUE, modelEvalRes);
    ASSERT_EQ(v2Eval, v1Eval);

    Z3_model_dec_ref(context_, model);
}

/*
 * Quantifier elimination 1
 */
TEST_F(Z3Solver, TestQE01) {
    Z3_tactic qe = Z3_mk_tactic(context_, "qe");
    Z3_tactic_inc_ref(context_, qe);
    Z3_goal elim = Z3_mk_goal(context_, false, false, false);
    Z3_goal_inc_ref(context_, elim);

    Z3_sort sort = Z3_mk_real_sort(context_);
    Z3_symbol symbx = Z3_mk_string_symbol(context_, "x");
    Z3_symbol symby = Z3_mk_string_symbol(context_, "y");
    Z3_ast x = Z3_mk_const(context_, symbx, sort);
    Z3_ast y = Z3_mk_const(context_, symby, sort);
    Z3_ast conj1 = Z3_mk_le(context_, y, Z3_mk_numeral(context_, "3", sort));
    Z3_ast conj2 = Z3_mk_le(context_, x, y);
    Z3_ast conj[] = { conj1, conj2 };
    Z3_ast formula = Z3_mk_and(context_, 2, conj);

    Z3_app vars[] = { Z3_to_app(context_, x) };
    Z3_ast existsFormula = Z3_mk_exists_const(context_, 0, 1, vars, 0, 0, formula);
    Z3_goal_assert(context_, elim, existsFormula);
    Z3_apply_result result = Z3_tactic_apply(context_, qe, elim);

    const unsigned nSubGoals =
    Z3_apply_result_get_num_subgoals(context_, result);
    ASSERT_EQ(1, nSubGoals);
    Z3_goal resGoal = Z3_apply_result_get_subgoal(context_, result, 0);
    ASSERT_EQ(1, Z3_goal_size(context_, resGoal));
    Z3_ast resFormula = Z3_goal_formula(context_, resGoal, 0);

    std::string strRes = Z3_ast_to_string(context_, resFormula);

    ASSERT_EQ("(<= y 3.0)", strRes);

    Z3_goal_dec_ref(context_, elim);
    Z3_tactic_dec_ref(context_, qe);
}

/*
 * Quantifier elimination 2
 */
TEST_F(Z3Solver, TestQE02) {
    Z3_tactic qe = Z3_mk_tactic(context_, "qe");
    Z3_tactic_inc_ref(context_, qe);
    Z3_goal elim = Z3_mk_goal(context_, false, false, false);
    Z3_goal_inc_ref(context_, elim);

    Z3_sort sort = Z3_mk_real_sort(context_);
    Z3_symbol symbx = Z3_mk_string_symbol(context_, "x");
    Z3_symbol symby = Z3_mk_string_symbol(context_, "y");
    Z3_ast x = Z3_mk_const(context_, symbx, sort);
    Z3_ast y = Z3_mk_const(context_, symby, sort);
    Z3_ast args[] = { x, y };
    Z3_ast plus = Z3_mk_add(context_, 2, args);
    Z3_ast f = Z3_mk_le(context_, Z3_mk_numeral(context_, "3", sort), plus);

    Z3_app vars[] = { Z3_to_app(context_, x), };
    Z3_ast existsFormula = Z3_mk_exists_const(context_, 0, 1, vars, 0, 0, f);
    Z3_goal_assert(context_, elim, existsFormula);
    Z3_apply_result result = Z3_tactic_apply(context_, qe, elim);

    const unsigned nSubGoals =
    Z3_apply_result_get_num_subgoals(context_, result);
    ASSERT_EQ(1, nSubGoals);
    Z3_goal resGoal = Z3_apply_result_get_subgoal(context_, result, 0);
    ASSERT_EQ(0, Z3_goal_size(context_, resGoal));

    Z3_goal_dec_ref(context_, elim);
    Z3_tactic_dec_ref(context_, qe);
}

TEST_F(Z3, TestOpt01) {
    Z3_optimize optContext = Z3_mk_optimize(context_);
    Z3_optimize_inc_ref(context_, optContext);
    Z3_sort sort = Z3_mk_bool_sort(context_);
    Z3_symbol symb1 = Z3_mk_string_symbol(context_, "v1");
    Z3_ast v1 = Z3_mk_const(context_, symb1, sort);
    Z3_symbol symb2 = Z3_mk_string_symbol(context_, "v2");
    Z3_ast v2 = Z3_mk_const(context_, symb2, sort);
    Z3_ast args[] = { v1, v2 };
    Z3_ast gate1 = Z3_mk_and(context_, 2, args);
    Z3_ast gate2 = Z3_mk_or(context_, 2, args);
    Z3_optimize_assert(context_, optContext, gate1);
    Z3_optimize_assert(context_, optContext, gate2);
    Z3_lbool result = Z3_optimize_check(context_, optContext);
    ASSERT_EQ(Z3_L_TRUE, result);
    Z3_model model = Z3_optimize_get_model(context_, optContext);
    Z3_ast gate1Eval, gate2Eval;
    Z3_bool modelEvalRes = Z3_model_eval(context_, model, gate1, Z3_FALSE, &gate1Eval);
    ASSERT_EQ(Z3_L_TRUE, modelEvalRes);
    modelEvalRes = Z3_model_eval(context_, model, gate2, Z3_FALSE, &gate2Eval);
    ASSERT_EQ(Z3_L_TRUE, modelEvalRes);
    ASSERT_EQ(Z3_mk_true(context_), gate1Eval);
    ASSERT_EQ(Z3_mk_true(context_), gate2Eval);
    Z3_optimize_dec_ref(context_, optContext);
}

TEST_F(Z3Solver, TestNonLinearArithmetic) {
    Z3_sort sort = Z3_mk_real_sort(context_);
    Z3_symbol symbx = Z3_mk_string_symbol(context_, "x");
    Z3_symbol symby = Z3_mk_string_symbol(context_, "y");
    Z3_ast x = Z3_mk_const(context_, symbx, sort);
    Z3_ast y = Z3_mk_const(context_, symby, sort);
    Z3_ast args[] = { x, y };
    Z3_ast xy = Z3_mk_mul(context_, 2, args);
    Z3_ast limit = Z3_mk_numeral(context_, "50.0", sort);
    Z3_ast target = Z3_mk_ge(context_, xy, limit);
    Z3_solver_assert(context_, solver_, target);
    Z3_lbool result = Z3_solver_check(context_, solver_);
    ASSERT_EQ(Z3_L_TRUE, result);
}

TEST_F(Z3Solver, TestUnsatCore01) {
    Z3_sort sort = Z3_mk_bool_sort(context_);
    Z3_symbol symbx = Z3_mk_string_symbol(context_, "x");
    Z3_symbol symby = Z3_mk_string_symbol(context_, "y");
    Z3_symbol symbz = Z3_mk_string_symbol(context_, "z");
    Z3_symbol symbxx = Z3_mk_string_symbol(context_, "xx");
    Z3_symbol symbyy = Z3_mk_string_symbol(context_, "yy");
    Z3_symbol symbzz = Z3_mk_string_symbol(context_, "zz");
    Z3_ast x = Z3_mk_const(context_, symbx, sort);
    Z3_ast y = Z3_mk_const(context_, symby, sort);
    Z3_ast z = Z3_mk_const(context_, symbz, sort);
    Z3_ast xx = Z3_mk_const(context_, symbxx, sort);
    Z3_ast yy = Z3_mk_const(context_, symbyy, sort);
    Z3_ast zz = Z3_mk_const(context_, symbzz, sort);
    Z3_ast not_x = Z3_mk_not(context_, x);
    Z3_ast not_y = Z3_mk_not(context_, y);
    Z3_ast args[] = { not_x, not_y };
    Z3_ast x_or_y = Z3_mk_or(context_, 2, args);
    Z3_ast xa[] = { x, xx };
    Z3_ast ya[] = { y, yy };
    Z3_ast za[] = { z, zz };
    Z3_ast conj[] = { x_or_y,
                      Z3_mk_or(context_, 2, xa),
                      Z3_mk_or(context_, 2, ya),
                      Z3_mk_or(context_, 2, za) };
    Z3_ast target = Z3_mk_and(context_, 4, conj);
    Z3_solver solver = Z3_mk_solver(context_);
    Z3_solver_inc_ref(context_, solver);
    Z3_solver_assert(context_, solver, target);
    Z3_ast assump[] = { Z3_mk_not(context_, xx),
                        Z3_mk_not(context_, yy),
                        Z3_mk_not(context_, zz) };
    Z3_lbool result = Z3_solver_check_assumptions(context_, solver, 3, assump);
    ASSERT_EQ(Z3_L_FALSE, result);
    Z3_ast_vector uc = Z3_solver_get_unsat_core(context_, solver);
    const unsigned size = Z3_ast_vector_size(context_, uc);
    ASSERT_EQ(2, size);
    Z3_solver_dec_ref(context_, solver);
}

TEST_F(Z3, TestNumbers01) {
    // This test is here to track a bug in Z3: numbers such as 1.0 / 10.0 are
    // not translated correctly :(
    //Z3_ast num = Z3_mk_numeral(context_, "1.1/10.1", Z3_mk_real_sort(context_));
    //ASSERT_EQ("0.1", std::string(Z3_ast_to_string(context_, num)));
}

TEST_F(Z3, TestNumbers02) {
    Z3_ast num = Z3_mk_numeral(context_, "0.0", Z3_mk_fpa_sort_32(context_));
    ASSERT_EQ("(_ +zero 8 24)", std::string(Z3_ast_to_string(context_, num)));
}

}  // namespace test
