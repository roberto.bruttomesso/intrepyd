/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_FACTORY_Z3FACTORY_H_
#define SRC_FACTORY_Z3FACTORY_H_

#include <string>

#include "src/factory/Factory.h"
#include "src/net/Z3SeqNet.h"
#include "src/net/Z3ComNet.h"
#include "src/net/Z3SeqNetStore.h"
#include "src/net/Z3ComNetStore.h"
#include "src/circuit/Z3SeqCircuit.h"
#include "src/circuit/Z3ComCircuit.h"
#include "src/engine/Z3SubstituteHelper.h"

namespace factory {

class Z3Factory final : public Factory<net::Z3SeqNet,
                                       net::Z3ComNet> {
 public:
    Z3Factory();
    ~Z3Factory();

    std::unique_ptr<net::NetStore<net::Z3SeqNet>>
                            buildSeqNetStore() override;
    std::unique_ptr<net::NetStore<net::Z3ComNet>>
                            buildComNetStore() override;
    std::unique_ptr<circuit::SeqCircuit<net::Z3SeqNet>>
                            buildSeqCircuit(const std::string& name) override;
    std::unique_ptr<circuit::ComCircuit<net::Z3ComNet>>
                            buildComCircuit(const std::string& name) override;
    std::unique_ptr<solver::Solver<net::Z3SeqNet>>
                            buildSeqSolver() override;
    std::unique_ptr<solver::Solver<net::Z3ComNet>>
                            buildComSolver(bool optimize) override;
    std::unique_ptr<engine::SubstituteHelper<net::Z3SeqNet>>
                            buildSubstituteHelper() override;
    std::unique_ptr<engine::QuantifierEliminator<net::Z3SeqNet>>
                            buildQuantifierEliminator() override;
    std::unique_ptr<engine::UnsatCoreExtractor<net::Z3SeqNet>>
                            buildSeqUnsatCoreExtractor() override;

 private:
    Z3_context context_;
};

}  // namespace factory

#endif  // SRC_FACTORY_Z3FACTORY_H_
