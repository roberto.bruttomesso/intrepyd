/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_FACTORY_FACTORY_H_
#define SRC_FACTORY_FACTORY_H_

#include <memory>
#include <string>

#include "src/net/NetStore.h"
#include "src/solver/Solver.h"
#include "src/circuit/ComCircuit.h"
#include "src/circuit/SeqCircuit.h"
#include "src/engine/SubstituteHelper.h"
#include "src/engine/QuantifierEliminator.h"
#include "src/engine/UnsatCoreExtractor.h"

namespace factory {

template<class SeqNet, class ComNet>
class Factory {
 public:
    virtual ~Factory() {}
    virtual std::unique_ptr<net::NetStore<SeqNet>> buildSeqNetStore() = 0;
    virtual std::unique_ptr<net::NetStore<ComNet>> buildComNetStore() = 0;
    virtual std::unique_ptr<circuit::SeqCircuit<SeqNet>>
                    buildSeqCircuit(const std::string& name) = 0;
    virtual std::unique_ptr<circuit::ComCircuit<ComNet>>
                    buildComCircuit(const std::string& name) = 0;
    virtual std::unique_ptr<solver::Solver<SeqNet>> buildSeqSolver() = 0;
    virtual std::unique_ptr<solver::Solver<ComNet>>
                    buildComSolver(bool optimize) = 0;
    virtual std::unique_ptr<engine::SubstituteHelper<SeqNet>>
                    buildSubstituteHelper() = 0;
    virtual std::unique_ptr<engine::QuantifierEliminator<SeqNet>>
                    buildQuantifierEliminator() = 0;
    virtual std::unique_ptr<engine::UnsatCoreExtractor<SeqNet>>
                    buildSeqUnsatCoreExtractor() = 0;
};

}  // namespace factory

#endif  // SRC_FACTORY_FACTORY_H_
