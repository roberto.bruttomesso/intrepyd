/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#include "src/factory/Z3Factory.h"
#include "src/circuit/Z3SeqCircuit.h"
#include "src/circuit/Z3ComCircuit.h"
#include "src/solver/Z3SeqSolver.h"
#include "src/solver/Z3ComSolver.h"
#include "src/engine/Z3SubstituteHelper.h"
#include "src/engine/Z3QuantifierEliminator.h"
#include "src/engine/Z3UnsatCoreExtractor.h"
#include "src/utils/Z3Utils.h"

namespace factory {

Z3Factory::Z3Factory() {
    Z3_config config = Z3_mk_config();
    context_ = Z3_mk_context(config);
    Z3_del_config(config);
    utils::Z3Utils::initialize();
}

Z3Factory::~Z3Factory() {
    Z3_del_context(context_);
    Z3_finalize_memory();
}

std::unique_ptr<net::NetStore<net::Z3SeqNet>>
Z3Factory::buildSeqNetStore() {
    return std::unique_ptr<net::NetStore<net::Z3SeqNet>>(
            new net::Z3SeqNetStore(context_));
}

std::unique_ptr<net::NetStore<net::Z3ComNet>>
Z3Factory::buildComNetStore() {
    return std::unique_ptr<net::NetStore<net::Z3ComNet>>(
            new net::Z3ComNetStore(context_));
}

std::unique_ptr<circuit::SeqCircuit<net::Z3SeqNet>>
Z3Factory::buildSeqCircuit(const std::string& name) {
    return std::unique_ptr<circuit::SeqCircuit<net::Z3SeqNet>>(
            new circuit::Z3SeqCircuit(name));
}

std::unique_ptr<circuit::ComCircuit<net::Z3ComNet>>
Z3Factory::buildComCircuit(const std::string& name) {
    return std::unique_ptr<circuit::ComCircuit<net::Z3ComNet>>(
            new circuit::Z3ComCircuit(name));
}

std::unique_ptr<solver::Solver<net::Z3SeqNet>>
Z3Factory::buildSeqSolver() {
    return std::unique_ptr<solver::Solver<net::Z3SeqNet>>(
            new solver::Z3SeqSolver(context_));
}

std::unique_ptr<solver::Solver<net::Z3ComNet>>
Z3Factory::buildComSolver(bool optimize) {
    if (optimize) {
        return std::unique_ptr<solver::Solver<net::Z3ComNet>>(
            new solver::Z3ComOptSolver(context_));
    }
    return std::unique_ptr<solver::Solver<net::Z3ComNet>>(
            new solver::Z3ComSolver(context_));
}

std::unique_ptr<engine::UnsatCoreExtractor<net::Z3SeqNet>>
Z3Factory::buildSeqUnsatCoreExtractor() {
    return std::unique_ptr<engine::UnsatCoreExtractor<net::Z3SeqNet>>(
            new engine::Z3UnsatCoreExtractor<net::Z3SeqNet>(context_));
}

std::unique_ptr<engine::SubstituteHelper<net::Z3SeqNet>>
Z3Factory::buildSubstituteHelper() {
    return std::unique_ptr<engine::SubstituteHelper<net::Z3SeqNet>>(
            new engine::Z3SubstituteHelper(context_));
}

std::unique_ptr<engine::QuantifierEliminator<net::Z3SeqNet>>
Z3Factory::buildQuantifierEliminator() {
    return std::unique_ptr<engine::QuantifierEliminator<net::Z3SeqNet>>(
            new engine::Z3QuantifierEliminator(context_));
}

}  // namespace factory
