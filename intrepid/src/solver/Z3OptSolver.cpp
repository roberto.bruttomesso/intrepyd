/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_SOLVER_Z3OPTSOLVER_CPP_
#define SRC_SOLVER_Z3OPTSOLVER_CPP_

#include <stdexcept>

#include "src/net/Z3NetUtils.h"
#include "src/utils/Z3Utils.h"
#include "src/solver/Z3OptSolver.h"

namespace solver {

template<class Net>
void
Z3OptSolver<Net>::addAssumption(Net t, const bool negate) {
    Z3_ast a = t.getZ3Ast();
    if (negate) {
        a = Z3_mk_not(context_, a);
    }
    Z3_optimize_assert(context_, optimize_, a);
}

template<class Net>
SolverResult
Z3OptSolver<Net>::solve() {
    Z3_optimize_push(context_, optimize_);
    lastResult_ = Z3_optimize_check(context_, optimize_);
    if (lastResult_ == Z3_L_TRUE) {
        lastModel_ = Z3_optimize_get_model(context_, optimize_);
        Z3_model_inc_ref(context_, lastModel_);
        modelsToDecRef_.push_back(lastModel_);
    }
    Z3_optimize_pop(context_, optimize_);
    return utils::Z3Utils::z3ResultToSolverResult(lastResult_);
}

template<class Net>
SolverResult
Z3OptSolver<Net>::solve(Net t) {
    Z3_ast a = t.getZ3Ast();
    Z3_optimize_push(context_, optimize_);
    Z3_optimize_assert(context_, optimize_, a);
    lastResult_ = Z3_optimize_check(context_, optimize_);
    if (lastResult_ == Z3_L_TRUE) {
        lastModel_ = Z3_optimize_get_model(context_, optimize_);
        Z3_model_inc_ref(context_, lastModel_);
        modelsToDecRef_.push_back(lastModel_);
    }
    Z3_optimize_pop(context_, optimize_);
    return utils::Z3Utils::z3ResultToSolverResult(lastResult_);
}

template<class Net>
SolverResult
Z3OptSolver<Net>::lastSolveResult() const {
    return utils::Z3Utils::z3ResultToSolverResult(lastResult_);
}

template<class Net>
Net
Z3OptSolver<Net>::evaluate(Net t) {
    assert(lastResult_ == Z3_L_TRUE);
    Z3_ast result = nullptr;
    Z3_ast ast = t.getZ3Ast();
#ifndef NDEBUG
    Z3_bool_opt evalRes =
#endif
    Z3_model_eval(context_, lastModel_, ast, Z3_L_FALSE, &result);
    assert(result != nullptr);
    assert(evalRes == Z3_TRUE);
    if (result == ast) {
        // This is essentially a don't care
        return t;
    }
    return net::Z3NetUtils::mkNetHelper<Net>(context_, result);
}

template<class Net>
void
Z3OptSolver<Net>::pushBacktrackPoint() {
    Z3_optimize_push(context_, optimize_);
}

template<class Net>
void
Z3OptSolver<Net>::popBacktrackPoint() {
    Z3_optimize_pop(context_, optimize_);
}

template<class Net>
void
Z3OptSolver<Net>::allSmtAddWatchedNet(Net t) {
    throw exception::IntrepidException("Unimplemented",
                                       __FILE__,
                                       __LINE__);
}

template<class Net>
void
Z3OptSolver<Net>::allSmtSetTarget(Net t) {
    throw exception::IntrepidException("Unimplemented",
                                       __FILE__,
                                       __LINE__);
}

template<class Net>
void
Z3OptSolver<Net>::allSmtClearWatchedNets() {
    throw exception::IntrepidException("Unimplemented",
                                       __FILE__,
                                       __LINE__);
}

template<class Net>
Net
Z3OptSolver<Net>::allSmtSolve(net::NetStore<Net>* store, bool block) {
    throw exception::IntrepidException("Unimplemented",
                                       __FILE__,
                                       __LINE__);
    return Net();
}

template<class Net>
void
Z3OptSolver<Net>::blockCube(Net cube) {
    exception::IntrepidException("Unimplemented",
                                 __FILE__,
                                 __LINE__);
}

}  // namespace solver

#endif  // SRC_SOLVER_Z3OPTSOLVER_CPP_
