/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_SOLVER_SOLVER_H_
#define SRC_SOLVER_SOLVER_H_

#include <vector>
#include <utility>
#include <unordered_set>

#include "src/net/NetStore.h"
#include "src/solver/SolverResult.h"

namespace solver {

template<class Net>
class Solver {
 protected:
    using Lit = std::pair<Net, bool>;
    using Cube = std::vector<Lit>;

 public:
    virtual ~Solver() {}

    virtual void addAssumption(Net t, const bool negate = false) = 0;
    virtual SolverResult solve() = 0;
    virtual SolverResult solve(Net t) = 0;
    virtual SolverResult lastSolveResult() const = 0;
    virtual Net evaluate(Net t) = 0;
    virtual void pushBacktrackPoint() = 0;
    virtual void popBacktrackPoint() = 0;

    virtual void allSmtAddWatchedNet(Net t) = 0;
    virtual void allSmtSetTarget(Net t) = 0;
    virtual Net allSmtSolve(net::NetStore<Net>* store, bool blockCube = true) = 0;
    virtual void blockCube(Net cube) = 0;
    virtual void allSmtClearWatchedNets() = 0;

    void addTarget(Net t);
    void clearTargets();
    Net getLastSatTarget() const;
    Net resolveTermItes(net::NetStore<Net>* store, Net net);

    SolverResult solveAnyTargets();

 protected:
    Net getCubeFromLastModel(net::NetStore<Net>* store);

    std::unordered_set<Net> watchedNets_;
    std::vector<Net> targets_;
    Net lastSatTarget_;
};

}  // namespace solver

#include "src/solver/Solver.cpp"

#endif  // SRC_SOLVER_SOLVER_H_
