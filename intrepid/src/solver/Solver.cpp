/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_SOLVER_SOLVER_CPP_
#define SRC_SOLVER_SOLVER_CPP_

#include <stdexcept>
#include <iostream>

#include "src/solver/Solver.h"

namespace solver {

template<class Net>
void
Solver<Net>::addTarget(Net t) {
    targets_.push_back(t);
}

template<class Net>
void
Solver<Net>::clearTargets() {
    targets_.clear();
}

template<class Net>
SolverResult
Solver<Net>::solveAnyTargets() {
    bool seenUnknown = false;
    for (auto t : targets_) {
        const SolverResult result = solve(t);
        switch (result) {
        case SolverResult::Satisfiable:
            lastSatTarget_ = t;
            return SolverResult::Satisfiable;
        case SolverResult::Unsatisfiable:
            continue;
        case SolverResult::Unknown:
            seenUnknown = true;
        default:
            assert(false);
            throw exception::IntrepidException("Unhandled result",
                                               __FILE__,
                                               __LINE__);
        }
    }
    if (seenUnknown) {
        return SolverResult::Unknown;
    }
    return SolverResult::Unsatisfiable;
}

template<class Net>
Net
Solver<Net>::getLastSatTarget() const {
    return lastSatTarget_;
}

template<class Net>
Net
Solver<Net>::
resolveTermItes(net::NetStore<Net>* store, Net n) {
    assert(store != nullptr);
    const unsigned size = store->getNofChildren(n);
    if (size == 0) {
        return n;
    }
    if (store->isIte(n)) {
        Net if_ = store->getChild(n, 0);
        Net then_ = store->getChild(n, 1);
        Net else_ = store->getChild(n, 2);
        Net value = evaluate(if_);
        if (store->isTrue(value)) {
            return resolveTermItes(store, then_);
        }
        if (store->isFalse(value)) {
            return resolveTermItes(store, else_);
        }
        // It's a don't care (an input?)
        // Return true (arbitrarily chosen)
        return resolveTermItes(store, then_);
    }
    auto kind = store->getNetKind(n);
    if (std::get<0>(kind) == net::NetKind::Extract) {
        Net resolvedChild = resolveTermItes(store, store->getChild(n, 0));
        return store->mkExtract(std::get<1>(kind), std::get<2>(kind), resolvedChild);
    }
    std::vector<Net> children;
    for (unsigned i = 0; i < size; i++) {
        Net resolvedChild = resolveTermItes(store, store->getChild(n, i));
        children.push_back(resolvedChild);
    }
    return store->mkNet(std::get<0>(kind), children);
}

template<class Net>
Net
Solver<Net>::getCubeFromLastModel(net::NetStore<Net>* store) {
    Net cube = store->mkTrue();
    for (auto net : watchedNets_) {
        Net value = evaluate(net);
        Net resolvedNet = resolveTermItes(store, net);
        if (store->isTrue(value)) {
            cube = store->mkAnd(cube, resolvedNet);
        } else if (store->isFalse(value)) {
            cube = store->mkAnd(cube, store->mkNot(resolvedNet));
        } else if (value == net) {
            // do nothing, boolean don't care
        } else {
            // A theory latch inside a predicate is don't care, skip
        }
    }
    return cube;
}

}  // namespace solver

#endif  // SRC_SOLVER_SOLVER_CPP_
