/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_SOLVER_Z3SOLVER_H_
#define SRC_SOLVER_Z3SOLVER_H_

#include <list>
#include <unordered_set>

#include "src/net/Z3Net.h"

#include "src/net/Z3NetStore.h"
#include "src/net/NetStore.h"
#include "src/solver/Solver.h"

namespace solver {

template<class Net>
class Z3Solver : public Solver<Net> {
 public:
    explicit Z3Solver(Z3_context context)
        : context_(context) {
        solver_ = Z3_mk_solver(context_);
        Z3_solver_inc_ref(context_, solver_);
        assert(solver_ != nullptr);
        lastResult_ = Z3_L_UNDEF;
    }

    virtual ~Z3Solver() {
        for (auto model : modelsToDecRef_) {
            Z3_model_dec_ref(context_, model);
        }
        Z3_solver_dec_ref(context_, solver_);
    }

    void addAssumption(Net t, const bool negate = false) override;
    SolverResult solve() override;
    SolverResult solve(Net t) override;
    SolverResult lastSolveResult() const override;
    Net evaluate(Net t) override;
    void pushBacktrackPoint() override;
    void popBacktrackPoint() override;

    void allSmtAddWatchedNet(Net t) override;
    void allSmtSetTarget(Net t) override;
    Net allSmtSolve(net::NetStore<Net>* store, bool blockCube = true) override;
    void blockCube(Net cube) override;
    void allSmtClearWatchedNets() override;

 protected:
    using Solver<Net>::watchedNets_;
    using Solver<Net>::getCubeFromLastModel;

    void blockCube(Z3_ast cube);

    Z3_context context_;
    Z3_solver solver_;
    Z3_lbool lastResult_;
    Z3_model lastModel_;
    std::list<Z3_model> modelsToDecRef_;
};

}  // namespace solver

#include "src/solver/Z3Solver.cpp"

#endif  // SRC_SOLVER_Z3SOLVER_H_
