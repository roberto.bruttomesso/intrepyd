#include "src/exception/IntrepidException.h"

namespace exception {

IntrepidException::IntrepidException(const std::string& message,
                                     const char* file,
                                     unsigned line) 
    : message_(message),
      file_(file),
      line_(line) {
    what_ = message_ + ", at " + file_ + ", line " + std::to_string(line_);
}

const char*
IntrepidException::getError() const {
    return what_.c_str();
}

}  // namespace exception
