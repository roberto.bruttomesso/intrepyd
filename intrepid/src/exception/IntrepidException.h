#ifndef SRC_EXCEPTION_INTREPID_EXCEPTION_H_
#define SRC_EXCEPTION_INTREPID_EXCEPTION_H_

#include <exception>
#include <string>

namespace exception {

class IntrepidException final : public std::exception {
public:
    IntrepidException(const std::string& message,
                      const char* file,
                      unsigned line);

    const char* getError() const;

private:
    const std::string message_;
    const std::string file_;
    const unsigned line_;
    std::string what_;
};

}  // namespace exception

#endif  // SRC_EXCEPTION_INTREPID_EXCEPTION_H_
