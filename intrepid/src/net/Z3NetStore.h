/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_Z3NETSTORE_H_
#define SRC_NET_Z3NETSTORE_H_

#include <unordered_set>
#include <vector>
#include <string>

#include "src/net/Z3Net.h"
#include "src/net/NetStore.h"

namespace net {

template<class Net>
class Z3NetStore : public NetStore<Net> {
 public:
    explicit Z3NetStore(Z3_context context);
    virtual ~Z3NetStore() {}

    Net mkTrue() const override;
    Net mkFalse() const override;

    Net mkNot(Net x) override;
    Net mkAnd(Net x, Net y) override;
    Net mkOr(Net x, Net y) override;
    Net mkXor(Net x, Net y) override;
    Net mkIff(Net x, Net y) override;
    Net mkMinus(Net x) override;
    Net mkSub(Net x, Net y) override;
    Net mkAdd(Net x, Net y) override;
    Net mkMul(Net x, Net y) override;
    Net mkDiv(Net x, Net y) override;
    Net mkMod(Net x, Net y) override;
    Net mkEq(Net x, Net y) override;
    Net mkLeqIntSigned(Net x, Net y) override;
    Net mkLeqIntUnsigned(Net x, Net y) override;
    Net mkLtIntSigned(Net x, Net y) override;
    Net mkLtIntUnsigned(Net x, Net y) override;
    Net mkLeqReal(Net x, Net y) override;
    Net mkLtReal(Net x, Net y) override;
    Net mkLeqFloat(Net x, Net y) override;
    Net mkLtFloat(Net x, Net y) override;
    Net mkIte(Net i, Net t, Net e) override;
    Net mkSubstitute(Net t, Net n, Net o) const override;
    Net mkRealFromFpa(Net n) override;

    Net mkEnumValue(const std::string& enumName,
                    const std::string& enumValue) override;
    NetTypeInfo mkUserEnum(const std::string& enumName,
                           const std::vector<std::string>& enumValues) override;
    Net mkFpaRounding() override;
    Net mkGetBit(Net x, unsigned bit) override;
    Net mkSetBit(Net x, unsigned bit, Net y) override;
    Net mkExtract(unsigned msb, unsigned lsb, Net x) override;

    bool isTrue(Net n) const override;
    bool isFalse(Net n) const override;
    bool isNumber(Net n) const override;
    bool isVariable(Net n) const override;
    bool isNot(Net x) const override;
    bool isMinus(Net x) const override;
    bool isAnd(Net x) const override;
    bool isOr(Net x) const override;
    bool isXor(Net x) const override;
    bool isIff(Net x) const override;
    bool isAdd(Net x) const override;
    bool isSub(Net x) const override;
    bool isMul(Net x) const override;
    bool isDiv(Net x) const override;
    bool isMod(Net x) const override;
    bool isEq(Net x) const override;
    bool isIte(Net x) const override;
    bool isFpaRounding(Net x) const override;

    unsigned getNofChildren(Net x) const override;
    Net getChild(Net x, unsigned i) const override;
    Net getFirstEnumValue(const std::string &enumName) const override;

    std::string toString(Net x) const override;

    Net substitute(Net net,
                   const std::vector<Net>& from,
                   const std::vector<Net>& to) const override;

    NetTypeInfo getNetTypeInfo(Net t) const override;
    unsigned getMsb(Net x) const override;
    unsigned getLsb(Net x) const override;

    bool isLeqIntSigned(Net x) const override;
    bool isLeqIntUnsigned(Net x) const override;
    bool isLeqFloat(Net x) const override;
    bool isLeqReal(Net x) const override;
    bool isGeqIntSigned(Net x) const override;
    bool isGeqIntUnsigned(Net x) const override;
    bool isGeqFloat(Net x) const override;
    bool isGeqReal(Net x) const override;
    bool isLtIntSigned(Net x) const override;
    bool isLtIntUnsigned(Net x) const override;
    bool isLtFloat(Net x) const override;
    bool isLtReal(Net x) const override;
    bool isGtIntSigned(Net x) const override;
    bool isGtIntUnsigned(Net x) const override;
    bool isGtFloat(Net x) const override;
    bool isGtReal(Net x) const override;
    bool isExtract(Net x) const override;
    bool isConcat(Net x) const override;

    bool isAtom(Net x) const override;

 protected:
    template<class Op> Net mkUnaryOp(Net x, Op op);
    template<class Op> Net mkBinaryOp(Net x, Net y, Op op);
    template<class Op> Net mkTernaryOp(Net x, Net y, Net z, Op op);
    template<class Op> Net mkNaryOp(Net x, Net y, Op op);
    template<class Op> Net mkUnaryOpFloat(Net x, Op op);
    template<class Op> Net mkBinaryOpFloat(Net x, Net y, Op op);
    template<class Op> Net mkTernaryOpFloat(Net x, Net y, Net z, Op op);
    template<class Op> Net mkNaryOpFloat(Net x, Net y, Op op);

    Net mkVariableImpl(const std::string& name, NetTypeInfo t) override;
    Net mkNumberImpl(const std::string& name, NetTypeInfo t) override;
    Net mkZeroExtend(unsigned i, Net x) override;
    Net mkSignExtend(unsigned i, Net x) override;

    template<class FInt, class FReal, class FFloat, class FInfInt>
    Net mkDispatcher(FInt fint, FReal freal, FFloat fdouble, FInfInt finfint, Net x);

    template<class FInt, class FReal, class FFloat, class FInfInt>
    Net mkDispatcher(FInt fint, FReal freal, FFloat fdouble, FInfInt finfint, Net x, Net y);

    Net mkMinusInt(Net x);
    Net mkMinusReal(Net x);
    Net mkMinusFloat(Net x);
    Net mkSubInt(Net x, Net y);
    Net mkSubReal(Net x, Net y);
    Net mkSubFloat(Net x, Net y);
    Net mkAddInt(Net x, Net y);
    Net mkAddReal(Net x, Net y);
    Net mkAddFloat(Net x, Net y);
    Net mkMulInt(Net x, Net y);
    Net mkMulReal(Net x, Net y);
    Net mkMulFloat(Net x, Net y);
    Net mkDivInt(Net x, Net y);
    Net mkDivReal(Net x, Net y);
    Net mkDivFloat(Net x, Net y);

    Z3_app getApp(Net x) const;
    Z3_func_decl getFuncDecl(Net x) const;
    Z3_decl_kind getFuncKind(Net x) const;

    const Z3_context context_;
    const Net true_;
    const Net false_;
    Z3_ast fpa_rounding_mode_;

    std::unordered_map<std::string,
                       std::vector<Z3_func_decl>> enumName2funcDecl_;
    std::unordered_set<Net> unsignedNets_;

 private:
     Net mkConcat(Net x, Net y) override;
};

}  // namespace net

#include "src/net/Z3NetStore.cpp"

#endif  // SRC_NET_Z3NETSTORE_H_
