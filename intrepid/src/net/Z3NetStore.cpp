/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_Z3NETSTORE_CPP_
#define SRC_NET_Z3NETSTORE_CPP_

#include <regex>
#include <sstream>
#include <vector>
#include <string>
#include <stdexcept>

#include "src/solver/Z3Solver.h"
#include "src/utils/Z3Utils.h"
#include "src/utils/SMTUtils.h"
#include "src/net/Z3NetStore.h"
#include "src/net/Z3NetUtils.h"

namespace net {

template<class Net>
Z3NetStore<Net>::Z3NetStore(Z3_context context)
    : context_(context),
      true_(Z3NetUtils::mkNetHelper<Net>(context, Z3_mk_true(context))),
      false_(Z3NetUtils::mkNetHelper<Net>(context, Z3_mk_false(context))) {
    assert(context != nullptr);
    fpa_rounding_mode_ = Z3_mk_fpa_round_nearest_ties_to_even(context);
    Z3_set_error_handler(context, [](Z3_context ctx, Z3_error_code c) {
        assert(false);
        throw exception::IntrepidException("Error from Z3",
                                         __FILE__,
                                         __LINE__);
    });
}

template<class Net>
Net
Z3NetStore<Net>::mkTrue() const {
    return true_;
}

template<class Net>
Net
Z3NetStore<Net>::mkFalse() const {
    return false_;
}

template<class Net>
Net
Z3NetStore<Net>::mkVariableImpl(const std::string& name, NetTypeInfo t) {
    Z3_sort sort = utils::Z3Utils::typeToSort(context_, t);
    assert(sort != nullptr);
    Z3_symbol symb = Z3_mk_string_symbol(context_, name.c_str());
    Z3_ast z3Var = Z3_mk_const(context_, symb, sort);
    Net result = Z3NetUtils::mkNetHelper<Net>(context_, z3Var);
    NetType nt = t.getNetType();
    if (nt == NetType::Uint) {
        unsignedNets_.insert(result);
    }
    return result;
}

template<class Net>
Net
Z3NetStore<Net>::mkNumberImpl(const std::string& value, NetTypeInfo t) {
    Z3_sort sort = utils::Z3Utils::typeToSort(context_, t);
    assert(sort != nullptr);
    Z3_ast numb = nullptr;
    if (value[0] == '#') {
        if (value[1] == 'x') {
            // bit-vector hex literal that needs to be converted to integer
            // Turn #NNNN into 0xNNNN
            std::string hexValue = value.substr(2);
            if (hexValue.size() > 16) {
                assert(false);
                exception::IntrepidException("Number value too big",
                                             __FILE__,
                                             __LINE__);
            }
            std::stringstream ss;
            ss << std::hex << hexValue;
            int64_t decValue;
            ss >> decValue;
            const std::string decValueStr(std::to_string(decValue));
            numb = Z3_mk_numeral(context_, decValueStr.c_str(), sort);
        } else if (value[1] == 'b') {
            // bit-vector bin literal that needs to be converted to integer
            // Turn #NNNN into 0xNNNN
            std::string binValue = value.substr(2);
            if (binValue.size() > 64) {
                assert(false);
                exception::IntrepidException("Number value too big",
                                             __FILE__,
                                             __LINE__);
            }
            char *pEnd;
            auto decValue = strtoull (value.substr(2).c_str(), &pEnd, 2);
            const std::string decValueStr(std::to_string(decValue));
            numb = Z3_mk_numeral(context_, decValueStr.c_str(), sort);
        } else {
            assert(false);
            exception::IntrepidException("Unknown number representation type",
                                         __FILE__,
                                         __LINE__);
        }
	} else if (value[0] == '(') {
        if (utils::SMTUtils::isFpNumber(value)) {
            bool sign;
            int32_t bexp;
            uint64_t sig;
            std::tie(sign, bexp, sig) = utils::SMTUtils::getFpNumberComponents(value);
            int bias, maxexp;
            if (t.getNetType() != NetType::Float) {
                assert(false);
                exception::IntrepidException("Float expected",
                                             __FILE__,
                                             __LINE__);
            }
            if (t.getSize() == 16) {
                bias = maxexp = 15;
            } else if (t.getSize() == 32) {
                bias = maxexp = 127;
            } else if (t.getSize() == 64) {
                bias = maxexp = 1023;
            } else {
                bias = 0; // fools warning
                assert(false);
                exception::IntrepidException("Unexpected float size",
                                             __FILE__,
                                             __LINE__);
            }
            const int exp = bexp - bias;
            assert(-maxexp - 1 <= exp);
            assert(exp <= maxexp);
            numb = Z3_mk_fpa_numeral_int64_uint64(context_,
                                                  sign,
                                                  exp,
                                                  sig,
                                                  sort);
        } else {
            const size_t size = value.size();
            assert(value[size - 1] == ')');
            assert(size >= 5);
            assert(value[1] == '-' || value[1] == '/');
            assert(value[2] == ' ');
            std::string adjValue = value;
            if (value[1] == '-') {
                // Negative decimal numbers
                // Rewrites numbers such as (- 1.0) into -1.0
                adjValue = value[1] + value.substr(3, size - 4);
            }
            else if (value[1] == '/') {
                // Rewrite (/ 1.0 2.0) into 1.0/2.0
                const std::regex pattern("\\(\\/ (\\-?[0-9]+)\\.0 (\\-?[0-9]+)\\.0\\)");
                std::smatch matches;
                if (!std::regex_match(value, matches, pattern) || matches.size() != 3) {
                    assert(false);
                    exception::IntrepidException("Wrong rational number",
                                                 __FILE__,
                                                 __LINE__);
                }
                adjValue = matches[1].str() + '/' + matches[2].str();
            }
            numb = Z3_mk_numeral(context_, adjValue.c_str(), sort);
        }
	} else {
        numb = Z3_mk_numeral(context_, value.c_str(), sort);
    }
    assert(numb != nullptr);
    return Z3NetUtils::mkNetHelper<Net>(context_, numb);
}

template<class Net>
Net
Z3NetStore<Net>::mkNot(Net x) {
    return mkUnaryOp(x, Z3_mk_not);
}

template<class Net>
Net
Z3NetStore<Net>::mkMinusInt(Net x) {
    return mkUnaryOp(x, Z3_mk_bvneg);
}

template<class Net>
Net
Z3NetStore<Net>::mkMinusReal(Net x) {
    return mkUnaryOp(x, Z3_mk_unary_minus);
}

template<class Net>
Net
Z3NetStore<Net>::mkMinusFloat(Net x) {
    return mkUnaryOp(x, Z3_mk_fpa_neg);
}

template<class Net>
Net
Z3NetStore<Net>::mkAnd(Net x, Net y) {
    return mkNaryOp(x, y, Z3_mk_and);
}

template<class Net>
Net
Z3NetStore<Net>::mkOr(Net x, Net y) {
    return mkNaryOp(x, y, Z3_mk_or);
}

template<class Net>
Net
Z3NetStore<Net>::mkXor(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_xor);
}

template<class Net>
Net
Z3NetStore<Net>::mkIff(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_iff);
}

template<class Net>
Net
Z3NetStore<Net>::mkEq(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_eq);
}

template<class Net>
Net
Z3NetStore<Net>::mkMinus(Net x) {
    auto fint = [this] (Net x) { return mkMinusInt(x); };
    auto freal = [this] (Net x) { return mkMinusReal(x); };
    auto fdouble = [this] (Net x) { return mkMinusFloat(x); };
    auto finfint = freal;
    return mkDispatcher(fint, freal, fdouble, finfint, x);
}

template<class Net>
Net
Z3NetStore<Net>::mkSub(Net x, Net y) {
    auto fint = [this] (Net x, Net y) { return mkSubInt(x, y); };
    auto freal = [this] (Net x, Net y) { return mkSubReal(x, y); };
    auto fdouble = [this] (Net x, Net y) { return mkSubFloat(x, y); };
    auto finfint = freal;
    return mkDispatcher(fint, freal, fdouble, finfint, x, y);
}

template<class Net>
Net
Z3NetStore<Net>::mkAdd(Net x, Net y) {
    auto fint = [this] (Net x, Net y) { return mkAddInt(x, y); };
    auto freal = [this] (Net x, Net y) { return mkAddReal(x, y); };
    auto fdouble = [this] (Net x, Net y) { return mkAddFloat(x, y); };
    auto finfint = freal;
    return mkDispatcher(fint, freal, fdouble, finfint, x, y);
}

template<class Net>
Net
Z3NetStore<Net>::mkMul(Net x, Net y) {
    auto fint = [this] (Net x, Net y) { return mkMulInt(x, y); };
    auto freal = [this] (Net x, Net y) { return mkMulReal(x, y); };
    auto ffloat = [this] (Net x, Net y) { return mkMulFloat(x, y); };
    auto finfint = freal;
    return mkDispatcher(fint, freal, ffloat, finfint, x, y);
}

template<class Net>
Net
Z3NetStore<Net>::mkDiv(Net x, Net y) {
    auto fint = [this] (Net x, Net y) { return mkDivInt(x, y); };
    auto freal = [this] (Net x, Net y) { return mkDivReal(x, y); };
    auto ffloat = [this] (Net x, Net y) { return mkDivFloat(x, y); };
    auto finfint = freal;
    return mkDispatcher(fint, freal, ffloat, finfint, x, y);
}

template<class Net>
Net
Z3NetStore<Net>::mkMod(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvsmod);
}

template<class Net>
Net
Z3NetStore<Net>::mkIte(Net i, Net t, Net e) {
    return mkTernaryOp(i, t, e, Z3_mk_ite);
}

template<class Net>
Net
Z3NetStore<Net>::mkExtract(unsigned msb, unsigned lsb, Net x) {
    Z3_ast res = Z3_mk_extract(context_, msb, lsb, x.getZ3Ast());
    /*
#ifndef NDEBUG
    Z3_sort sort = Z3_get_sort(context_, res);
    unsigned size = Z3_get_bv_sort_size(context_, sort);
    assert(size == 1 || size == 8 || size == 16 || size == 32);
#endif
    */
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
Net
Z3NetStore<Net>::mkZeroExtend(unsigned i, Net x) {
    Z3_ast res = Z3_mk_zero_ext(context_, i, x.getZ3Ast());
#ifndef NDEBUG
    Z3_sort sort = Z3_get_sort(context_, res);
    unsigned size = Z3_get_bv_sort_size(context_, sort);
    assert(size == 8 || size == 16 || size == 32);
#endif
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
Net
Z3NetStore<Net>::mkSignExtend(unsigned i, Net x) {
    Z3_ast res = Z3_mk_sign_ext(context_, i, x.getZ3Ast());
#ifndef NDEBUG
    Z3_sort sort = Z3_get_sort(context_, res);
    unsigned size = Z3_get_bv_sort_size(context_, sort);
    assert(size == 8 || size == 16 || size == 32);
#endif
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
Net
Z3NetStore<Net>::mkGetBit(Net x, unsigned bit) {
    Z3_ast ext = Z3_mk_extract(context_, bit, bit, x.getZ3Ast());
    Z3_sort sort = Z3_mk_bv_sort(context_, 1);
    Z3_ast one = Z3_mk_numeral(context_, "1", sort);
    Z3_ast res = Z3_mk_eq(context_, ext, one);
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
Net
Z3NetStore<Net>::mkSetBit(Net x, unsigned bit, Net y) {
    Z3_sort sort = Z3_mk_bv_sort(context_, 1);
    Z3_ast zero = Z3_mk_numeral(context_, "0", sort);
    Z3_ast one = Z3_mk_numeral(context_, "1", sort);
    Z3_ast ite = Z3_mk_ite(context_, y.getZ3Ast(), one, zero);
    NetTypeInfo nti = getNetTypeInfo(x);
    const unsigned size = nti.getSize();
    Z3_ast result = nullptr;
    if (bit == 0) {
        Z3_ast ext = Z3_mk_extract(context_, size - 1, 1, x.getZ3Ast());
        result = Z3_mk_concat(context_, ext, ite);
    }
    else if (bit == size - 1) {
        Z3_ast ext = Z3_mk_extract(context_, size - 2, 0, x.getZ3Ast());
        result = Z3_mk_concat(context_, ite, ext);
    }
    else {
        Z3_ast extHigh = Z3_mk_extract(context_, size - 1, bit + 1, x.getZ3Ast());
        Z3_ast extLow = Z3_mk_extract(context_, bit - 1, 0, x.getZ3Ast());
        result = Z3_mk_concat(context_,
                              extHigh,
                              Z3_mk_concat(context_,
                                           ite,
                                           extLow));
    }
    return Z3NetUtils::mkNetHelper<Net>(context_, result);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkUnaryOp(Net x, Op op) {
    Z3_ast res = op(context_, x.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkBinaryOp(Net x, Net y, Op op) {
    Z3_ast res = op(context_, x.getZ3Ast(), y.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkTernaryOp(Net x, Net y, Net z, Op op) {
    Z3_ast res = op(context_, x.getZ3Ast(), y.getZ3Ast(), z.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkNaryOp(Net x, Net y, Op op) {
    std::vector<Z3_ast> args;
    args.push_back(x.getZ3Ast());
    args.push_back(y.getZ3Ast());
    Z3_ast res = op(context_, static_cast<unsigned>(args.size()), args.data());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkUnaryOpFloat(Net x, Op op) {
    Z3_ast res = op(context_, fpa_rounding_mode_, x.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkBinaryOpFloat(Net x, Net y, Op op) {
    Z3_ast res = op(context_, fpa_rounding_mode_, x.getZ3Ast(), y.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkTernaryOpFloat(Net x, Net y, Net z, Op op) {
    Z3_ast res = op(context_, fpa_rounding_mode_,
                    x.getZ3Ast(), y.getZ3Ast(), z.getZ3Ast());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class Op>
Net
Z3NetStore<Net>::mkNaryOpFloat(Net x, Net y, Op op) {
    std::vector<Z3_ast> args;
    args.push_back(x.getZ3Ast());
    args.push_back(y.getZ3Ast());
    Z3_ast res = op(context_, fpa_rounding_mode_,
                    static_cast<unsigned>(args.size()), args.data());
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
template<class FInt, class FReal, class FFloat, class FInfInt>
Net
Z3NetStore<Net>::mkDispatcher(FInt fint, FReal freal, FFloat ffloat, FInfInt finfint, Net x) {
    NetType xt = getNetTypeInfo(x).getNetType();
    switch (xt) {
    case NetType::Int:
    case NetType::Uint:
        return fint(x);
    case NetType::Real:
        return freal(x);
    case NetType::Float:
        return ffloat(x);
    case NetType::InfInt:
        return finfint(x);
    default:
        throw exception::IntrepidException("Unexpected operand type",
                                           __FILE__,
                                           __LINE__);
    }
    return Net();
}

template<class Net>
template<class FInt, class FReal, class FFloat, class FInfInt>
Net
Z3NetStore<Net>::mkDispatcher(FInt fint, FReal freal, FFloat ffloat, FInfInt finfint,
                              Net x, Net y) {
    NetType xt = getNetTypeInfo(x).getNetType();
#ifndef NDEBUG
    // NetType yt = getNetTypeInfo(y).getNetType();
#endif
    // This assertion is not true for a uint variable and int number
    // assert(xt == yt);
    switch (xt) {
    case NetType::Int:
    case NetType::Uint:
        return fint(x, y);
    case NetType::Real:
        return freal(x, y);
     case NetType::Float:
        return ffloat(x, y);
     case NetType::InfInt:
        return finfint(x, y);
    default:
        throw exception::IntrepidException("Unexpected operand type",
                                           __FILE__,
                                           __LINE__);
    }
    return Net();
}

template<class Net>
Net
Z3NetStore<Net>::mkSubInt(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvsub);
}

template<class Net>
Net
Z3NetStore<Net>::mkSubReal(Net x, Net y) {
    return mkNaryOp(x, y, Z3_mk_sub);
}

template<class Net>
Net
Z3NetStore<Net>::mkSubFloat(Net x, Net y) {
    return mkBinaryOpFloat(x, y, Z3_mk_fpa_sub);
}

template<class Net>
Net
Z3NetStore<Net>::mkAddInt(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvadd);
}

template<class Net>
Net
Z3NetStore<Net>::mkAddReal(Net x, Net y) {
    return mkNaryOp(x, y, Z3_mk_add);
}

template<class Net>
Net
Z3NetStore<Net>::mkAddFloat(Net x, Net y) {
    return mkBinaryOpFloat(x, y, Z3_mk_fpa_add);
}

template<class Net>
Net
Z3NetStore<Net>::mkMulInt(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvmul);
}

template<class Net>
Net
Z3NetStore<Net>::mkMulReal(Net x, Net y) {
    return mkNaryOp(x, y, Z3_mk_mul);
}

template<class Net>
Net
Z3NetStore<Net>::mkMulFloat(Net x, Net y) {
    return mkBinaryOpFloat(x, y, Z3_mk_fpa_mul);
}

template<class Net>
Net
Z3NetStore<Net>::mkDivInt(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvsdiv);
}

template<class Net>
Net
Z3NetStore<Net>::mkDivReal(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_div);
}

template<class Net>
Net
Z3NetStore<Net>::mkDivFloat(Net x, Net y) {
    return mkBinaryOpFloat(x, y, Z3_mk_fpa_div);
}

template<class Net>
Net
Z3NetStore<Net>::mkLeqIntSigned(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvsle);
}

template<class Net>
Net
Z3NetStore<Net>::mkLeqIntUnsigned(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvule);
}

template<class Net>
Net
Z3NetStore<Net>::mkLtIntSigned(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvslt);
}

template<class Net>
Net
Z3NetStore<Net>::mkLtIntUnsigned(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_bvult);
}

template<class Net>
Net
Z3NetStore<Net>::mkLeqReal(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_le);
}

template<class Net>
Net
Z3NetStore<Net>::mkLtReal(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_lt);
}

template<class Net>
Net
Z3NetStore<Net>::mkLeqFloat(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_fpa_leq);
}

template<class Net>
Net
Z3NetStore<Net>::mkLtFloat(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_fpa_lt);
}

template<class Net>
Net
Z3NetStore<Net>::mkEnumValue(const std::string& enumName,
                             const std::string& enumValue) {
    Net result;
    auto it = enumName2funcDecl_.find(enumName);;
    if (it == enumName2funcDecl_.end()) {
        throw exception::IntrepidException(
                    "Enum " + enumName + " not declared",
                    __FILE__, __LINE__);
    }
    const std::vector<Z3_func_decl>& enumFuncDecl = it->second;
    bool found = false;
    for (auto z3FuncDecl : enumFuncDecl) {
        Z3_symbol symb = Z3_get_decl_name(context_, z3FuncDecl);
        Z3_string name = Z3_get_symbol_string(context_, symb);
        if (std::string(name) == enumValue) {
            found = true;
            result = Z3NetUtils::mkNetHelper<Net>(context_,
                                                  Z3_mk_app(context_,
                                                            z3FuncDecl,
                                                            0,
                                                            nullptr));
            break;
        }
    }
    if (!found) {
        throw exception::IntrepidException(
                    "Enum value " + enumValue + " not declared",
                    __FILE__, __LINE__);
    }
    return result;
}

template<class Net>
NetTypeInfo
Z3NetStore<Net>::mkUserEnum(const std::string& enumName,
                            const std::vector<std::string>& enumValues) {
    if (enumName2funcDecl_.find(enumName) != enumName2funcDecl_.end()) {
        throw exception::IntrepidException("Enum already declared",
                                           __FILE__,
                                           __LINE__);
    }
    enumName2funcDecl_.insert(
                std::make_pair(enumName,
                               utils::Z3Utils::mkEnumFuncDecl(context_,
                                                              enumName,
                                                              enumValues)));
    return NetTypeInfo::mkUserEnumType(enumName);
}

template<class Net>
Net
Z3NetStore<Net>::mkFpaRounding() {
    return Z3NetUtils::mkNetHelper<Net>(context_, fpa_rounding_mode_);
}

template<class Net>
Net
Z3NetStore<Net>::mkRealFromFpa(Net n) {
    Z3_ast an = n.getZ3Ast();
    Z3_ast res = Z3_mk_fpa_to_real(context_, an);
    return Z3NetUtils::mkNetHelper<Net>(context_, res);
}

template<class Net>
Z3_app
Z3NetStore<Net>::getApp(Net x) const {
    Z3_ast ax = x.getZ3Ast();
    assert(Z3_is_app(context_, ax));
    return Z3_to_app(context_, ax);
}

template<class Net>
Z3_func_decl
Z3NetStore<Net>::getFuncDecl(Net x) const {
    Z3_app app = getApp(x);
    return Z3_get_app_decl(context_, app);
}

template<class Net>
Z3_decl_kind
Z3NetStore<Net>::getFuncKind(Net x) const {
    Z3_func_decl decl = getFuncDecl(x);
    return Z3_get_decl_kind(context_, decl);
}

template<class Net>
bool
Z3NetStore<Net>::isTrue(Net x) const {
    return getFuncKind(x) == Z3_OP_TRUE;
}

template<class Net>
bool
Z3NetStore<Net>::isFalse(Net x) const {
    return getFuncKind(x) == Z3_OP_FALSE;
}

template<class Net>
bool
Z3NetStore<Net>::isVariable(Net x) const {
    Z3_func_decl decl = getFuncDecl(x);
    return Z3_get_arity(context_, decl) == 0 && !isNumber(x);
}

template<class Net>
bool
Z3NetStore<Net>::isNumber(Net x) const {
    Z3_ast ax = x.getZ3Ast();
    return Z3_is_numeral_ast(context_, ax) != 0;
}

template<class Net>
bool
Z3NetStore<Net>::isNot(Net x) const {
    return getFuncKind(x) == Z3_OP_NOT;
}

template<class Net>
bool
Z3NetStore<Net>::isMinus(Net x) const {
    return getFuncKind(x) == Z3_OP_UMINUS ||
           getFuncKind(x) == Z3_OP_BNEG ||
           getFuncKind(x) == Z3_OP_FPA_NEG;
}

template<class Net>
bool
Z3NetStore<Net>::isAnd(Net x) const {
    return getFuncKind(x) == Z3_OP_AND;
}

template<class Net>
bool
Z3NetStore<Net>::isOr(Net x) const {
    return getFuncKind(x) == Z3_OP_OR;
}

template<class Net>
bool
Z3NetStore<Net>::isXor(Net x) const {
    return getFuncKind(x) == Z3_OP_XOR;
}

template<class Net>
bool
Z3NetStore<Net>::isIff(Net x) const {
    return getFuncKind(x) == Z3_OP_IFF;
}

template<class Net>
bool
Z3NetStore<Net>::isEq(Net x) const {
    return getFuncKind(x) == Z3_OP_EQ;
}

template<class Net>
bool
Z3NetStore<Net>::isLeqIntSigned(Net x) const {
    return getFuncKind(x) == Z3_OP_SLEQ;
}

template<class Net>
bool
Z3NetStore<Net>::isGeqIntSigned(Net x) const {
    return getFuncKind(x) == Z3_OP_SGEQ;
}

template<class Net>
bool
Z3NetStore<Net>::isLeqIntUnsigned(Net x) const {
    return getFuncKind(x) == Z3_OP_ULEQ;
}

template<class Net>
bool
Z3NetStore<Net>::isGeqIntUnsigned(Net x) const {
    return getFuncKind(x) == Z3_OP_UGEQ;
}

template<class Net>
bool
Z3NetStore<Net>::isLeqFloat(Net x) const {
    return getFuncKind(x) == Z3_OP_FPA_LE;
}

template<class Net>
bool
Z3NetStore<Net>::isGeqFloat(Net x) const {
    return getFuncKind(x) == Z3_OP_FPA_GE;
}

template<class Net>
bool
Z3NetStore<Net>::isLeqReal(Net x) const {
    return getFuncKind(x) == Z3_OP_LE;
}

template<class Net>
bool
Z3NetStore<Net>::isGeqReal(Net x) const {
    return getFuncKind(x) == Z3_OP_GE;
}

template<class Net>
bool
Z3NetStore<Net>::isLtIntSigned(Net x) const {
    return getFuncKind(x) == Z3_OP_SLT;
}

template<class Net>
bool
Z3NetStore<Net>::isGtIntSigned(Net x) const {
    return getFuncKind(x) == Z3_OP_SGT;
}

template<class Net>
bool
Z3NetStore<Net>::isLtIntUnsigned(Net x) const {
    return getFuncKind(x) == Z3_OP_ULT;
}

template<class Net>
bool
Z3NetStore<Net>::isGtIntUnsigned(Net x) const {
    return getFuncKind(x) == Z3_OP_UGT;
}

template<class Net>
bool
Z3NetStore<Net>::isLtFloat(Net x) const {
    return getFuncKind(x) == Z3_OP_FPA_LT;
}

template<class Net>
bool
Z3NetStore<Net>::isGtFloat(Net x) const {
    return getFuncKind(x) == Z3_OP_FPA_GT;
}

template<class Net>
bool
Z3NetStore<Net>::isLtReal(Net x) const {
    return getFuncKind(x) == Z3_OP_LT;
}

template<class Net>
bool
Z3NetStore<Net>::isGtReal(Net x) const {
    return getFuncKind(x) == Z3_OP_GT;
}

template<class Net>
bool
Z3NetStore<Net>::isAdd(Net x) const {
    return getFuncKind(x) == Z3_OP_ADD ||
           getFuncKind(x) == Z3_OP_BADD ||
           getFuncKind(x) == Z3_OP_FPA_ADD;
}

template<class Net>
bool
Z3NetStore<Net>::isSub(Net x) const {
    return getFuncKind(x) == Z3_OP_SUB ||
           getFuncKind(x) == Z3_OP_BSUB ||
           getFuncKind(x) == Z3_OP_FPA_SUB;
}

template<class Net>
bool
Z3NetStore<Net>::isMul(Net x) const {
    return getFuncKind(x) == Z3_OP_MUL ||
           getFuncKind(x) == Z3_OP_BMUL ||
           getFuncKind(x) == Z3_OP_FPA_MUL;
}

template<class Net>
bool
Z3NetStore<Net>::isDiv(Net x) const {
    return getFuncKind(x) == Z3_OP_DIV ||
           getFuncKind(x) == Z3_OP_BSDIV ||
           getFuncKind(x) == Z3_OP_BSDIV_I ||
           getFuncKind(x) == Z3_OP_FPA_DIV;
}

template<class Net>
bool
Z3NetStore<Net>::isMod(Net x) const {
    return getFuncKind(x) == Z3_OP_BSMOD ||
           getFuncKind(x) == Z3_OP_BSMOD_I;
}

template<class Net>
bool
Z3NetStore<Net>::isIte(Net x) const {
    return getFuncKind(x) == Z3_OP_ITE;
}

template<class Net>
bool
Z3NetStore<Net>::isExtract(Net x) const {
    return getFuncKind(x) == Z3_OP_EXTRACT;
}

template<class Net>
bool
Z3NetStore<Net>::isConcat(Net x) const {
    return getFuncKind(x) == Z3_OP_CONCAT;
}

template<class Net>
bool
Z3NetStore<Net>::isFpaRounding(Net x) const {
    Z3_ast ax = x.getZ3Ast();
    return ax == fpa_rounding_mode_;
}

template<class Net>
unsigned
Z3NetStore<Net>::getNofChildren(Net x) const {
    Z3_app app = getApp(x);
    return Z3_get_app_num_args(context_, app);
}

template<class Net>
Net
Z3NetStore<Net>::getChild(Net x, unsigned i) const {
    assert(i < getNofChildren(x));
    Z3_app app = getApp(x);
    Z3_ast ret = Z3_get_app_arg(context_, app, i);
    return Z3NetUtils::mkNetHelper<Net>(context_, ret);
}

template<class Net>
Net
Z3NetStore<Net>::getFirstEnumValue(const std::string &enumName) const {
    auto it = enumName2funcDecl_.find(enumName);;
    if (it == enumName2funcDecl_.end()) {
        throw exception::IntrepidException(
                    "Enum " + enumName + " not declared",
                    __FILE__, __LINE__);
    }
    const std::vector<Z3_func_decl>& enumFuncDecl = it->second;
    assert(enumFuncDecl.size() > 0);
    return Z3NetUtils::mkNetHelper<Net>(context_,
                                        Z3_mk_app(context_,
                                                  enumFuncDecl[0],
                                                  0,
                                                  nullptr));
}

template<class Net>
std::string
Z3NetStore<Net>::toString(Net x) const {
    Z3_ast ax = x.getZ3Ast();
    return utils::Z3Utils::toString(context_, ax);
}

template<class Net>
Net
Z3NetStore<Net>::mkSubstitute(Net t, Net n, Net o) const {
    std::vector<Net> to = { n };
    std::vector<Net> from = { o };
    return substitute(t, from, to);
}

template<class Net>
Net
Z3NetStore<Net>::substitute(Net net,
                            const std::vector<Net>& from,
                            const std::vector<Net>& to) const {
    std::vector<Z3_ast> vars, terms;
    for (auto n : from) { vars.push_back(n.getZ3Ast()); }
    for (auto n : to) { terms.push_back(n.getZ3Ast()); }
    Z3_ast resultAst = Z3_substitute(context_, net.getZ3Ast(),
                                     (unsigned)vars.size(),
                                     vars.data(),
                                     terms.data());
    return net::Z3NetUtils::mkNetHelper<Net>(context_, resultAst);
}

template<class Net>
NetTypeInfo
Z3NetStore<Net>::getNetTypeInfo(Net net) const {
    Z3_ast ast = net.getZ3Ast();
    Z3_sort sort = Z3_get_sort(context_, ast);
    NetTypeInfo nti = utils::Z3Utils::sortToType(context_, sort);
    NetType nt = nti.getNetType();
    if (nt == NetType::Int) {
        if (unsignedNets_.find(net) != unsignedNets_.end()) {
            switch (nt) {
            case NetType::Int:
                return NetTypeInfo::mkUintType(nti.getSize());
            default:
                throw exception::IntrepidException("Unreachable",
                                                   __FILE__,
                                                   __LINE__);
            }
        }
    }
    return nti;
}

template<class Net>
unsigned
Z3NetStore<Net>::getMsb(Net x) const {
    Z3_func_decl decl = getFuncDecl(x);
    return Z3_get_decl_int_parameter(context_, decl, 0);
}

template<class Net>
unsigned
Z3NetStore<Net>::getLsb(Net x) const {
    Z3_func_decl decl = getFuncDecl(x);
    return Z3_get_decl_int_parameter(context_, decl, 1);
}

template<class Net>
Net
Z3NetStore<Net>::mkConcat(Net x, Net y) {
    return mkBinaryOp(x, y, Z3_mk_concat);
}

template<class Net>
bool
Z3NetStore<Net>::isAtom(Net x) const {
    if (getNetTypeInfo(x).getNetType() != NetType::Boolean) {
        return false;
    }
    // Boolean variables are atoms
    if (isVariable(x)) {
        return true;
    }
    // Constants are not atoms
    if (getNofChildren(x) == 0) {
        return false;
    }
    // Predicates over non booleans are atoms
    return getNetTypeInfo(getChild(x, 0)).getNetType() != NetType::Boolean;
}

}  // namespace net

#endif  // SRC_NET_Z3NETSTORE_CPP_
