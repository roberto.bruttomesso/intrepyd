/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_NETTYPEINFO_H_
#define SRC_NET_NETTYPEINFO_H_

#include <string>
#include <unordered_map>
#include <ostream>
#include <cassert>

#include "src/net/NetType.h"
#include "src/exception/IntrepidException.h"

namespace net {

class NetTypeInfo final {
 public:
    NetTypeInfo operator=(const NetTypeInfo& type) {
        netType_ = type.getNetType();
        size_ = type.getSize();
        if (netType_ == NetType::UserEnum) {
            userEnum_ = type.getUserEnum();
        }
        return *this;
    }

    bool operator==(const NetTypeInfo& type) const {
        if (netType_ == NetType::UserEnum) {
            return netType_ == type.getNetType() &&
                   userEnum_ == type.getUserEnum();
        }
        return netType_ == type.getNetType() && size_ == type.getSize();
    }

    bool operator!=(const NetTypeInfo& type) const {
        return !(*this == type);
    }

    friend std::ostream& operator<<(std::ostream& os, const NetTypeInfo& self) {
        switch (self.netType_) {
            case NetType::Boolean:
                os << "Boolean";
                break;
            case NetType::Int:
                os << "Int";
                if (self.size_ != 0) {
                    os << self.size_;
                }
                break;
            case NetType::Uint:
                os << "Uint";
                if (self.size_ != 0) {
                    os << self.size_;
                }
                break;
            case NetType::Float:
                os << "Float";
                if (self.size_ != 0) {
                    os << self.size_;
                }
                break;
            case NetType::InfInt:
                os << "Int";
                break;
            case NetType::Real:
                os << "Real";
                break;
            default:
                assert(false);
                throw exception::IntrepidException("Not an valid type",
                                                   __FILE__,
                                                   __LINE__);
        }
        return os;
    }

    static NetTypeInfo mkUnknownType() {
        return NetTypeInfo(NetType::Unknown, 0, "");
    }

    static NetTypeInfo mkNativeType(NetType t, const unsigned size = 0) {
        return NetTypeInfo(t, size, "");
    }

    static NetTypeInfo mkBooleanType() {
        return NetTypeInfo(NetType::Boolean, 0, "");
    }

    static NetTypeInfo mkIntType(const unsigned size = 0) {
        return NetTypeInfo(NetType::Int, size, "");
    }

    static NetTypeInfo mkUintType(const unsigned size = 0) {
        return NetTypeInfo(NetType::Uint, size, "");
    }

    static NetTypeInfo mkFloatType(const unsigned size = 0) {
        return NetTypeInfo(NetType::Float, size, "");
    }

    static NetTypeInfo mkRealType() {
        return NetTypeInfo(NetType::Real, 0, "");
    }

    static NetTypeInfo mkInfIntType() {
        return NetTypeInfo(NetType::InfInt, 0, "");
    }

    static NetTypeInfo mkUserEnumType(const std::string& userEnum) {
        return NetTypeInfo(NetType::UserEnum, 0, userEnum);
    }

    NetType getNetType() const {
        return netType_;
    }

    bool isNativeType() const {
        return netType_ == NetType::Boolean ||
               netType_ == NetType::Int     ||
               netType_ == NetType::Uint    ||
               netType_ == NetType::Real    ||
               netType_ == NetType::InfInt  ||
               netType_ == NetType::Float;
    }

    std::string getMaxInt() const {
        switch (netType_) {
        case NetType::Int:
            return std::to_string((1 << (size_ - 1)) - 1);
        case NetType::Uint:
            return std::to_string((1 << size_) - 1);
        default:
            assert(false);
            throw exception::IntrepidException("Not an integer type",
                                               __FILE__,
                                               __LINE__);
        }
        return "";
    }

    unsigned getSize() const {
        return size_;
    }

    bool isSignedInteger() const {
        return netType_ == NetType::Int;
    }

    bool isFloatingPoint() const {
        return netType_ == NetType::Float;
    }

    const std::string& getUserEnum() const {
        assert(netType_ == NetType::UserEnum);
        return userEnum_;
    }

 private:
    NetTypeInfo(const NetType netType,
                const unsigned size,
                const std::string& userEnum)
        : netType_(netType),
          size_(size),
          userEnum_(userEnum)
    {}

    NetType netType_;
    unsigned size_;
    std::string userEnum_;
};

}  // namespace net

#endif  // SRC_NET_NETTYPEINFO_H_
