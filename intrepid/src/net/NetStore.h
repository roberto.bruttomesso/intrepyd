/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_NETSTORE_H_
#define SRC_NET_NETSTORE_H_

#include <string>
#include <memory>
#include <vector>

#include "src/net/NetTypeInfo.h"
#include "src/net/NetKind.h"
#include "src/net/NetType.h"

namespace net {

template<class Net>
class NetStore {
 public:
    using NetDataType = Net;

 public:
    virtual ~NetStore() {}

    virtual Net mkTrue() const = 0;
    virtual Net mkFalse() const = 0;

    Net mkVariable(const std::string& name, NetTypeInfo t);
    Net mkNumber(const std::string& name, NetTypeInfo t);

    bool isLeq(Net x) const;
    bool isLt(Net x) const;
    bool isGeq(Net x) const;
    bool isGt(Net x) const;

    virtual Net mkNot(Net x) = 0;
    virtual Net mkMinus(Net x) = 0;
    virtual Net mkAnd(Net x, Net y) = 0;
    virtual Net mkOr(Net x, Net y) = 0;
    virtual Net mkXor(Net x, Net y) = 0;
    virtual Net mkIff(Net x, Net y) = 0;
    virtual Net mkAdd(Net x, Net y) = 0;
    virtual Net mkSub(Net x, Net y) = 0;
    virtual Net mkMul(Net x, Net y) = 0;
    virtual Net mkDiv(Net x, Net y) = 0;
    virtual Net mkMod(Net x, Net y) = 0;
    virtual Net mkEq(Net x, Net y) = 0;
    virtual Net mkLeqReal(Net x, Net y) = 0;
    virtual Net mkLtReal(Net x, Net y) = 0;
    virtual Net mkLeqFloat(Net x, Net y) = 0;
    virtual Net mkLtFloat(Net x, Net y) = 0;
    virtual Net mkLeqIntSigned(Net x, Net y) = 0;
    virtual Net mkLtIntSigned(Net x, Net y) = 0;
    virtual Net mkLeqIntUnsigned(Net x, Net y) = 0;
    virtual Net mkLtIntUnsigned(Net x, Net y) = 0;
    virtual Net mkIte(Net i, Net t, Net e) = 0;
    virtual Net mkExtract(unsigned msb, unsigned lsb, Net x) = 0;
    virtual Net mkSignExtend(unsigned i, Net x) = 0;
    virtual Net mkZeroExtend(unsigned i, Net x) = 0;
    virtual Net mkGetBit(Net x, unsigned bit) = 0;
    virtual Net mkSetBit(Net x, unsigned bit, Net y) = 0;
    virtual Net mkEnumValue(const std::string& enumName,
                            const std::string& enumValue) = 0;
    virtual Net mkSubstitute(Net t, Net n, Net o) const = 0;
    virtual NetTypeInfo
                mkUserEnum(const std::string& enumName,
                           const std::vector<std::string>& enumValues) = 0;
    virtual Net mkFpaRounding() = 0;
    virtual Net mkRealFromFpa(Net n) = 0;
    virtual bool isTrue(Net n) const = 0;
    virtual bool isFalse(Net n) const = 0;
    virtual bool isNumber(Net n) const = 0;
    virtual bool isVariable(Net n) const = 0;
    virtual bool isNot(Net x) const = 0;
    virtual bool isMinus(Net x) const = 0;
    virtual bool isAnd(Net x) const = 0;
    virtual bool isOr(Net x) const = 0;
    virtual bool isXor(Net x) const = 0;
    virtual bool isIff(Net x) const = 0;
    virtual bool isAdd(Net x) const = 0;
    virtual bool isSub(Net x) const = 0;
    virtual bool isMul(Net x) const = 0;
    virtual bool isDiv(Net x) const = 0;
    virtual bool isMod(Net x) const = 0;
    virtual bool isEq(Net x) const = 0;
    virtual bool isIte(Net x) const = 0;
    virtual bool isLeqIntSigned(Net x) const = 0;
    virtual bool isLeqIntUnsigned(Net x) const = 0;
    virtual bool isGeqIntSigned(Net x) const = 0;
    virtual bool isGeqIntUnsigned(Net x) const = 0;
    virtual bool isLeqFloat(Net x) const = 0;
    virtual bool isGeqFloat(Net x) const = 0;
    virtual bool isLeqReal(Net x) const = 0;
    virtual bool isGeqReal(Net x) const = 0;
    virtual bool isLtIntSigned(Net x) const = 0;
    virtual bool isLtIntUnsigned(Net x) const = 0;
    virtual bool isGtIntSigned(Net x) const = 0;
    virtual bool isGtIntUnsigned(Net x) const = 0;
    virtual bool isLtFloat(Net x) const = 0;
    virtual bool isGtFloat(Net x) const = 0;
    virtual bool isLtReal(Net x) const = 0;
    virtual bool isGtReal(Net x) const = 0;
    virtual bool isFpaRounding(Net x) const = 0;
    virtual bool isExtract(Net x) const = 0;
    virtual bool isConcat(Net x) const = 0;

    virtual bool isAtom(Net x) const = 0;

    virtual unsigned getNofChildren(Net x) const = 0;
    virtual Net getChild(Net x, unsigned i) const = 0;
    virtual Net getFirstEnumValue(const std::string& enumName) const = 0;
    virtual unsigned getMsb(Net x) const = 0;
    virtual unsigned getLsb(Net x) const = 0;

    virtual std::string toString(Net x) const = 0;

    virtual Net substitute(Net net,
                           const std::vector<Net>& from,
                           const std::vector<Net>& to) const = 0;

    virtual NetTypeInfo getNetTypeInfo(Net x) const = 0;

    Net mkGeqIntSigned(Net x, Net y);
    Net mkGtIntSigned(Net x, Net y);
    Net mkGeqIntUnsigned(Net x, Net y);
    Net mkGtIntUnsigned(Net x, Net y);
    Net mkGeqReal(Net x, Net y);
    Net mkGtReal(Net x, Net y);
    Net mkGeqFloat(Net x, Net y);
    Net mkGtFloat(Net x, Net y);
    Net mkNet(NetKind kind, Net x);
    Net mkNet(NetKind kind, Net x, Net y);
    Net mkNet(NetKind kind, Net z, Net x, Net y);
    Net mkNet(NetKind kind, const std::vector<Net>& args);

    std::tuple<NetKind, unsigned, unsigned> getNetKind(Net x) const;
    Net getDefaultInitFromType(NetTypeInfo t);
    void adjustNumberTypes(Net* x, Net* y);
    Net castToType(Net x, NetTypeInfo& newType);
    bool isConcrete(Net x) const;

 private:
    virtual Net mkVariableImpl(const std::string& name, NetTypeInfo t) = 0;
    virtual Net mkNumberImpl(const std::string& name, NetTypeInfo t) = 0;
    virtual Net mkConcat(Net x, Net y) = 0;
};

}  // namespace net

#include "src/net/NetStore.cpp"

#endif  // SRC_NET_NETSTORE_H_
