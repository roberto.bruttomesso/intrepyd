/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_NETUTILS_H_
#define SRC_NET_NETUTILS_H_

#include "src/net/NetKind.h"

namespace net {

class NetUtils final {
 public:
    static bool isRelationalOperator(net::NetKind kind) {
        return kind == net::NetKind::Eq ||
               kind == net::NetKind::LeqIntSigned ||
               kind == net::NetKind::LeqIntUnsigned ||
               kind == net::NetKind::LeqReal ||
               kind == net::NetKind::LeqFloat ||
               kind == net::NetKind::LtIntSigned ||
               kind == net::NetKind::LtIntUnsigned ||
               kind == net::NetKind::LtReal ||
               kind == net::NetKind::LtFloat ||
               kind == net::NetKind::GeqIntSigned ||
               kind == net::NetKind::GeqIntUnsigned ||
               kind == net::NetKind::GeqReal ||
               kind == net::NetKind::GeqFloat ||
               kind == net::NetKind::GtIntSigned ||
               kind == net::NetKind::GtIntUnsigned ||
               kind == net::NetKind::GtReal ||
               kind == net::NetKind::GtFloat;
    }
};

}  // namespace net

#endif  // SRC_NET_NETUTILS_H_
