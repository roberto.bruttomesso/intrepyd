/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_NETSTORE_CPP_
#define SRC_NET_NETSTORE_CPP_

#include <string>
#include <cassert>
#include <iostream>

#include "src/exception/IntrepidException.h"
#include "src/net/NetStore.h"

namespace net {

template<class Net>
Net
NetStore<Net>::mkVariable(const std::string& name, NetTypeInfo t) {
    return mkVariableImpl(name, t);
}

template<class Net>
Net
NetStore<Net>::mkNumber(const std::string& name, NetTypeInfo t) {
    return mkNumberImpl(name, t);
}

template<class Net>
bool
NetStore<Net>::isLeq(Net x) const {
    return isLeqReal(x) ||
           isLeqIntSigned(x) ||
           isLeqIntUnsigned(x) ||
           isLeqFloat(x);
}

template<class Net>
bool
NetStore<Net>::isGeq(Net x) const {
    return isGeqReal(x) ||
           isGeqIntSigned(x) ||
           isGeqIntUnsigned(x) ||
           isGeqFloat(x);
}

template<class Net>
bool
NetStore<Net>::isLt(Net x) const {
    return isLtReal(x) ||
           isLtIntSigned(x) ||
           isLtIntUnsigned(x) ||
           isLtFloat(x);
}

template<class Net>
bool
NetStore<Net>::isGt(Net x) const {
    return isGtReal(x) ||
           isGtIntSigned(x) ||
           isGtIntUnsigned(x) ||
           isGtFloat(x);
}

template<class Net>
Net
NetStore<Net>::mkNet(NetKind kind, Net x) {
    switch (kind) {
    case NetKind::Not:
        return mkNot(x);
    case NetKind::Minus:
        return mkMinus(x);
    default:
        assert(false);
        throw exception::IntrepidException("Unhandled unary kind",
                                           __FILE__,
                                           __LINE__);
    }
    // Unreachable
    return x;
}

template<class Net>
Net
NetStore<Net>::mkNet(NetKind kind, Net x, Net y) {
    if (kind != NetKind::Concat) {
        adjustNumberTypes(&x, &y);
    }

    switch (kind) {
    case NetKind::And:
        return mkAnd(x, y);
    case NetKind::Or:
        return mkOr(x, y);
    case NetKind::Iff:
        return mkIff(x, y);
    case NetKind::Eq:
        return mkEq(x, y);
    case NetKind::Xor:
        return mkXor(x, y);
    case NetKind::Sub:
        return mkSub(x, y);
    case NetKind::Add:
        return mkAdd(x, y);
    case NetKind::Mul:
        return mkMul(x, y);
    case NetKind::Div:
        return mkDiv(x, y);
    case NetKind::Mod:
        return mkMod(x, y);
    case NetKind::LeqReal:
        return mkLeqReal(x, y);
    case NetKind::GeqReal:
        return mkGeqReal(x, y);
    case NetKind::LtReal:
        return mkLtReal(x, y);
    case NetKind::GtReal:
        return mkGtReal(x, y);
    case NetKind::LeqFloat:
        return mkLeqFloat(x, y);
    case NetKind::GeqFloat:
        return mkGeqFloat(x, y);
    case NetKind::LtFloat:
        return mkLtFloat(x, y);
    case NetKind::GtFloat:
        return mkGtFloat(x, y);
    case NetKind::LeqIntSigned:
        return mkLeqIntSigned(x, y);
    case NetKind::LeqIntUnsigned:
        return mkLeqIntUnsigned(x, y);
    case NetKind::Concat:
        return mkConcat(x, y);
    default:
        assert(false);
        throw exception::IntrepidException("Unhandled binary kind",
                                           __FILE__,
                                           __LINE__);
    }
    // Unreachable
    return Net();
}

template<class Net>
Net
NetStore<Net>::mkNet(NetKind kind, Net z, Net x, Net y) {
    adjustNumberTypes(&x, &y);

    switch (kind) {
    case NetKind::Ite:
        return mkIte(z, x, y);
    case NetKind::Or:
        return mkOr(x, mkOr(y, z));
    case NetKind::And:
        return mkAnd(x, mkAnd(y, z));
    case NetKind::Mul:
        return mkMul(x, mkMul(y, z));
    case NetKind::Add:
        return mkAdd(x, mkAdd(y, z));
    default:
        assert(false);
        throw exception::IntrepidException(
            "Unhandled ternary kind",
            __FILE__,
            __LINE__);
    }
    // Unreachable
    return Net();
}

template<class Net>
Net
NetStore<Net>::mkNet(NetKind kind, const std::vector<Net>& args) {
    const auto size = args.size();
    assert(size > 0);
    unsigned firstIndex = 0;
    if (isFpaRounding(args[0])) {
        firstIndex = 1;
    }
    switch (size - firstIndex) {
    case 1:
        return mkNet(kind, args[firstIndex]);
    case 2:
        return mkNet(kind, args[firstIndex],
                           args[firstIndex + 1]);
    case 3:
        return mkNet(kind, args[firstIndex],
                           args[firstIndex + 1],
                           args[firstIndex + 2]);
    }
    Net result = args[firstIndex];
    for (unsigned i = firstIndex + 1; i < size; i++) {
        result = mkNet(kind, result, args[i]);
    }
    return result;
}

template<class Net>
void
NetStore<Net>::adjustNumberTypes(Net* x, Net* y) {
    if (!isNumber(*x) && !isNumber(*y)) {
        return;
    }
    NetTypeInfo xti = getNetTypeInfo(*x);
    NetTypeInfo yti = getNetTypeInfo(*y);
    assert(xti.isNativeType());
    assert(yti.isNativeType());
    NetType xt = xti.getNetType();
    NetType yt = yti.getNetType();
    if (xt == yt) {
      return;
    }
    if (isNumber(*x)) {
        *x = castToType(*x, yti);
    } else {
        assert(isNumber(*y));
        *y = castToType(*y, xti);
    }
}

template<class Net>
std::tuple<NetKind, unsigned, unsigned>
NetStore<Net>::getNetKind(Net x) const {
    if (isAnd(x)) {
        return {NetKind::And, 0, 0};
    }
    if (isOr(x)) {
        return {NetKind::Or, 0, 0};
    }
    if (isNot(x)) {
        return {NetKind::Not, 0, 0};
    }
    if (isMinus(x)) {
        return {NetKind::Minus, 0, 0};
    }
    if (isAdd(x)) {
        return {NetKind::Add, 0, 0};
    }
    if (isSub(x)) {
        return {NetKind::Sub, 0, 0};
    }
    if (isMul(x)) {
        return {NetKind::Mul, 0, 0};
    }
    if (isDiv(x)) {
        return {NetKind::Div, 0, 0};
    }
    if (isMod(x)) {
        return {NetKind::Mod, 0, 0};
    }
    if (isXor(x)) {
        return {NetKind::Xor, 0, 0};
    }
    if (isIff(x)) {
        return {NetKind::Iff, 0, 0};
    }
    if (isEq(x)) {
        return {NetKind::Eq, 0, 0};
    }
    if (isVariable(x)) {
        return {NetKind::Variable, 0, 0};
    }
    if (isNumber(x)) {
        return {NetKind::Number, 0, 0};
    }
    if (isLeqIntSigned(x)) {
        return {NetKind::LeqIntSigned, 0, 0};
    }
    if (isLeqIntUnsigned(x)) {
        return {NetKind::LeqIntUnsigned, 0, 0};
    }
    if (isLeqFloat(x)) {
        return {NetKind::LeqFloat, 0, 0};
    }
    if (isLeqReal(x)) {
        return {NetKind::LeqReal, 0, 0};
    }
    if (isLtFloat(x)) {
        return {NetKind::LtFloat, 0, 0};
    }
    if (isGeqIntSigned(x)) {
        return {NetKind::GeqIntSigned, 0, 0};
    }
    if (isGeqIntUnsigned(x)) {
        return {NetKind::GeqIntUnsigned, 0, 0};
    }
    if (isGeqFloat(x)) {
        return {NetKind::GeqFloat, 0, 0};
    }
    if (isGeqReal(x)) {
        return {NetKind::GeqReal, 0, 0};
    }
    if (isGtFloat(x)) {
        return {NetKind::GtFloat, 0, 0};
    }
    if (isIte(x)) {
        return {NetKind::Ite, 0, 0};
    }
    if (isExtract(x)) {
        return {NetKind::Extract, getMsb(x), getLsb(x)};
    }
    if (isConcat(x)) {
        return {NetKind::Concat, 0, 0};
    }
    const std::string xStr = toString(x);
    assert(false);
    throw exception::IntrepidException("Unhandled net " + xStr,
                                       __FILE__,
                                       __LINE__);
    return {NetKind::Unknown, 0, 0};
}

template<class Net>
Net
NetStore<Net>::mkGeqIntSigned(Net x, Net y) {
    return mkLeqIntSigned(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGtIntSigned(Net x, Net y) {
    return mkLtIntSigned(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGeqIntUnsigned(Net x, Net y) {
    return mkLeqIntUnsigned(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGtIntUnsigned(Net x, Net y) {
    return mkLtIntUnsigned(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGeqReal(Net x, Net y) {
    return mkLeqReal(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGtReal(Net x, Net y) {
    return mkLtReal(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGeqFloat(Net x, Net y) {
    return mkLeqFloat(y, x);
}

template<class Net>
Net
NetStore<Net>::mkGtFloat(Net x, Net y) {
    return mkLtFloat(y, x);
}

template<class Net>
Net
NetStore<Net>::castToType(Net x, NetTypeInfo& newTypeInfo) {
    assert(newTypeInfo.isNativeType());
    NetTypeInfo curTypeInfo = getNetTypeInfo(x);
#ifndef NDEBUG
    NetType newType = newTypeInfo.getNetType();
    NetType currentType = curTypeInfo.getNetType();
#endif
    assert(currentType != newType || curTypeInfo.getSize() != newTypeInfo.getSize());
    auto extend = static_cast<int>(curTypeInfo.getSize() - newTypeInfo.getSize());
    Net result = x;
    if (extend > 0) {
        result = mkExtract(newTypeInfo.getSize() - 1, 0, x);
    } else if (extend < 0) {
        if (newTypeInfo.isSignedInteger()) {
            result = mkSignExtend(-extend, x);
        } else {
            result = mkZeroExtend(-extend, x);
        }
    }
    return result;
}

template<class Net>
Net
NetStore<Net>::getDefaultInitFromType(NetTypeInfo t) {
    switch (t.getNetType()) {
    case net::NetType::Boolean:
        return mkFalse();
    case net::NetType::Int:
    case net::NetType::Uint:
    case net::NetType::Float:
    case net::NetType::Real:
    case net::NetType::InfInt:
        return mkNumber("0", t);
    case net::NetType::UserEnum:
        return getFirstEnumValue(t.getUserEnum());
    default:
        assert(false);
        throw exception::IntrepidException("Unhandled type",
                                           __FILE__,
                                           __LINE__);
    }
    return Net();
}

template<class Net>
bool
NetStore<Net>::isConcrete(Net x) const {
    assert(getNetTypeInfo(x).isNativeType());
    return isTrue(x) || isFalse(x) || isNumber(x);
}

}  // namespace net

#endif  // SRC_NET_NETSTORE_CPP_
