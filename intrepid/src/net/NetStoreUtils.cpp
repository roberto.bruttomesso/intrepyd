/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_NET_NETSTOREUTILS_CPP_
#define SRC_NET_NETSTOREUTILS_CPP_

#include <cassert>
#include <string>

#include "src/net/NetStoreUtils.h"
#include "src/exception/IntrepidException.h"

namespace net {

template<class SeqNet, class ComNet>
ComNet
NetStoreUtils<SeqNet, ComNet>::
translateConstantOrNumber(SeqNet seqNet,
                          const NetStore<SeqNet>& seqNetStore,
                          NetStore<ComNet>* comNetStore) {
    assert(comNetStore != nullptr);
    ComNet result;
    const std::string name = seqNetStore.toString(seqNet);
    const net::NetTypeInfo type = seqNetStore.getNetTypeInfo(seqNet);
    if (seqNetStore.isTrue(seqNet)) {
        result = comNetStore->mkTrue();
    } else if (seqNetStore.isFalse(seqNet)) {
        result = comNetStore->mkFalse();
    } else if (seqNetStore.isNumber(seqNet)) {
        result = comNetStore->mkNumber(name, type);
    } else {
        assert(false);
        throw exception::IntrepidException("Unhandled case",
                                           __FILE__,
                                           __LINE__);
    }
    return result;
}

}  // namespace net

#endif  // SRC_NET_NETSTOREUTILS_CPP_
