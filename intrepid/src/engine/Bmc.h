/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_BMC_H_
#define SRC_ENGINE_BMC_H_

#include <vector>
#include <unordered_map>
#include <memory>
#include <string>

#include "src/factory/Factory.h"
#include "src/solver/Solver.h"
#include "src/engine/Engine.h"
#include "src/net/NetStore.h"
#include "src/circuit/SeqCircuit.h"
#include "src/circuit/ComCircuit.h"
#include "src/circuit/Unroller.h"

namespace engine {

template<class SeqNet, class ComNet>
class Bmc : public Engine<SeqNet, ComNet> {
 public:
    Bmc(factory::Factory<SeqNet, ComNet>* factory,
        net::NetStore<SeqNet>* seqNetStore,
        circuit::SeqCircuit<SeqNet>* seqCircuit);

    Bmc(const Bmc& bmc) = delete;
    Bmc operator=(const Bmc& bmc) = delete;

    void setCurrentDepth(const unsigned currentDepth);
    void setOptimize(const bool optimize);
    void setUseAttackPathAxioms(SeqNet source, SeqNet target);
    void setInduction(const bool induction);
    void setAllowTargetsOnlyAtLastDepth(const bool value);
    void addAssumptionAtDepth(SeqNet n, unsigned depth);

 private:
    void initializeSolver();
    std::shared_ptr<Trace<SeqNet, ComNet>> computeTrace() const;

    bool verifyWithSimulation(const Trace<SeqNet, ComNet>& cex) const;
    void extendTrace(Trace<SeqNet, ComNet>* cex) const;

    void prepareForSolvingImpl() override;
    EngineResult findFirstReachableTargetImpl() override;
    solver::SolverResult checkInductive();
    void blockCex(const Trace<SeqNet, ComNet>& cex);
    void afterRemoveTarget() override;
    void allowTargetsOnlyAtLastDepth();
    void addInitialStates();
    bool findEqualStates(unsigned* depth1, unsigned* depth2);
    void blockEqualStates(const unsigned depth1, const unsigned depth2);
    std::string computeStateSignature(const unsigned depth);

    using Engine<SeqNet, ComNet>::seqNetStore_;
    using Engine<SeqNet, ComNet>::seqCircuit_;
    using Engine<SeqNet, ComNet>::lastReachedTargets_;
    using Engine<SeqNet, ComNet>::targets_;
    using Engine<SeqNet, ComNet>::watchedNets_;
    using Engine<SeqNet, ComNet>::targetToTrace_;
    using Engine<SeqNet, ComNet>::factory_;
    using Engine<SeqNet, ComNet>::verbosity_;

    std::unique_ptr<circuit::Unroller<SeqNet, ComNet>> unroller_;
    std::unique_ptr<net::NetStore<ComNet>> comNetStore_;
    std::unique_ptr<circuit::ComCircuit<ComNet>> comCircuit_;
    unsigned currentDepth_;
    bool solverInitialized_;
    bool optimize_;
    bool induction_;
    bool allowTargetsOnlyAtLastDepth_;
    bool attackPathAxioms_;
    SeqNet source_;
    SeqNet target_;
    std::unique_ptr<solver::Solver<ComNet>> comSolver_;
    std::vector<SeqNet> depthToAssumption_;
};

}  // namespace engine

#include "src/engine/Bmc.cpp"

#endif  // SRC_ENGINE_BMC_H_
