/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_ENGINE_H_
#define SRC_ENGINE_ENGINE_H_

#include <vector>
#include <memory>

#include "src/factory/Factory.h"
#include "src/net/NetStore.h"
#include "src/circuit/SeqCircuit.h"
#include "src/engine/Trace.h"
#include "src/engine/EngineResult.h"

namespace engine {

template<class SeqNet, class ComNet>
class Engine {
 public:
    Engine(factory::Factory<SeqNet, ComNet>* factory,
           net::NetStore<SeqNet>* seqNetStore,
           circuit::SeqCircuit<SeqNet>* seqCircuit);
    virtual ~Engine() {}

    EngineResult findFirstReachableTarget();
    void prepareForSolving();
    void addTarget(SeqNet t);
    void addWatchedNet(SeqNet t);
    void removeLastReachedTargets();
    size_t getNumberOfRemainingTargets() const;
    const std::vector<SeqNet>& getLastReachedTargets() const;
    const Trace<SeqNet, ComNet>&
                            getTraceForTarget(SeqNet t) const;
    std::shared_ptr<Trace<SeqNet, ComNet>>
                            getTracePtrForTarget(SeqNet t) const;
    const circuit::SeqCircuit<SeqNet>& getCircuit() const;
    void setVerbosity(unsigned v);

 protected:
    virtual EngineResult findFirstReachableTargetImpl() = 0;
    virtual void prepareForSolvingImpl() = 0;

    void removeTarget(SeqNet t);
    bool isPrepared() const;

    factory::Factory<SeqNet, ComNet>* factory_;
    net::NetStore<SeqNet>* seqNetStore_;
    circuit::SeqCircuit<SeqNet>* seqCircuit_;
    std::vector<SeqNet> targets_;
    std::vector<SeqNet> watchedNets_;
    std::vector<SeqNet> lastReachedTargets_;
    std::unordered_map<SeqNet,
                       std::shared_ptr<Trace<SeqNet, ComNet>>>
                                    targetToTrace_;
    unsigned verbosity_ {0};

 private:
    virtual void afterRemoveTarget() = 0;
    bool prepared_;
};

}  // namespace engine

#include "src/engine/Engine.cpp"

#endif  // SRC_ENGINE_ENGINE_H_
