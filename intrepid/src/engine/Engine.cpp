/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_ENGINE_CPP_
#define SRC_ENGINE_ENGINE_CPP_

#include <cassert>
#include <vector>

#include "src/exception/IntrepidException.h"
#include "src/engine/Engine.h"

namespace engine {

template<class SeqNet, class ComNet>
Engine<SeqNet, ComNet>::
Engine(factory::Factory<SeqNet, ComNet>* factory,
       net::NetStore<SeqNet>* seqNetStore,
       circuit::SeqCircuit<SeqNet>* seqCircuit)
    : factory_(factory),
      seqNetStore_(seqNetStore),
      seqCircuit_(seqCircuit),
      prepared_(false)
{}

template<class SeqNet, class ComNet>
EngineResult
Engine<SeqNet, ComNet>::
findFirstReachableTarget() {
    if (targets_.empty()) {
        throw exception::IntrepidException(
                    "Could not find any target to solve",
                    __FILE__, __LINE__);
    }
    if (!isPrepared()) {
        throw exception::IntrepidException("Engine was not initialized",
                                           __FILE__,
                                           __LINE__);
    }
    return findFirstReachableTargetImpl();
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
prepareForSolving() {
    if (!isPrepared()) {
        prepareForSolvingImpl();
        prepared_ = true;
    }
    for (auto latch : seqCircuit_->getLatches()) {
        SeqNet initValue = seqCircuit_->getLatchInit(latch);
        if (initValue == SeqNet()) {
            throw exception::IntrepidException(
                        "Uninitialized latch " + seqNetStore_->toString(latch),
                        __FILE__, __LINE__);
        }
    }
}

template<class SeqNet, class ComNet>
bool
Engine<SeqNet, ComNet>::
isPrepared() const {
    return prepared_;
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
addTarget(SeqNet t) {
    if (isPrepared()) {
        throw exception::IntrepidException(
                    "Cannot add target after engine is prepared",
                    __FILE__, __LINE__);
    }
    targets_.push_back(t);
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
addWatchedNet(SeqNet t) {
    watchedNets_.push_back(t);
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
removeLastReachedTargets() {
    for (SeqNet t : lastReachedTargets_) {
        removeTarget(t);
    }
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
removeTarget(SeqNet t) {
    unsigned rd, wr;
    for (rd = 0, wr = 0; rd < targets_.size(); rd++) {
        if (targets_[rd] == t) {
            // Do nothing, this value will be overridden
        } else {
            targets_[wr] = targets_[rd];
            wr++;
        }
    }
    while (targets_.size() > wr) {
        targets_.pop_back();
    }
    afterRemoveTarget();
}

template<class SeqNet, class ComNet>
size_t
Engine<SeqNet, ComNet>::
getNumberOfRemainingTargets() const {
    return targets_.size();
}

template<class SeqNet, class ComNet>
const std::vector<SeqNet>&
Engine<SeqNet, ComNet>::
getLastReachedTargets() const {
    return lastReachedTargets_;
}

template<class SeqNet, class ComNet>
const Trace<SeqNet, ComNet>&
Engine<SeqNet, ComNet>::
getTraceForTarget(SeqNet t) const {
    auto it = targetToTrace_.find(t);
    if (it == targetToTrace_.end()) {
        throw exception::IntrepidException(
                    "Could not find counterexample for given target",
                    __FILE__, __LINE__);
    }
    return *(it->second);
}

template<class SeqNet, class ComNet>
std::shared_ptr<Trace<SeqNet, ComNet>>
Engine<SeqNet, ComNet>::
getTracePtrForTarget(SeqNet t) const {
    auto it = targetToTrace_.find(t);
    if (it == targetToTrace_.end()) {
        throw exception::IntrepidException(
                    "Could not find counterexample for given target",
                    __FILE__, __LINE__);
    }
    return it->second;
}

template<class SeqNet, class ComNet>
const circuit::SeqCircuit<SeqNet>&
Engine<SeqNet, ComNet>::
getCircuit() const {
    return seqCircuit_;
}

template<class SeqNet, class ComNet>
void
Engine<SeqNet, ComNet>::
setVerbosity(unsigned v) {
    verbosity_ = v;
}

}  // namespace engine

#endif  // SRC_ENGINE_ENGINE_CPP_
