/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_BMC_CPP_
#define SRC_ENGINE_BMC_CPP_

#include "src/circuit/Unroller.h"
#include "src/net/NetStore.h"
#include "src/net/NetTypeInfo.h"
#include "src/engine/Bmc.h"
#include "src/engine/Simulator.h"

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

namespace engine {

template<class SeqNet, class ComNet>
Bmc<SeqNet, ComNet>::
Bmc(factory::Factory<SeqNet, ComNet>* factory,
    net::NetStore<SeqNet>* seqNetStore,
    circuit::SeqCircuit<SeqNet>* seqCircuit)
    : Engine<SeqNet, ComNet>(factory, seqNetStore, seqCircuit),
      unroller_(nullptr),
      comNetStore_(nullptr),
      comCircuit_(nullptr),
      currentDepth_(0),
      solverInitialized_(false),
      optimize_(false),
      induction_(false),
      allowTargetsOnlyAtLastDepth_(true),
      attackPathAxioms_(false)
{
    comNetStore_ = factory_->buildComNetStore();
    comCircuit_ = factory_->buildComCircuit("");
    unroller_ = std::unique_ptr<circuit::Unroller<SeqNet, ComNet>>(
                    new circuit::Unroller<SeqNet, ComNet>(*seqNetStore_,
                                                          *seqCircuit_,
                                                          comNetStore_.get(),
                                                          comCircuit_.get()));
    comSolver_ = factory_->buildComSolver(optimize_);
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
prepareForSolvingImpl() {
    unroller_->setAdditionalNetsToWatch(targets_);
    unroller_->setAdditionalNetsToWatch(depthToAssumption_);
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
setUseAttackPathAxioms(SeqNet source, SeqNet target) {
    source_ = source;
    target_ = target;
    attackPathAxioms_ = true;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
initializeSolver() {
    assert(!solverInitialized_);

    unroller_->unroll(currentDepth_,
                      induction_
                      ? circuit::UnrollerOptions::Initialize::DoNotInitialize
                      : circuit::UnrollerOptions::Initialize::InlineInitialStates);

    for (SeqNet a : seqCircuit_->getAssumptions()) {
        for (unsigned d = 0; d <= currentDepth_; d++) {
            ComNet ca = unroller_->getNetAtTime(a, d);
            comSolver_->addAssumption(ca);
        }
    }

    // depth 0 can be ignored as initial states are good enough
    for (unsigned d = 1; d < depthToAssumption_.size(); ++d) {
        ComNet ca = unroller_->getNetAtTime(depthToAssumption_[d], d);
        comSolver_->addAssumption(ca);
    }

    if (allowTargetsOnlyAtLastDepth_) {
        allowTargetsOnlyAtLastDepth();
    }

    if (attackPathAxioms_ && currentDepth_ >= 2) {
        /*
        For currentDepth_ = 4
        t@4 and s@4 are the last ones and should be ignored

        Axioms to add
        (t@0 = s@1 \/ t@0 = s@2 \/ t@0 = s@3) /\
                     (t@1 = s@2 \/ t@1 = s@3) /\
                                  (t@2 = s@3)
        */
        assert(currentDepth_ >= 1);
        for (int tDepth = 0; tDepth < static_cast<int>(currentDepth_) - 1; tDepth++) {
            auto tAtDepth = unroller_->getNetAtTime(target_, tDepth);
            auto clause = comNetStore_->mkFalse();
            for (int sDepth = tDepth + 1; sDepth < static_cast<int>(currentDepth_); sDepth ++) {
                auto sAtDepth = unroller_->getNetAtTime(source_, sDepth);
                auto eq = comNetStore_->mkEq(tAtDepth, sAtDepth);
                clause = comNetStore_->mkOr(clause, eq);
            }
            assert(clause != comNetStore_->mkFalse());
            comSolver_->addAssumption(clause);
        }
    }

    for (SeqNet t : targets_) {
        ComNet ct = unroller_->getNetAtTime(t, currentDepth_);
        comSolver_->addTarget(ct);
    }

    solverInitialized_ = true;
}

template<class SeqNet, class ComNet>
solver::SolverResult
Bmc<SeqNet, ComNet>::
checkInductive() {
    assert(induction_);
    assert(currentDepth_ > 0);
    constexpr auto MAX_ATTEMPTS = 100;
    auto attempts = 0;
    while (++attempts < MAX_ATTEMPTS) {
        auto result = comSolver_->solveAnyTargets();
        if (result == solver::SolverResult::Unsatisfiable) {
            return result;
        }
        assert(result == solver::SolverResult::Satisfiable);
        unsigned depth1, depth2;
        auto found = findEqualStates(&depth1, &depth2);
        if (!found) {
            return result;
        }
        blockEqualStates(depth1, depth2);
    }
    return solver::SolverResult::Satisfiable;
}

template<class SeqNet, class ComNet>
bool
Bmc<SeqNet, ComNet>::
findEqualStates(unsigned* depth1, unsigned* depth2) {
    std::unordered_map<std::string, unsigned> signatures;
    for (unsigned depth = 0; depth <= currentDepth_; depth++) {
        auto signature = computeStateSignature(depth);
        auto [it, inserted] = signatures.insert({signature, depth});
        if (!inserted) {
            *depth1 = it->second;
            *depth2 = depth;
            return true;
        }
    }
    return false;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
blockEqualStates(const unsigned depth1, const unsigned depth2) {
    auto atLeastOneDiff = comNetStore_->mkFalse();
    for (SeqNet latch : seqCircuit_->getLatches()) {
        if (!unroller_->hasNetAtTime(latch, depth1)) {
            continue;
        }
        assert(unroller_->hasNetAtTime(latch, depth2));
        auto comLatch1 = unroller_->getNetAtTime(latch, depth1);
        auto comLatch2 = unroller_->getNetAtTime(latch, depth2);
        ComNet diff;
        auto type = comNetStore_->getNetTypeInfo(comLatch1).getNetType();
        if (type == net::NetType::Boolean) {
            diff = comNetStore_->mkXor(comLatch1, comLatch2);
        } else {
            diff = comNetStore_->mkEq(comLatch1, comLatch2);
            diff = comNetStore_->mkNot(diff);
        }
        atLeastOneDiff = comNetStore_->mkOr(atLeastOneDiff, diff);
    }
    comSolver_->addAssumption(atLeastOneDiff);
}

template<class SeqNet, class ComNet>
std::string
Bmc<SeqNet, ComNet>::
computeStateSignature(const unsigned depth) {
    assert(depth <= currentDepth_);
    std::string signature;
    for (SeqNet latch : seqCircuit_->getLatches()) {
        if (unroller_->hasNetAtTime(latch, depth)) {
            ComNet comLatch = unroller_->getNetAtTime(latch, depth);
            ComNet value = comSolver_->evaluate(comLatch);
            signature += comNetStore_->toString(value);
        }
    }
    return signature;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
addInitialStates() {
    assert(induction_);
    comSolver_->pushBacktrackPoint();
    // Add initial states, only if they were not inlined
    for (auto latch : seqCircuit_->getLatches()) {
        SeqNet init = seqCircuit_->getLatchInit(latch);
        if (init != SeqNet() && unroller_->hasNetAtTime(latch, 0)) {
            ComNet l = unroller_->getNetAtTime(latch, 0);
            ComNet i = unroller_->getComNumber(init);
            auto eq = comNetStore_->mkEq(l, i);
            comSolver_->addAssumption(eq);
        }
    }
}

template<class SeqNet, class ComNet>
EngineResult
Bmc<SeqNet, ComNet>::
findFirstReachableTargetImpl() {
    if (verbosity_ > 0) {
        std::cout << "Depth: " << currentDepth_ << std::endl;
    }

    lastReachedTargets_.clear();

    const bool firstRound = !solverInitialized_;
    if (firstRound) {
        // One can call BMC many times, only the
        // first one we should initialize the solver
        initializeSolver();
    }

    // BMC step
    if (induction_) {
        addInitialStates();
    }

    if (verbosity_ > 0) {
        std::cout << "Bmc check" << std::endl;
    }

    auto solverResult = comSolver_->solveAnyTargets();

    switch (solverResult) {
    case solver::SolverResult::Satisfiable:
        break;
    case solver::SolverResult::Unsatisfiable:
    {
        // One may call BMC many times, only the first
        // time it makes sense to check for induction
        if (induction_ && currentDepth_ > 0 && firstRound) {
            // Remove initial states
            comSolver_->popBacktrackPoint();
            // Induction step
            if (verbosity_ > 0) {
                std::cout << "Induction check" << std::endl;
            }
            auto result = checkInductive();
            if (result == solver::SolverResult::Unsatisfiable) {
                return EngineResult::Unreachable;
            }
        }
        return EngineResult::Unknown;
    }
    default:
        assert(false);
        throw exception::IntrepidException("Unexpected solver result",
                                           __FILE__, __LINE__);
    }
    assert(solverResult == solver::SolverResult::Satisfiable);

    auto cex = computeTrace();

    ComNet lastSatTarget = comSolver_->getLastSatTarget();
#ifndef NDEBUG
    bool found = false;
#endif
    for (SeqNet t : targets_) {
        ComNet ct = unroller_->getNetAtTime(t, currentDepth_);
        if (ct == lastSatTarget) {
            lastReachedTargets_.push_back(t);
            targetToTrace_[t] = cex;
#ifndef NDEBUG
            found = true;
#endif
        }
    }
    assert(found);

    if (!verifyWithSimulation(*cex)) {
        assert(false);
        throw exception::IntrepidException("Unexpected solver result",
                                           __FILE__, __LINE__);
    }

    blockCex(*cex);

    extendTrace(cex.get());

    return EngineResult::Reachable;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
blockCex(const Trace<SeqNet, ComNet>& cex) {
    ComNet block = comNetStore_->mkTrue();
    for (auto& [seqNet, valueAtDepth] : cex) {
        unsigned depth = 0;
        for (auto& value : valueAtDepth) {
            if (!comNetStore_->isConcrete(value)) {
                continue;
            }
            auto type = comNetStore_->getNetTypeInfo(value);
            ComNet ct = unroller_->getNetAtTime(seqNet, depth++);
            ComNet conj;
            if (type.getNetType() == net::NetType::Boolean) {
                conj = comNetStore_->mkIff(ct, value);
            } else {
                conj = comNetStore_->mkEq(ct, value);
            }
            block = comNetStore_->mkAnd(block, conj);
        }
    }
    block = comNetStore_->mkNot(block);
    comSolver_->addAssumption(block);
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
allowTargetsOnlyAtLastDepth() {
    for (unsigned depth = 0; depth < currentDepth_; depth++) {
        for (auto target : targets_) {
            auto targetAtDepth = unroller_->getNetAtTime(target, depth);
            auto block = comNetStore_->mkNot(targetAtDepth);
            comSolver_->addAssumption(block);
        }
    }
}

template<class SeqNet, class ComNet>
bool
Bmc<SeqNet, ComNet>::
verifyWithSimulation(const Trace<SeqNet, ComNet>& trace) const {
    Simulator<SeqNet, ComNet> simulator(*seqNetStore_,
                                        *seqCircuit_,
                                        comNetStore_.get());
    for (SeqNet t : lastReachedTargets_) {
        simulator.addTarget(t);
    }

    simulator.simulate(trace, currentDepth_);

    if (simulator.getLastReachedTargets().size() !=
            lastReachedTargets_.size()) {
        return false;
    }

    return true;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
extendTrace(Trace<SeqNet, ComNet>* cex) const {
    assert(cex != nullptr);

    Simulator<SeqNet, ComNet> simulator(*seqNetStore_,
                                        *seqCircuit_,
                                        comNetStore_.get());
    for (SeqNet t : watchedNets_) {
        simulator.addWatchedNet(t);
    }

    simulator.extendTrace(cex, currentDepth_);
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
setCurrentDepth(const unsigned currentDepth) {
    currentDepth_ = currentDepth;
    comSolver_ = factory_->buildComSolver(optimize_);
    solverInitialized_ = false;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
setInduction(const bool induction) {
    induction_ = induction;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
setOptimize(const bool optimize) {
    optimize_ = optimize;
    comSolver_ = factory_->buildComSolver(optimize_);
}

template<class SeqNet, class ComNet>
std::shared_ptr<Trace<SeqNet, ComNet>>
Bmc<SeqNet, ComNet>::
computeTrace() const {
    auto cex = std::make_shared<Trace<SeqNet, ComNet>>();

    for (unsigned depth = 0; depth <= currentDepth_; depth++) {
        for (SeqNet input : seqCircuit_->getInputs()) {
            ComNet comInput = unroller_->getNetAtTime(input, depth);
            ComNet value = comSolver_->evaluate(comInput);
            cex->setData(input, depth, value);
        }
        if (depth == 0) {
            for (SeqNet latch : seqCircuit_->getLatches()) {
                if (seqCircuit_->getLatchInit(latch) == SeqNet()) {
                    ComNet comLatch = unroller_->getNetAtTime(latch, 0);
                    ComNet value = comSolver_->evaluate(comLatch);
                    cex->setData(latch, 0, value);
                }
            }
        }
    }

    return cex;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
afterRemoveTarget() {
    solverInitialized_ = false;
    comSolver_ = factory_->buildComSolver(optimize_);
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
setAllowTargetsOnlyAtLastDepth(const bool value) {
    allowTargetsOnlyAtLastDepth_ = value;
}

template<class SeqNet, class ComNet>
void
Bmc<SeqNet, ComNet>::
addAssumptionAtDepth(SeqNet net, unsigned depth) {
    depthToAssumption_.resize(depth + 1);
    depthToAssumption_[depth] = net;
    depthToAssumption_[0] = seqNetStore_->mkTrue();
}


}  // namespace engine

#endif  // SRC_ENGINE_BMC_CPP_
