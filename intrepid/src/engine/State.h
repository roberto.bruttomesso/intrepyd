/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_STATE_H_
#define SRC_ENGINE_STATE_H_

#include "src/solver/Solver.h"

namespace engine {

template<class SeqNet>
class State final {
 public:
    /**
     * A state that directly originates from a target, so it has no parent
     */
    static State* buildTargetState(const SeqNet cube, const SeqNet target) {
        return new State<SeqNet>(nextId_++, cube, nullptr, target, 0);
    }

    /**
     * A state that has a parent, so it is not directly originating from a target
     */
    static State* buildIntermediateState(const SeqNet cube, State* parent) {
        assert(parent != nullptr);
        return new State<SeqNet>(nextId_++, cube, parent, SeqNet(), parent->getDepth() + 1);
    }

    SeqNet getCube() const {
        return cube_;
    }

    State* getParent() const {
        return parent_;
    }

    SeqNet getTarget() const {
        return target_;
    }

    unsigned getId() const {
        return id_;
    }

    unsigned getDepth() const {
        return depth_;
    }

 private:
    State(const unsigned id,
          const SeqNet cube,
          State* parent,
          const SeqNet target,
          const unsigned depth)
        : id_(id),
          cube_(cube),
          parent_(parent),
          target_(target),
          depth_(depth)
    {
        assert(parent == nullptr || target == SeqNet());
        assert(parent != nullptr || target != SeqNet());
    }

    static unsigned nextId_;

    const unsigned id_;
    const SeqNet cube_;
    State* parent_;
    const SeqNet target_;
    const unsigned depth_;
};

}  // namespace engine

#include "src/engine/State.cpp"

#endif  // SRC_ENGINE_STATE_H_
