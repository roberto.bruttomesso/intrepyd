/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <vector>

#include "src/engine/Z3QuantifierEliminator.h"
#include "src/net/Z3NetUtils.h"

namespace engine {

Z3QuantifierEliminator::
Z3QuantifierEliminator(Z3_context context)
    : context_(context) 
{}

net::Z3SeqNet
Z3QuantifierEliminator::
eliminateQuantifiers(net::Z3SeqNet predicate) const {
    Z3_tactic qe = Z3_mk_tactic(context_, "qe");
    Z3_tactic_inc_ref(context_, qe);
    Z3_goal elim = Z3_mk_goal(context_, false, false, false);
    Z3_goal_inc_ref(context_, elim);

    const int varSize = static_cast<int>(existentialNets_.size());
    std::vector<Z3_app> vars(varSize);
    unsigned i = 0;
    for (auto net : existentialNets_) {
        vars[i++] = Z3_to_app(context_, net.getZ3Ast());
    }
        
    Z3_ast existsFormula =
        Z3_mk_exists_const(context_, 0, varSize, vars.data(), 0, 0, predicate.getZ3Ast());

    Z3_goal_assert(context_, elim, existsFormula);
    Z3_apply_result result = Z3_tactic_apply(context_, qe, elim);

#ifndef NDEBUG
    const unsigned nSubGoals =
#endif
    Z3_apply_result_get_num_subgoals(context_, result);
    assert(nSubGoals == 1);
    Z3_goal resGoal = Z3_apply_result_get_subgoal(context_, result, 0);

    Z3_ast resFormula = Z3_mk_true(context_);
    for (unsigned i = 0, size = Z3_goal_size(context_, resGoal); i < size; i++) {
        Z3_ast conj = Z3_goal_formula(context_, resGoal, i);
        Z3_ast args[] = { resFormula, conj };
        resFormula = Z3_mk_and(context_, 2, args);
    }

    Z3_goal_dec_ref(context_, elim);
    Z3_tactic_dec_ref(context_, qe);

    return net::Z3NetUtils::mkNetHelper<net::Z3SeqNet>(context_, resFormula);
}

}  // namespace engine
