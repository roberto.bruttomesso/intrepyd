/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#ifndef SRC_UNSAT_CORE_EXTRACTOR_H_
#define SRC_UNSAT_CORE_EXTRACTOR_H_

#include <vector>
#include <iostream>

#include "src/net/NetStore.h"

namespace engine {

template<class Net>
class UnsatCoreExtractor {
 public:
     virtual ~UnsatCoreExtractor() {}

     void pushBacktrackPoint() {
         backtrackPoints_.push_back(disjuncts_.size());
     }

     void popBacktrackPoint() {
         assert(!backtrackPoints_.empty());
         disjuncts_.resize(backtrackPoints_.back());
         backtrackPoints_.pop_back();
     }

     void initialize(net::NetStore<Net>* netStore) {
         netStore_ = netStore;
     }

     void addDisjunct(Net n) {
         assert(!netStore_->isTrue(n));
         disjuncts_.push_back(n);
     }

     Net extractCore(Net cube) {
         assert(!disjuncts_.empty());
         Net formula = netStore_->mkFalse();
         for (auto net : disjuncts_) {
             formula = netStore_->mkOr(formula, net);
         }
         return extractCoreImpl(formula, cube);
     }

 private:
     virtual Net extractCoreImpl(Net formula, Net cube) = 0;

 protected:
     void createMarkers(const size_t size) {
         while (size > markers_.size()) {
             const size_t currSize = markers_.size();
             markers_.push_back(
                 netStore_->mkVariable(
                     "__mark_" + std::to_string(currSize),
                     net::NetTypeInfo::mkBooleanType()));
             markid2index_[markers_.back()] = currSize;
         }
     }

     std::vector<Net>
     retrieveConjuncts(Net seqNet) {
         std::vector<Net> conjuncts;
         retrieveConjuncts(seqNet, &conjuncts);
         return conjuncts;
     }

     void retrieveConjuncts(Net net, 
                            std::vector<Net>* conjuncts) {
         if (netStore_->isAnd(net)) {
             for (unsigned i = 0, size = netStore_->getNofChildren(net);
                 i < size;
                 i++) {
                 retrieveConjuncts(netStore_->getChild(net, i),
                                   conjuncts);
             }
         }
         else {
             conjuncts->push_back(net);
         }
     }

     net::NetStore<Net>* netStore_;
     std::vector<Net> markers_;
     std::vector<Net> disjuncts_;
     std::unordered_map<unsigned, size_t> markid2index_;

 private:
     std::vector<size_t> backtrackPoints_;
};

}  // namespace engine

#endif  // SRC_UNSAT_CORE_EXTRACTOR_H_
