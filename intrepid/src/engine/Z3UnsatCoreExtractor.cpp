/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#ifndef SRC_ENGINE_Z3_UNSAT_CORE_EXTRACTOR_CPP_
#define SRC_ENGINE_Z3_UNSAT_CORE_EXTRACTOR_CPP_

#include <unordered_map>
#include <vector>

#include "src/net/NetStoreUtils.h"
#include "src/net/Z3NetUtils.h"
#include "src/engine/Z3UnsatCoreExtractor.h"

namespace engine {

template<class Net>
Net
Z3UnsatCoreExtractor<Net>::extractCoreImpl(Net formula, Net cube) {
    assert(netStore_ != nullptr);
    std::vector<Net> conjuncts = retrieveConjuncts(cube);
    assert(!conjuncts.empty());
    if (conjuncts.size() == 1) {
        return cube;
    }
    createMarkers(conjuncts.size());

    Z3_solver_push(context_, solver_);
    Z3_solver_assert(context_, solver_, formula.getZ3Ast());
    std::vector<Z3_ast> assumptions;
    for (size_t i = 0, size = conjuncts.size(); i < size; i++) {
        Z3_ast mark = markers_.at(i).getZ3Ast();
        Z3_ast disj[] = { conjuncts.at(i).getZ3Ast(), Z3_mk_not(context_, mark) };
        Z3_ast clause = Z3_mk_or(context_, 2, disj);
        Z3_solver_assert(context_, solver_, clause);
        assumptions.push_back(mark);
    }
#ifndef NDEBUG
    Z3_lbool result =
#endif
        Z3_solver_check_assumptions(context_,
                                    solver_,
                                    static_cast<unsigned>(assumptions.size()),
                                    assumptions.data());
    std::vector<Z3_ast> redCube;
    //if (result == Z3_L_TRUE) {
    //    auto model = Z3_solver_get_model(context_, solver_);
    //    Z3_model_inc_ref(context_, model);
    //    for (auto t : conjuncts) {
    //        Z3_ast result = nullptr;
    //        Z3_ast ast = t.getZ3Ast();
    //        Z3_model_eval(context_, model, ast, Z3_L_FALSE, &result);
    //        std::cout << Z3_ast_to_string(context_, ast)
    //                  << " -> "
    //                  << Z3_ast_to_string(context_, result)
    //                  << std::endl;
    //    }
    //    Z3_model_dec_ref(context_, model);
    //}
    assert(result == Z3_L_FALSE);
    Z3_ast_vector uc = Z3_solver_get_unsat_core(context_, solver_);
    for (unsigned i = 0; i < Z3_ast_vector_size(context_, uc); i++) {
        Z3_ast ast = Z3_ast_vector_get(context_, uc, i);
        const unsigned id = Z3_get_ast_id(context_, ast);
        redCube.push_back(conjuncts.at(markid2index_.at(id)).getZ3Ast());
    }
    Z3_solver_pop(context_, solver_, 1);
    Z3_ast redCubeAst = Z3_mk_and(context_,
                                  static_cast<unsigned>(redCube.size()),
                                  redCube.data());
    return net::Z3NetUtils::mkNetHelper<Net>(context_, redCubeAst);
}

}  // namespace engine

#endif  // SRC_ENGINE_Z3_UNSAT_CORE_EXTRACTOR_CPP_

