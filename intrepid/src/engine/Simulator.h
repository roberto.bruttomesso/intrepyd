/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_SIMULATOR_H_
#define SRC_ENGINE_SIMULATOR_H_

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <memory>

#include "src/factory/Factory.h"
#include "src/circuit/SeqCircuit.h"
#include "src/engine/Trace.h"

namespace engine {

template<class SeqNet, class ComNet>
class Simulator final {
 public:
    Simulator(const net::NetStore<SeqNet> &seqNetStore,
              const circuit::SeqCircuit<SeqNet>& seqCircuit,
              net::NetStore<ComNet>* comNetStore);

    void setOverrideMode(bool value);
    void simulate(const Trace<SeqNet, ComNet>& cex,
                  const unsigned targetDepth);
    void extendTrace(Trace<SeqNet, ComNet>* cex,
                     const unsigned maxDepth);
    void addTarget(SeqNet t);
    void addWatchedNet(SeqNet t);
    const std::vector<SeqNet>& getLastReachedTargets() const;
    ComNet getTargetValue(SeqNet target) const;

 protected:
    std::unordered_map<SeqNet, ComNet>
         simulate(const Trace<SeqNet, ComNet>& cex,
                  const unsigned depth,
                  const std::unordered_map<SeqNet, ComNet>& prevLatchValues);

    ComNet computeValue(const Trace<SeqNet, ComNet>& cex,
                        const unsigned depth,
                        const std::unordered_map<SeqNet, ComNet>& pLatchValues,
                        const SeqNet net);

    ComNet computeValueFromOne(const Trace<SeqNet, ComNet>& cex,
                               const unsigned depth,
                               const std::unordered_map<SeqNet, ComNet>& p,
                               const SeqNet net);

    ComNet computeValueFromTwo(const Trace<SeqNet, ComNet>& cex,
                               const unsigned depth,
                               const std::unordered_map<SeqNet, ComNet>& p,
                               const SeqNet net);

    ComNet computeValueFromThree(const Trace<SeqNet, ComNet>& cex,
                                 const unsigned depth,
                                 const std::unordered_map<SeqNet, ComNet>& p,
                                 const SeqNet net);

    ComNet computeValueFromN(const Trace<SeqNet, ComNet>& cex,
                             const unsigned depth,
                             const std::unordered_map<SeqNet, ComNet>& p,
                             const SeqNet net);

    const net::NetStore<SeqNet>& seqNetStore_;
    const circuit::SeqCircuit<SeqNet>& seqCircuit_;
    net::NetStore<ComNet>* comNetStore_;
    std::vector<SeqNet> lastReachedTargets_;
    std::vector<SeqNet> targets_;
    std::unordered_set<SeqNet> watchedNets_;
    std::unordered_map<SeqNet, ComNet> target2value_;
    std::vector<std::unique_ptr<Trace<SeqNet, ComNet>>> generatedCexes_;
    bool overrideMode_;
};

}  // namespace engine

#include "src/engine/Simulator.cpp"

#endif  // SRC_ENGINE_SIMULATOR_H_
