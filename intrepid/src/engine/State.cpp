/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#ifndef SRC_ENGINE_STATE_CPP_
#define SRC_ENGINE_STATE_CPP_

#include "src/engine/State.h"

namespace engine {

template<class SeqNet>
unsigned State<SeqNet>::nextId_ = 1;

}  // namespace engine

#endif  // SRC_ENGINE_STATE_CPP_
