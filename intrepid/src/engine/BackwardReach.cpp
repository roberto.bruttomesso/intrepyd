/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_BACKWARDREACH_CPP_
#define SRC_ENGINE_BACKWARDREACH_CPP_

#include <vector>
#include <cassert>

#include "nlohmann/json.hpp"

#include "src/net/NetType.h"
#include "src/engine/State.h"
#include "src/engine/BackwardReach.h"
#include "src/engine/Bmc.h"
#include "src/factory/Factory.h"
#include "src/net/NetUtils.h"
#include "src/net/NetStoreUtils.h"
#include "src/engine/Trace.h"

namespace engine {

template<class SeqNet, class ComNet>
BackwardReach<SeqNet, ComNet>::
BackwardReach(factory::Factory<SeqNet, ComNet>* factory,
              net::NetStore<SeqNet>* seqNetStore,
              circuit::SeqCircuit<SeqNet>* seqCircuit)
    : Engine<SeqNet, ComNet>(factory, seqNetStore, seqCircuit),
      blockedStates_(nullptr),
      initStates_(nullptr),
      preimageHelper_(nullptr)
{}

template<class SeqNet, class ComNet>
BackwardReach<SeqNet, ComNet>::
~BackwardReach() {
    for (auto state : stateSpace_) {
        delete state;
    }

    // nlohmann::json stats;
    // stats["depth_reached"] = statistics_.depthReached;
    // stats["enumerations"] = statistics_.enumerations;
    // stats["generalize"]["calls"] = statistics_.genCalls;

    // std::cout << stats.dump() << std::endl;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
prepareForSolvingImpl() {
    blockedStates_ = factory_->buildSeqSolver();
    initStates_ = factory_->buildSeqSolver();
    preimageHelper_ = factory_->buildSubstituteHelper();

    // Add initial states and set next mapping
    for (auto latch : seqCircuit_->getLatches()) {
        SeqNet init = seqCircuit_->getLatchInit(latch);
        SeqNet next = seqCircuit_->getLatchNext(latch);
        if (init != SeqNet()) {
            SeqNet eq = seqNetStore_->mkEq(latch, init);
            initStates_->addAssumption(eq);
        }
        preimageHelper_->setMapping(latch, next);
    }

    // Add assumptions to initial state solver and blocked states
    for (auto assumption : seqCircuit_->getAssumptions()) {
        initStates_->addAssumption(assumption);
        blockedStates_->addAssumption(assumption);
    }
}

template<class SeqNet, class ComNet>
EngineResult
BackwardReach<SeqNet, ComNet>::
findFirstReachableTargetImpl() {
    // Add all states from targets
    for (auto target : targets_) {
        auto result = enumerateAndAddStatesToFrontier(target, nullptr);
        if (result == EngineResult::Reachable) {
            return EngineResult::Reachable;
        }
    }

    // Add all states from preimage computation
    while (!isFrontierEmpty()) {
        State<SeqNet>* state = popFromFrontier();
        assert(state != nullptr);
        if (verbosity_ > 0) {
            std::cout << "[ ID=" << state->getId()
                      << ", DE=" << state->getDepth()
                      << ", FR=" << frontier_.size() + 1
                      << " ]" << std::endl;
        }

        const auto currentDepth = state->getDepth() + 1;
        if (currentDepth > statistics_.depthReached) {
            statistics_.depthReached = currentDepth;
        }

        SeqNet preimage = preimageHelper_->substitute(state->getCube());

        if (seqNetStore_->isFalse(preimage)) {
            continue;
        }
        auto result = enumerateAndAddStatesToFrontier(preimage, state);
        if (result == EngineResult::Reachable) {
            return EngineResult::Reachable;
        }
    }

    assert(isFrontierEmpty());

    // Frontier is empty, no state was found reachable,
    // and all added states were blocked, hence all targets
    // are unreachable

    //validateUnreachability();
    return EngineResult::Unreachable;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
addWatchedNets(solver::Solver<SeqNet>* solver, SeqNet t) {
    seqNetCache_.clear();
    addWatchedNetsRec(solver, t);
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
pushToFrontier(State<SeqNet>* state) {
    frontier_.push_back(state);
    statistics_.maxFrontier++;
}

template<class SeqNet, class ComNet>
State<SeqNet>*
BackwardReach<SeqNet, ComNet>::
popFromFrontier() {
    State<SeqNet>* result = frontier_.front();
    frontier_.pop_front();
    return result;
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
eliminateBooleanInputs(SeqNet cube) const {
    std::vector<SeqNet> theoryAtoms = collectTheoryAtoms(cube);
    SeqNet elimCube = seqNetStore_->mkTrue();
    for (auto t : theoryAtoms) {
        elimCube = seqNetStore_->mkAnd(elimCube, t);
    }
    return elimCube;
}

template<class SeqNet, class ComNet>
std::vector<SeqNet>
BackwardReach<SeqNet, ComNet>::
collectTheoryAtoms(SeqNet cube) const {
    std::vector<SeqNet> theoryAtoms;
    collectTheoryAtomsRec(cube, &theoryAtoms);
    return theoryAtoms;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
collectTheoryAtomsRec(SeqNet net, std::vector<SeqNet>* theoryAtoms) const {
    auto bt = net::NetTypeInfo::mkBooleanType();
    if (seqNetStore_->isAnd(net)) {
        for (unsigned i = 0; i < seqNetStore_->getNofChildren(net); i++) {
            collectTheoryAtomsRec(seqNetStore_->getChild(net, i), theoryAtoms);
        }
    } else {
        if (seqNetStore_->isNot(net)) {
            SeqNet child = seqNetStore_->getChild(net, 0);
            if (!seqCircuit_->isInput(child)) {
                // Negated theory atom
                theoryAtoms->push_back(net);
            }
        } else if (!seqCircuit_->isInput(net)) {
            // Positive theory atom
            theoryAtoms->push_back(net);
        }
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
addWatchedNetsRec(solver::Solver<SeqNet>* solver, SeqNet t) {
    if (!seqNetCache_.insert(t).second) {
        return;
    }
    net::NetTypeInfo ti = seqNetStore_->getNetTypeInfo(t);
    assert(ti == net::NetTypeInfo::mkBooleanType());
    if (seqNetStore_->isFalse(t)) {
        // Skip false
    } else if (seqCircuit_->isLatch(t)) {
        // Boolean latch
        solver->allSmtAddWatchedNet(t);
    } else if (seqCircuit_->isInput(t)) {
        // Watch boolean inputs
        solver->allSmtAddWatchedNet(t);
    } else if (isBooleanGate(t)) {
        // Boolean gate, recurse
        for (unsigned i = 0; i < seqNetStore_->getNofChildren(t); i++) {
            addWatchedNetsRec(solver, seqNetStore_->getChild(t, i));
        }
    } else if (seqNetStore_->isIte(t)) {
        // Boolean ite, recurse
        for (unsigned i = 0; i < seqNetStore_->getNofChildren(t); i++) {
            addWatchedNetsRec(solver, seqNetStore_->getChild(t, i));
        }
    } else if (containsLatches(t)) {
        // Predicate, which might contain ites
#ifndef NDEBUG
        auto kind = seqNetStore_->getNetKind(t);
        assert(net::NetUtils::isRelationalOperator(std::get<0>(kind)));
        assert(seqNetStore_->getNofChildren(t) == 2);
#endif
        solver->allSmtAddWatchedNet(t);
        addWatchedTheoryNetsRec(solver, seqNetStore_->getChild(t, 0));
        addWatchedTheoryNetsRec(solver, seqNetStore_->getChild(t, 1));
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
addWatchedTheoryNetsRec(solver::Solver<SeqNet>* solver, SeqNet t) {
    if (!seqNetCache_.insert(t).second) {
        return;
    }
    if (seqNetStore_->isFpaRounding(t)) {
        return;
    }
    net::NetTypeInfo ti = seqNetStore_->getNetTypeInfo(t);
    assert(ti != net::NetTypeInfo::mkBooleanType());
    if (seqNetStore_->isIte(t)) {
        // Term ite, (boolean) recurse in condition
        addWatchedNetsRec(solver, seqNetStore_->getChild(t, 0));
        // Term ite, (theory) recurse in then, else
        addWatchedTheoryNetsRec(solver, seqNetStore_->getChild(t, 1));
        addWatchedTheoryNetsRec(solver, seqNetStore_->getChild(t, 2));
    } else {
        // Theory term, recurse in children, if any
        for (unsigned i = 0; i < seqNetStore_->getNofChildren(t); i++) {
            addWatchedTheoryNetsRec(solver, seqNetStore_->getChild(t, i));
        }
    }
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
isBooleanGate(SeqNet n) const {
    if (seqNetStore_->getNofChildren(n) == 0) {
        return false;
    }
    SeqNet child0 = seqNetStore_->getChild(n, 0);
    net::NetTypeInfo child0type = seqNetStore_->getNetTypeInfo(child0);
    return seqNetStore_->isAnd(n) ||
           seqNetStore_->isOr(n)  ||
           seqNetStore_->isNot(n) ||
           seqNetStore_->isXor(n) ||
           seqNetStore_->isIff(n) ||
           (seqNetStore_->isEq(n) &&
            net::NetTypeInfo::mkBooleanType() ==
            child0type);
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
containsInputs(SeqNet net) const {
    if (seqCircuit_->isInput(net)) {
        return true;
    }
    const unsigned end = seqNetStore_->getNofChildren(net);
    for (unsigned i = 0; i < end; i++) {
        SeqNet child = seqNetStore_->getChild(net, i);
        if (containsInputs(child)) {
            return true;
        }
    }
    return false;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
retrieveInputs(SeqNet net,
               std::unordered_set<SeqNet>* inputs,
               std::unordered_set<SeqNet>* seen) const {
    if (!seen->insert(net).second) {
        return;
    }
    if (seqCircuit_->isInput(net)) {
        inputs->insert(net);
    }
    const unsigned end = seqNetStore_->getNofChildren(net);
    for (unsigned i = 0; i < end; i++) {
        SeqNet child = seqNetStore_->getChild(net, i);
        retrieveInputs(child, inputs, seen);
    }
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
containsLatches(SeqNet net) const {
    if (seqCircuit_->isLatch(net)) {
        return true;
    }
    const unsigned end = seqNetStore_->getNofChildren(net);
    for (unsigned i = 0; i < end; i++) {
        SeqNet child = seqNetStore_->getChild(net, i);
        if (containsLatches(child)) {
            return true;
        }
    }
    return false;
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
intersectsInitialStates(SeqNet net) {
    initStates_->pushBacktrackPoint();
    solver::SolverResult result = initStates_->solve(net);
    if (result == solver::SolverResult::Unknown) {
        assert(false);
        throw exception::IntrepidException("Unexpected solver result",
                                           __FILE__,
                                           __LINE__);
    }
    initStates_->popBacktrackPoint();
    return result == solver::SolverResult::Satisfiable;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
addStatesToBlocked(State<SeqNet>* state) {
    SeqNet cube = state->getCube();
    assert(!seqNetStore_->isTrue(cube));
    blockedStates_->blockCube(cube);
    pushToFrontier(state);
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
isFrontierEmpty() const {
    return frontier_.empty();
}

template<class SeqNet, class ComNet>
EngineResult
BackwardReach<SeqNet, ComNet>::
enumerateAndAddStatesToFrontier(SeqNet target,
                                State<SeqNet>* parent) {
    const auto buildState = [] (SeqNet cube, SeqNet target, State<SeqNet>* parent) {
        State<SeqNet>* state;
        if (parent == nullptr) {
            state = State<SeqNet>::buildTargetState(cube, target);
        } else {
            state = State<SeqNet>::buildIntermediateState(cube, parent);
        }
        return state;
    };

    if (intersectsInitialStates(target)) {
        auto state = buildState(target, target, parent);
        computeTrace(state);
        return EngineResult::Reachable;
    }

    // Does not seem a good idea, stalmark.lus takes forever
    // if (!containsInputs(target)) {
    //     auto state = buildState(target, target, parent);
    //     addStatesToBlocked(state);
    //     return EngineResult::Unknown;
    // }

    addWatchedNets(blockedStates_.get(), target);
    bool firstIteration = true;
    blockedStates_->pushBacktrackPoint();
    blockedStates_->allSmtSetTarget(target);

    std::vector<State<SeqNet>*> statesToBlock;
    for (;;) {
        statistics_.enumerations++;
        SeqNet cube = blockedStates_->allSmtSolve(seqNetStore_, false);
        auto result = blockedStates_->lastSolveResult();
        if (result == solver::SolverResult::Unsatisfiable) {
            break;
        } else if (result == solver::SolverResult::Unknown) {
            assert(false);
            throw exception::IntrepidException(
                        "Unexpected result from solver",
                        __FILE__, __LINE__);
        }
        assert(result == solver::SolverResult::Satisfiable);
        if (!seqNetStore_->isTrue(cube)) {
            cube = generalizeCube(cube, target);
        }
        cube = eliminateBooleanInputs(cube);
        cube = eliminateTheoryInputs(cube);
        assert(!containsInputs(cube));
        assert(!seqNetStore_->isFalse(cube));
        assert(!seqNetStore_->isTrue(cube));
        assert(!intersectsInitialStates(cube));
        auto state = buildState(cube, target, parent);
        blockedStates_->blockCube(cube); // will be undone by popBacktrackPoint!
        stateSpace_.push_back(state);
        statesToBlock.push_back(state);
        firstIteration = false;
    }
    blockedStates_->allSmtClearWatchedNets();
    blockedStates_->popBacktrackPoint();
    // Add popped states back again
    for (auto s : statesToBlock) {
        addStatesToBlocked(s);
    }
    return EngineResult::Unknown;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
computeTrace(State<SeqNet>* state) {
    State<SeqNet>* targetState = state;
    unsigned depth = 0;
    // std::vector<SeqNet> depthToState;
    while (targetState->getParent() != nullptr) {
        // depthToState.push_back(targetState->getCube());
        targetState = targetState->getParent();
        depth++;
    }
    assert(targetState->getDepth() == 0);
    assert(targetState->getParent() == nullptr);
    assert(targetState->getTarget() != SeqNet());
    assert(state->getDepth() == depth);
    SeqNet target = targetState->getTarget();
    Bmc<SeqNet, ComNet> bmc(factory_, seqNetStore_, seqCircuit_);
    // Seems slower :(
    // for (unsigned d = 1; d < depthToState.size(); ++d) {
    //     bmc.addAssumptionAtDepth(depthToState[d], d);
    // }
    bmc.addTarget(target);
    bmc.prepareForSolving();
    bmc.setCurrentDepth(depth);
    for (auto net : watchedNets_) {
        bmc.addWatchedNet(net);
    }
    EngineResult result = bmc.findFirstReachableTarget();
    if (result != EngineResult::Reachable) {
        assert(false);
        throw exception::IntrepidException(
            "Expected reachable result",
            __FILE__, __LINE__);
    }
    auto trace = bmc.getTracePtrForTarget(target);
    targetToTrace_[target] = trace;
    lastReachedTargets_.clear();
    for (auto t : bmc.getLastReachedTargets()) {
        lastReachedTargets_.push_back(t);
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
computeTrace(SeqNet target) {
    Bmc<SeqNet, ComNet> bmc(factory_, seqNetStore_, seqCircuit_);
    bmc.addTarget(target);
    bmc.prepareForSolving();
    bmc.setCurrentDepth(0);
    EngineResult result = bmc.findFirstReachableTarget();
    if (result != EngineResult::Reachable) {
        assert(false);
        throw exception::IntrepidException("Expected reachable result",
                                           __FILE__,
                                           __LINE__);
    }
    auto cex = bmc.getTracePtrForTarget(target);
    targetToTrace_[target] = cex;
    lastReachedTargets_.clear();
    for (auto t : bmc.getLastReachedTargets()) {
        lastReachedTargets_.push_back(t);
    }
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
eliminateTheoryInputs(SeqNet cube) const {
    statistics_.qeCalls++;
    if (seqNetStore_->isTrue(cube)) {
        return cube;
    }
    // Eliminate theory inputs
    auto theoryInputs = collectTheoryInputs(cube);
    if (theoryInputs.empty()) {
        // ... if any
        return cube;
    }
    auto quantifierEliminator = factory_->buildQuantifierEliminator();
    for (auto net : theoryInputs) {
        quantifierEliminator->setExistentialNet(net);
    }
    SeqNet result = quantifierEliminator->eliminateQuantifiers(cube);
    assert(!containsInputs(result));
    statistics_.qeTheoryInputs += theoryInputs.size();
    return result;
}

template<class SeqNet, class ComNet>
std::unordered_set<SeqNet>
BackwardReach<SeqNet, ComNet>::
collectTheoryInputs(SeqNet cube) const {
    std::unordered_set<SeqNet> inputs;
    std::unordered_set<SeqNet> cache;
    collectTheoryInputsRec(cube, &inputs, &cache);
    return inputs;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
collectTheoryInputsRec(SeqNet net,
                       std::unordered_set<SeqNet>* inputs,
                       std::unordered_set<SeqNet>* cache) const {
    if (!cache->insert(net).second) {
        return;
    }

    const unsigned end = seqNetStore_->getNofChildren(net);
    auto bt = net::NetTypeInfo::mkBooleanType();
    for (unsigned i = 0; i < end; i++) {
        SeqNet child = seqNetStore_->getChild(net, i);
        collectTheoryInputsRec(child, inputs, cache);
    }
    if (seqNetStore_->getNetTypeInfo(net) != bt &&
        seqCircuit_->isInput(net)) {
        inputs->insert(net);
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
validateUnreachability() {
    SeqNet candidateInvariant = buildCandidateInvariant();
    if (!checkContainsInitialStates(candidateInvariant)) {
        assert(false);
        throw exception::IntrepidException(
                    "Candidate invariant does not contain initial states",
                    __FILE__, __LINE__);
    }
    if (!checkNotIntersectingTargets(candidateInvariant)) {
        assert(false);
        throw exception::IntrepidException(
                    "Candidate invariant intersects some target",
                    __FILE__, __LINE__);
    }
    if (!checkIsInductive(candidateInvariant)) {
        assert(false);
        throw exception::IntrepidException(
                    "Candidate invariant is not inductive",
                    __FILE__, __LINE__);
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
retrieveBooleanInputs(const std::unordered_set<SeqNet>& atoms,
                      std::unordered_set<SeqNet>* inputs) const {
    for (auto atom : atoms) {
        if (seqNetStore_->getNofChildren(atom) == 0 &&
            seqCircuit_->isInput(atom)) {
            inputs->insert(atom);
        } else if (seqNetStore_->isNot(atom) &&
                   seqCircuit_->isInput(seqNetStore_->getChild(atom, 0))) {
            inputs->insert(atom);
        }
    }
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
simplifyTarget(SeqNet target,
               const std::unordered_set<SeqNet>& atoms,
               SeqNet except) const {
    auto substituteHelper = factory_->buildSubstituteHelper();
    for (auto atom: atoms) {
        if (atom == except) {
            continue;
        }
        if (seqNetStore_->isNot(atom)) {
            auto at = seqNetStore_->getChild(atom, 0);
            substituteHelper->setMapping(at, seqNetStore_->mkFalse());
        } else {
            substituteHelper->setMapping(atom, seqNetStore_->mkTrue());
        }
    }
    return substituteHelper->substitute(target);
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
retrieveAtoms(SeqNet net,
              std::unordered_set<SeqNet>* atoms) const {
    if (seqNetStore_->isAtom(net)) {
        atoms->insert(net);
    } else {
        const unsigned end = seqNetStore_->getNofChildren(net);
        auto bt = net::NetTypeInfo::mkBooleanType();
        for (unsigned i = 0; i < end; i++) {
            SeqNet child = seqNetStore_->getChild(net, i);
            retrieveAtoms(child, atoms);
        }
    }
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
retrieveImportantAtoms(SeqNet simplifiedTarget,
                       SeqNet target,
                       const std::unordered_set<SeqNet>& atoms,
                       std::unordered_set<SeqNet>* importantAtoms) const {
    // One thing to try could be: boolean atoms are not important
    std::unordered_set<SeqNet> unimportantAtoms = atoms;
    for (auto atom: atoms) {
        auto st = simplifyTarget(target, unimportantAtoms, atom);
        if (st != simplifiedTarget) {
            importantAtoms->insert(atom);
            unimportantAtoms.erase(atom);
        }
    }

    return simplifiedTarget;
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
generalizeCube(SeqNet cube, SeqNet target) {
    statistics_.genCalls++;

    if (!hasAtLeastTwoLatches(target)) {
        statistics_.genSkippedLessTwoLatches++;
        // Nothing to minimize
        return cube;
    }

    std::unordered_set<SeqNet> atoms;
    retrieveCubeConjuncts(cube, &atoms);
    auto simplifiedTarget = simplifyTarget(target, atoms, SeqNet());

    if (containsInputs(simplifiedTarget)) {
        statistics_.genSkippedInputs++;
        return cube;
    }

    std::unordered_set<SeqNet> importantAtoms;
    retrieveImportantAtoms(simplifiedTarget, target, atoms, &importantAtoms);

    SeqNet newCube = simplifiedTarget;
    for (auto atom : importantAtoms) {
        newCube = seqNetStore_->mkAnd(newCube, atom);
    }

    statistics_.genReducedAtoms += (atoms.size() - importantAtoms.size());

    return newCube;
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
hasAtLeastTwoLatches(SeqNet net) const {
    if (seqCircuit_->isLatch(net)) {
        return false;
    }
    int count = 0;
    for (unsigned i = 0, size = seqNetStore_->getNofChildren(net);
         i < size;
         i++) {
        count += countTwoLatchesRec(seqNetStore_->getChild(net, i));
        if (count >= 2) {
            return true;
        }
    }
    return false;
}

template<class SeqNet, class ComNet>
int
BackwardReach<SeqNet, ComNet>::
countTwoLatchesRec(SeqNet net) const {
    if (seqCircuit_->isLatch(net)) {
        return 1;
    } else {
        int count = 0;
        for (unsigned i = 0, size = seqNetStore_->getNofChildren(net);
             i < size;
             i++) {
            count += countTwoLatchesRec(seqNetStore_->getChild(net, i));
            if (count >= 2) {
                return count;
            }
        }
        return count;
    }
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
retrieveCubeConjuncts(SeqNet net,
                      std::unordered_set<SeqNet>* atoms) const {
    assert(atoms != nullptr);
    if (seqNetStore_->isAnd(net)) {
        for (unsigned i = 0, size = seqNetStore_->getNofChildren(net);
             i < size;
             i++) {
            retrieveCubeConjuncts(seqNetStore_->getChild(net, i),
                                  atoms);
        }
    } else {
        atoms->insert(net);
    }
}

template<class SeqNet, class ComNet>
SeqNet
BackwardReach<SeqNet, ComNet>::
buildCandidateInvariant() const {
    SeqNet candidateInvariant = seqNetStore_->mkTrue();
    for (auto state : stateSpace_) {
        SeqNet cube = state->getCube();
        SeqNet clause = seqNetStore_->mkNot(cube);
        candidateInvariant =
                seqNetStore_->mkAnd(candidateInvariant, clause);
    }
    return candidateInvariant;
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
checkContainsInitialStates(SeqNet candidateInvariant) const {
    initStates_->pushBacktrackPoint();
    SeqNet negatedCandidateInvariant = seqNetStore_->mkNot(candidateInvariant);
    solver::SolverResult result = initStates_->solve(negatedCandidateInvariant);
    initStates_->popBacktrackPoint();
    return result == solver::SolverResult::Unsatisfiable;
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
checkNotIntersectingTargets(SeqNet candidateInvariant) const {
    auto checker = factory_->buildSeqSolver();
    checker->addAssumption(candidateInvariant);
    for (auto assumption : seqCircuit_->getAssumptions()) {
        checker->addAssumption(assumption);
    }
    for (SeqNet target : targets_) {
        checker->pushBacktrackPoint();
        solver::SolverResult result = checker->solve(target);
        checker->popBacktrackPoint();
        if (result == solver::SolverResult::Satisfiable) {
            return false;
        }
    }
    return true;
}

template<class SeqNet, class ComNet>
bool
BackwardReach<SeqNet, ComNet>::
checkIsInductive(SeqNet candidateInvariant) {
    // TODO: rewrite into something that simply does
    // CAND_INV(X) /\ T(X, X', I) -> CAND_INV(X')

    assert(!seqNetStore_->isFalse(candidateInvariant));
    if (seqNetStore_->isTrue(candidateInvariant)) {
        return true;
    }
    SeqNet preimage = preimageHelper_->substitute(candidateInvariant);
    // preimage may contain inputs, we need to do an all-smt on the
    // important predicates (e.g., latches) in order to get rid of
    // the imputs
    auto checker = factory_->buildSeqSolver();
    auto enumerator = factory_->buildSeqSolver();
    for (auto assumption : seqCircuit_->getAssumptions()) {
        checker->addAssumption(assumption);
        enumerator->addAssumption(assumption);
    }
    addWatchedNets(enumerator.get(), preimage);
    enumerator->allSmtSetTarget(preimage);
    for (;;) {
        SeqNet cube = enumerator->allSmtSolve(seqNetStore_);
        cube = eliminateBooleanInputs(cube);
        cube = eliminateTheoryInputs(cube);
        assert(false); // allSmtSolve needs to be adjusted to not block cubes, because now they contain inputs too
        assert(!containsInputs(cube));
        assert(!seqNetStore_->isFalse(cube));
        auto result = enumerator->lastSolveResult();
        if (result == solver::SolverResult::Unsatisfiable) {
            // Finished enumerating and adding cubes
            break;
        } else if (result == solver::SolverResult::Unknown) {
            assert(false);
            throw exception::IntrepidException(
                        "Unexpected result from solver",
                        __FILE__, __LINE__);
        }
        assert(result == solver::SolverResult::Satisfiable);
        checker->addAssumption(cube);
    }

    SeqNet negatedCandidateInvariant = seqNetStore_->mkNot(candidateInvariant);
    solver::SolverResult result = checker->solve(negatedCandidateInvariant);
    return result == solver::SolverResult::Unsatisfiable;
}

template<class SeqNet, class ComNet>
void
BackwardReach<SeqNet, ComNet>::
afterRemoveTarget() {
}

}  // namespace engine

#endif  // SRC_ENGINE_BACKWARDREACH_CPP_
