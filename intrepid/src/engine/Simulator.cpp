/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_SIMULATOR_CPP_
#define SRC_ENGINE_SIMULATOR_CPP_

#include <vector>
#include <unordered_map>

#include "src/engine/Simulator.h"
#include "src/net/NetStoreUtils.h"

namespace engine {

template<class SeqNet, class ComNet>
Simulator<SeqNet, ComNet>::
Simulator(const net::NetStore<SeqNet> &seqNetStore,
          const circuit::SeqCircuit<SeqNet>& seqCircuit,
          net::NetStore<ComNet>* comNetStore)
    : seqNetStore_(seqNetStore),
      seqCircuit_(seqCircuit),
      comNetStore_(comNetStore),
      overrideMode_(false)
{}

template<class SeqNet, class ComNet>
void
Simulator<SeqNet, ComNet>::
setOverrideMode(bool value) {
    overrideMode_ = value;
}

template<class SeqNet, class ComNet>
void
Simulator<SeqNet, ComNet>::
addTarget(SeqNet t) {
    targets_.push_back(t);
}

template<class SeqNet, class ComNet>
void
Simulator<SeqNet, ComNet>::
addWatchedNet(SeqNet t) {
    watchedNets_.insert(t);
}

template<class SeqNet, class ComNet>
const std::vector<SeqNet>&
Simulator<SeqNet, ComNet>::
getLastReachedTargets() const {
    return lastReachedTargets_;
}

template<class SeqNet, class ComNet>
void
Simulator<SeqNet, ComNet>::
simulate(const Trace<SeqNet, ComNet>& trace,
         const unsigned targetDepth) {
    lastReachedTargets_.clear();
    target2value_.clear();

    std::unordered_map<SeqNet, ComNet> previousLatchValues;

    // Loads latches values at step 0. If an
    // initial value is undefined, it is retrieved
    // from the trace
    for (auto latch : seqCircuit_.getLatches()) {
        SeqNet initValue = seqCircuit_.getLatchInit(latch);
        assert(initValue != SeqNet());
        ComNet initValueCom;
        if (trace.hasData(latch, 0)) {
            // There is data in the trace, concrete or not
            // we don't care
            initValueCom = trace.getData(latch, 0);
        } else {
            initValueCom = net::NetStoreUtils<SeqNet, ComNet>::
                    translateConstantOrNumber(initValue,
                                              seqNetStore_,
                                              comNetStore_);
        }
        assert(initValueCom != ComNet());
        previousLatchValues.insert(std::make_pair(latch, initValueCom));
    }

    for (unsigned depth = 0; depth < targetDepth; depth++) {
        previousLatchValues = simulate(trace, depth, previousLatchValues);
    }

    // Compute values of targets
    for (auto target : targets_) {
        const ComNet value = computeValue(trace,
                                          targetDepth,
                                          previousLatchValues,
                                          target);
        if (comNetStore_->isTrue(value)) {
            lastReachedTargets_.push_back(target);
        }
        target2value_.insert(std::make_pair(target, value));
    }
}


template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
getTargetValue(SeqNet target) const {
    assert(target2value_.find(target) != target2value_.end());
    return target2value_.at(target);
}

template<class SeqNet, class ComNet>
void
Simulator<SeqNet, ComNet>::
extendTrace(Trace<SeqNet, ComNet>* trace, const unsigned maxDepth) {
    assert(trace != nullptr);

    std::unordered_map<SeqNet, ComNet> previousLatchValues;

    // Loads latches values at step 0
    for (auto latch : seqCircuit_.getLatches()) {
        SeqNet initValue = seqCircuit_.getLatchInit(latch);
        assert(initValue != SeqNet());
        ComNet initValueCom;
        if (trace->hasData(latch, 0)) {
            // There is data in the trace, concrete or not
            // we don't care
            initValueCom = trace->getData(latch, 0);
        } else {
            initValueCom = net::NetStoreUtils<SeqNet, ComNet>::
                    translateConstantOrNumber(initValue,
                                              seqNetStore_,
                                              comNetStore_);
        }
        assert(initValueCom != ComNet());
        previousLatchValues.insert(std::make_pair(latch, initValueCom));
    }

    for (unsigned depth = 0; depth <= maxDepth; depth++) {
        for (auto watch : watchedNets_) {
            // Extends trace here for latches
            if (seqCircuit_.isLatch(watch)) {
                auto it = previousLatchValues.find(watch);
                assert(it != previousLatchValues.end());
                trace->setData(watch, depth, it->second);
            }
        }

        for (auto watch : watchedNets_) {
            // Extends trace here for non-latches
            if (!seqCircuit_.isLatch(watch)) {
                const ComNet value = computeValue(*trace,
                                                  depth,
                                                  previousLatchValues,
                                                  watch);
                trace->setData(watch, depth, value);
            }
        }

        // Actual simulation
        previousLatchValues = simulate(*trace, depth, previousLatchValues);
    }
}

template<class SeqNet, class ComNet>
std::unordered_map<SeqNet, ComNet>
Simulator<SeqNet, ComNet>::
simulate(const Trace<SeqNet, ComNet>& trace,
         const unsigned depth,
         const std::unordered_map<SeqNet, ComNet>& previousLatchValues) {
    std::unordered_map<SeqNet, ComNet> nextLatchValues;
    // Compute next states for latches
    for (auto latch : seqCircuit_.getLatches()) {
        assert(nextLatchValues.find(latch) == nextLatchValues.end());
        const ComNet value = computeValue(trace,
                                          depth,
                                          previousLatchValues,
                                          seqCircuit_.getLatchNext(latch));
        nextLatchValues.insert(std::make_pair(latch, value));
    }
    return nextLatchValues;
}

template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
computeValue(const Trace<SeqNet, ComNet>& trace,
             const unsigned depth,
             const std::unordered_map<SeqNet, ComNet>& previousLatchValues,
             const SeqNet net) {
    if (overrideMode_) {
        if (trace.hasData(net, depth)) {
            return trace.getData(net, depth);
        }
    }

    if (seqCircuit_.isInput(net)) {
        if (trace.hasData(net, depth)) {
            // This data has supposedly been set by the user,
            // so we read it from here, unless it is something
            // not concrete
            const ComNet data = trace.getData(net, depth);
            if (comNetStore_->isConcrete(data)) {
                return data;
            }
	      }
        // Return a combinational symbolic value
        const std::string& name = seqNetStore_.toString(net);
        auto typeInfo = seqNetStore_.getNetTypeInfo(net);
        return comNetStore_->mkVariable(name, typeInfo);
    }
    if (seqCircuit_.isLatch(net)) {
        auto it = previousLatchValues.find(net);
        assert(it != previousLatchValues.end());
        return it->second;
    }
    else if (seqNetStore_.isFpaRounding(net)) {
        return comNetStore_->mkFpaRounding();
    }

    const unsigned nofChildren = seqNetStore_.getNofChildren(net);
    ComNet result;

    switch (nofChildren) {
    case 0:
        result = net::NetStoreUtils<SeqNet, ComNet>::
                translateConstantOrNumber(net, seqNetStore_, comNetStore_);
        break;
    case 1:
        result = computeValueFromOne(trace, depth, previousLatchValues, net);
        break;
    case 2:
        result = computeValueFromTwo(trace, depth, previousLatchValues, net);
        break;
    case 3:
        result = computeValueFromThree(trace, depth, previousLatchValues, net);
        break;
    default:
        result = computeValueFromN(trace, depth, previousLatchValues, net);
    }

    assert(result != ComNet());
    return result;
}

template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
computeValueFromOne(const Trace<SeqNet, ComNet>& trace,
                    const unsigned depth,
                    const std::unordered_map<SeqNet, ComNet>& prevLatchValues,
                    const SeqNet net) {
    // Recurse on child first
    ComNet child0 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 0));
    auto kind = seqNetStore_.getNetKind(net);
    if (std::get<0>(kind) == net::NetKind::Extract) {
        return comNetStore_->mkExtract(std::get<1>(kind), std::get<2>(kind), child0);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0);
}

template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
computeValueFromTwo(const Trace<SeqNet, ComNet>& trace,
                    const unsigned depth,
                    const std::unordered_map<SeqNet, ComNet>& prevLatchValues,
                    const SeqNet net) {
    // Recurse on first child first
    ComNet child0 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 0));
    // Recurse on second child first
    ComNet child1 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 1));
    auto kind = seqNetStore_.getNetKind(net);
    if (comNetStore_->isFpaRounding(child0)) {
        return comNetStore_->mkNet(std::get<0>(kind), child1);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0, child1);
}

template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
computeValueFromThree(const Trace<SeqNet, ComNet>& trace,
                      const unsigned depth,
                      const std::unordered_map<SeqNet, ComNet>& prevLatchValues,
                      const SeqNet net) {
    // Recurse on first child first
    ComNet child0 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 0));
    // Recurse on second child first
    ComNet child1 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 1));
    // Recurse on second child first
    ComNet child2 = computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, 2));
    auto kind = seqNetStore_.getNetKind(net);
    if (comNetStore_->isFpaRounding(child0)) {
        return comNetStore_->mkNet(std::get<0>(kind), child1, child2);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0, child1, child2);
}

template<class SeqNet, class ComNet>
ComNet
Simulator<SeqNet, ComNet>::
computeValueFromN(const Trace<SeqNet, ComNet>& trace,
                  const unsigned depth,
                  const std::unordered_map<SeqNet, ComNet>& prevLatchValues,
                  const SeqNet net) {
    const unsigned nofChildren = seqNetStore_.getNofChildren(net);
    assert(nofChildren > 3);
    std::vector<ComNet> computedChildren;
    for (unsigned child = 0;
         child < nofChildren;
         child++) {
        computedChildren.push_back(
                    computeValue(trace,
                                 depth,
                                 prevLatchValues,
                                 seqNetStore_.getChild(net, child)));
    }
    assert(nofChildren == computedChildren.size());
    auto kind = seqNetStore_.getNetKind(net);
    ComNet result = comNetStore_->mkNet(std::get<0>(kind),
                                        computedChildren[0],
                                        computedChildren[1]);
    for (unsigned child = 2; child < nofChildren; child++) {
        result = comNetStore_->mkNet(std::get<0>(kind), result, computedChildren[child]);
    }
    return result;
}

}  // namespace engine

#endif  // SRC_ENGINE_SIMULATOR_CPP_
