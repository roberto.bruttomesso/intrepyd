/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_TRACE_H_
#define SRC_ENGINE_TRACE_H_

#include <vector>
#include <string>
#include <unordered_map>
#include <ostream>

#include "src/net/NetStore.h"

namespace engine {

template<class SeqNet, class ComNet>
class Trace {
 public:
    Trace()
        : name_("unnamed"),
          maxDepth_(0)
    {}

    void setName(const std::string& name);
    void setData(SeqNet net, const unsigned depth, ComNet value);
    unsigned getNetsNumber() const;
    SeqNet getNet(unsigned i) const;
    unsigned getMaxDepth() const;
    ComNet getData(SeqNet net,
                   const unsigned depth) const;
    bool hasData(SeqNet net,
                 const unsigned depth) const;
    void clear();
    void print(std::ostream* out,
               const net::NetStore<SeqNet>& seqNetStore,
               const net::NetStore<ComNet>& comNetStore) const;

    using iterator = typename std::unordered_map<SeqNet, std::vector<ComNet>>::iterator;
    using const_iterator = typename std::unordered_map<SeqNet, std::vector<ComNet>>::const_iterator;

    iterator begin() { return data_.begin(); }
    iterator end() { return data_.end(); }
    const_iterator begin() const { return data_.begin(); }
    const_iterator end() const { return data_.end(); }

 protected:
    std::string name_;
    unsigned maxDepth_;
    std::unordered_map<SeqNet, std::vector<ComNet>> data_;
    std::vector<SeqNet> watchedNets_;
};

}  // namespace engine

#include "src/engine/Trace.cpp"

#endif  // SRC_ENGINE_TRACE_H_
