/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_TRACE_CPP_
#define SRC_ENGINE_TRACE_CPP_

#include <cassert>
#include <string>

#include "src/engine/Trace.h"
#include "src/exception/IntrepidException.h"

namespace engine {

template<class SeqNet, class ComNet>
void
Trace<SeqNet, ComNet>::setName(const std::string& name) {
    name_ = name;
}

template<class SeqNet, class ComNet>
void
Trace<SeqNet, ComNet>::setData(SeqNet net,
                               const unsigned depth,
                               ComNet value) {
    if (depth > maxDepth_) {
        maxDepth_ = depth;
    }
    if (data_.find(net) == data_.end()) {
        watchedNets_.push_back(net);
    }
    auto& values = data_[net];
    if (values.size() <= depth) {
        values.resize(depth + 1);
    }
    values[depth] = value;
    assert(data_.find(net) != data_.end());
    assert(data_.size() == watchedNets_.size());
}

template<class SeqNet, class ComNet>
unsigned
Trace<SeqNet, ComNet>::getNetsNumber() const {
    assert(data_.size() == watchedNets_.size());
    return static_cast<unsigned>(watchedNets_.size());
}

template<class SeqNet, class ComNet>
SeqNet
Trace<SeqNet, ComNet>::getNet(unsigned i) const {
    assert(i < watchedNets_.size());
    return watchedNets_[i];
}

template<class SeqNet, class ComNet>
unsigned
Trace<SeqNet, ComNet>::getMaxDepth() const {
    return maxDepth_;
}

template<class SeqNet, class ComNet>
bool
Trace<SeqNet, ComNet>::hasData(SeqNet net,
                               const unsigned depth) const {
    if (data_.find(net) == data_.end()) {
        return false;
    }
    const auto& values = data_.at(net);
    if (depth >= values.size()) {
        return false;
    }
    return true;
}

template<class SeqNet, class ComNet>
ComNet
Trace<SeqNet, ComNet>::getData(SeqNet net,
                               const unsigned depth) const {
    if (!hasData(net, depth)) {
        assert(false);
        throw exception::IntrepidException(
            "Cannot find value for net in trace (forgot to watch?)",
            __FILE__, __LINE__);
    }
    const auto& values = data_.at(net);
    assert(values[depth] != ComNet());
    return values[depth];
}

template<class SeqNet, class ComNet>
void
Trace<SeqNet, ComNet>::clear() {
    data_.clear();
}

template<class SeqNet, class ComNet>
void
Trace<SeqNet, ComNet>::
print(std::ostream* out,
      const net::NetStore<SeqNet>& seqNetStore,
      const net::NetStore<ComNet>& comNetStore) const {
    for (auto d : data_) {
        *out << seqNetStore.toString(d.first) << std::endl;
        unsigned step = 0;
        for (auto value : d.second) {
            if (comNetStore.isConcrete(value)) {
                std::string valueStr;
                if (comNetStore.getNetTypeInfo(value).isFloatingPoint()) {
                    ComNet realValue = comNetStore.mkRealFromFpa(value);
                    valueStr = comNetStore.toString(realValue);
                }
                else {
                    valueStr = comNetStore.toString(value);
                }
                *out << step << ": " << valueStr;
            } else {
                *out << step << ": ?";
            }
            *out << std::endl;
            step++;
        }
    }
}

}  // namespace engine

#endif  // SRC_ENGINE_TRACE_CPP_
