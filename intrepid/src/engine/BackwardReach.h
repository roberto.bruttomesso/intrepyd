/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ENGINE_BACKWARDREACH_H_
#define SRC_ENGINE_BACKWARDREACH_H_

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <deque>

#include "src/net/NetStore.h"
#include "src/solver/Solver.h"
#include "src/circuit/SeqCircuit.h"
#include "src/engine/Engine.h"
#include "src/engine/State.h"
#include "src/engine/QuantifierEliminator.h"
#include "src/engine/UnsatCoreExtractor.h"
#include "src/engine/Simulator.h"

namespace engine {

template<class SeqNet, class ComNet>
class BackwardReach final : public Engine<SeqNet, ComNet> {
 public:
    BackwardReach(factory::Factory<SeqNet, ComNet>* factory,
                  net::NetStore<SeqNet>* seqNetStore,
                  circuit::SeqCircuit<SeqNet>* seqCircuit);
    virtual ~BackwardReach();

    BackwardReach(const BackwardReach&) = delete;
    BackwardReach operator=(const BackwardReach&) = delete;

 private:
    void prepareForSolvingImpl() override;
    EngineResult findFirstReachableTargetImpl() override;
    void addWatchedNets(solver::Solver<SeqNet>* solver, SeqNet t);
    void addWatchedNetsRec(solver::Solver<SeqNet>* solver, SeqNet t);
    void addWatchedTheoryNetsRec(solver::Solver<SeqNet>* solver, SeqNet t);
    EngineResult enumerateAndAddStatesToFrontier(SeqNet net,
                                                 State<SeqNet>* parent);
    bool intersectsInitialStates(SeqNet net);
    void pushToFrontier(State<SeqNet>* state);
    State<SeqNet>* popFromFrontier();
    bool isFrontierEmpty() const;
    void addStatesToBlocked(State<SeqNet>* state);
    void retrieveInputs(SeqNet net,
                        std::unordered_set<SeqNet>* inputs,
                        std::unordered_set<SeqNet>* seen) const;
    bool containsInputs(SeqNet net) const;
    bool containsLatches(SeqNet net) const;
    bool isBooleanGate(SeqNet n) const;
    void computeTrace(State<SeqNet>* state);
    void computeTrace(SeqNet target);
    SeqNet generalizeCube(SeqNet cube, SeqNet target);
    void retrieveCubeConjuncts(SeqNet cube,
                               std::unordered_set<SeqNet>* atoms) const;
    void retrieveAtoms(SeqNet target,
                       std::unordered_set<SeqNet>* atoms) const;
    void retrieveBooleanInputs(const std::unordered_set<SeqNet>& atoms,
                               std::unordered_set<SeqNet>* inputs) const;
    SeqNet retrieveImportantAtoms(SeqNet simplifiedTarget, SeqNet target,
                                  const std::unordered_set<SeqNet>& atoms,
                                  std::unordered_set<SeqNet>* importantAtoms) const;
    SeqNet simplifyTarget(SeqNet target,
                          const std::unordered_set<SeqNet>& atoms,
                          SeqNet except) const;

    bool hasAtLeastTwoLatches(SeqNet net) const;
    int countTwoLatchesRec(SeqNet net) const;

    void validateUnreachability();
    SeqNet buildCandidateInvariant() const;
    bool checkContainsInitialStates(SeqNet net) const;
    bool checkNotIntersectingTargets(SeqNet net) const;
    bool checkIsInductive(SeqNet invariant);
    SeqNet eliminateTheoryInputs(SeqNet cube) const;
    SeqNet eliminateBooleanInputs(SeqNet cube) const;
    std::unordered_set<SeqNet> collectTheoryInputs(SeqNet predicate) const;
    void collectTheoryInputsRec(SeqNet net,
                                std::unordered_set<SeqNet>* inputs,
                                std::unordered_set<SeqNet>* cache) const;
    std::vector<SeqNet> collectTheoryAtoms(SeqNet cube) const;
    void collectTheoryAtomsRec(SeqNet cube, std::vector<SeqNet>* theoryAtoms) const;
    void afterRemoveTarget() override;

    using Engine<SeqNet, ComNet>::seqNetStore_;
    using Engine<SeqNet, ComNet>::seqCircuit_;
    using Engine<SeqNet, ComNet>::targets_;
    using Engine<SeqNet, ComNet>::factory_;
    using Engine<SeqNet, ComNet>::targetToTrace_;
    using Engine<SeqNet, ComNet>::watchedNets_;
    using Engine<SeqNet, ComNet>::lastReachedTargets_;
    using Engine<SeqNet, ComNet>::verbosity_;

    std::vector<State<SeqNet>*> stateSpace_;
    std::deque<State<SeqNet>*> frontier_;
    std::unique_ptr<solver::Solver<SeqNet>> blockedStates_;
    std::unique_ptr<solver::Solver<SeqNet>> initStates_;
    std::unique_ptr<SubstituteHelper<SeqNet>> preimageHelper_;
    std::unique_ptr<Simulator<SeqNet, ComNet>> simulator_;
    std::unordered_set<SeqNet> seqNetCache_;

    struct Statistics {
        unsigned depthReached {0};
        unsigned enumerations {0};
        unsigned maxFrontier {0};
        // Generalize
        unsigned genCalls {0};
        unsigned genSkippedLessTwoLatches {0};
        unsigned genSkippedInputs {0};
        unsigned genReducedAtoms {0};
        // QE
        unsigned qeCalls {0};
        unsigned qeTheoryInputs {0};
    };

    mutable Statistics statistics_;
};

}  // namespace engine

#include "src/engine/BackwardReach.cpp"

#endif  // SRC_ENGINE_BACKWARDREACH_H_
