/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#ifndef SRC_Z3_UNSAT_CORE_EXTRACTOR_H_
#define SRC_Z3_UNSAT_CORE_EXTRACTOR_H_

#include <vector>

#include "src/net/Z3Net.h"
#include "src/engine/UnsatCoreExtractor.h"

namespace engine {

template<class Net>
class Z3UnsatCoreExtractor final : public UnsatCoreExtractor<Net> {
 public:
    explicit Z3UnsatCoreExtractor(Z3_context context)
        : context_(context) {
          solver_ = Z3_mk_solver(context_);
          Z3_solver_inc_ref(context_, solver_);
          assert(solver_ != nullptr);
    }

    virtual ~Z3UnsatCoreExtractor() {
        Z3_solver_dec_ref(context_, solver_);
    }

 private:
    using UnsatCoreExtractor<Net>::netStore_;
    using UnsatCoreExtractor<Net>::markers_;
    using UnsatCoreExtractor<Net>::markid2index_;
    using UnsatCoreExtractor<Net>::retrieveConjuncts;
    using UnsatCoreExtractor<Net>::createMarkers;

    Net extractCoreImpl(Net formula, Net cube) override;

    Z3_context context_;
    Z3_solver solver_;
};

}  // namespace engine

#include "src/engine/Z3UnsatCoreExtractor.cpp"

#endif  // SRC_Z3_UNSAT_CORE_EXTRACTOR_H_
