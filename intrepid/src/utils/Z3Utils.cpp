/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <string>
#include <vector>

#include "src/exception/IntrepidException.h"
#include "src/utils/Z3Utils.h"

namespace utils {

std::unordered_map<std::string, Z3_sort> Z3Utils::enumName2sort_;
std::unordered_map<Z3_sort, std::string> Z3Utils::sort2enumName_;

void
Z3Utils::initialize() {
    enumName2sort_.clear();
    sort2enumName_.clear();
}

Z3_sort
Z3Utils::typeToSort(Z3_context context, net::NetTypeInfo t) {
    assert(context != nullptr);
    Z3_sort sort = nullptr;
    const unsigned size = t.getSize();
    switch (t.getNetType()) {
    case net::NetType::Boolean:
        sort = Z3_mk_bool_sort(context);
        break;
    case net::NetType::Int:
    case net::NetType::Uint:
        sort = Z3_mk_bv_sort(context, size);
        break;
    case net::NetType::Float:
        if (size == 16) {
            sort = Z3_mk_fpa_sort_16(context);
        } else if (size == 32) {
            sort = Z3_mk_fpa_sort_32(context);
        } else if (size == 64) {
            sort = Z3_mk_fpa_sort_64(context);
        } else {
            assert(false);
            throw exception::IntrepidException("Unhandled fp size type",
                                               __FILE__,
                                               __LINE__);
        }
        break;
    case net::NetType::InfInt:
        sort = Z3_mk_int_sort(context);
        break;
    case net::NetType::Real:
        sort = Z3_mk_real_sort(context);
        break;
    case net::NetType::UserEnum:
        assert(enumName2sort_.find(t.getUserEnum()) != enumName2sort_.end());
        sort = enumName2sort_.at(t.getUserEnum());
        break;
    default:
        assert(false);
        throw exception::IntrepidException("Unhandled type",
                                           __FILE__,
                                           __LINE__);
    }
    assert(sort != nullptr);
    return sort;
}

net::NetTypeInfo
Z3Utils::sortToType(Z3_context context, Z3_sort sort) {
    assert(context != nullptr);
    assert(sort != nullptr);
    net::NetType type = net::NetType::Unknown;
    unsigned size = 0;

    auto it = sort2enumName_.find(sort);
    if (it != sort2enumName_.end()) {
        return net::NetTypeInfo::mkUserEnumType(it->second);
    }

    Z3_sort_kind kind = Z3_get_sort_kind(context, sort);
    switch (kind) {
    case Z3_BOOL_SORT:
        type = net::NetType::Boolean;
        break;
    case Z3_BV_SORT:
        type = net::NetType::Int;
        size = Z3_get_bv_sort_size(context, sort);
        break;
    case Z3_INT_SORT:
        type = net::NetType::InfInt;
        break;
    case Z3_REAL_SORT:
        type = net::NetType::Real;
        break;
    case Z3_FLOATING_POINT_SORT:
        {
            type = net::NetType::Float;
            size = Z3_fpa_get_ebits(context, sort);
            switch (size) {
            case 5:
                size = 16;
                break;
            case 8:
                size = 32;
                break;
            case 11:
                size = 64;
                break;
            default:
                throw exception::IntrepidException("Unhandled size",
                                                   __FILE__,
                                                   __LINE__);
            }
        }
        break;
    default:
        throw exception::IntrepidException("Unhandled type",
                                           __FILE__,
                                           __LINE__);
    }

    return net::NetTypeInfo::mkNativeType(type, size);
}

std::string
Z3Utils::toString(Z3_context context, Z3_ast ast) {
    if (ast == nullptr) {
        return "null";
    }
    std::string result;
    return Z3_ast_to_string(context, ast);
}

solver::SolverResult
Z3Utils::z3ResultToSolverResult(Z3_lbool result) {
    switch (result) {
    case Z3_L_TRUE:
        return solver::SolverResult::Satisfiable;
    case Z3_L_FALSE:
        return solver::SolverResult::Unsatisfiable;
    case Z3_L_UNDEF:
        return solver::SolverResult::Unknown;
    default:
        exception::IntrepidException("Unexpected Z3_lbool value",
                                     __FILE__,
                                     __LINE__);
    }
    return solver::SolverResult::Unknown;
}

std::vector<Z3_func_decl>
Z3Utils::mkEnumFuncDecl(Z3_context context,
                        const std::string& enumName,
                        const std::vector<std::string>& enumValues) {
    Z3_symbol enumSymb = Z3_mk_string_symbol(context, enumName.c_str());

    std::vector<Z3_symbol> z3EnumValues;
    for (auto value : enumValues) {
        z3EnumValues.push_back(Z3_mk_string_symbol(context, value.c_str()));
    }

    Z3_func_decl* valueConsts = new Z3_func_decl[enumValues.size()];
    Z3_func_decl* valueTesters = new Z3_func_decl[enumValues.size()];

    Z3_sort sort =
            Z3_mk_enumeration_sort(context,
                                   enumSymb,
                                   static_cast<unsigned>(z3EnumValues.size()),
                                   z3EnumValues.data(),
                                   valueConsts,
                                   valueTesters);

    if (enumName2sort_.find(enumName) != enumName2sort_.end()) {
        exception::IntrepidException("Enum already declared",
                                     __FILE__,
                                     __LINE__);
    }
    enumName2sort_.insert(std::make_pair(enumName, sort));
    sort2enumName_.insert(std::make_pair(sort, enumName));

    std::vector<Z3_func_decl> result;
    for (unsigned i = 0; i < enumValues.size(); i++) {
        result.push_back(valueConsts[i]);
    }

    return result;
}

}  // namespace utils
