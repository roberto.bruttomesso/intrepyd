/*
 * Copyright (c) 2017, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author(s).
 */

#include <cassert>
#include <string>
#include <regex>
#include <tuple>
#include <limits>

#include "src/exception/IntrepidException.h"
#include "src/utils/SMTUtils.h"

namespace utils {

const std::regex
SMTUtils::pattern_("\\(fp #b([01]) #([bx][0-9A-Fa-f]+) #([bx][0-9A-Fa-f]+)\\)");

const std::regex
SMTUtils::zero_("\\(_ ([\\+\\-])zero [0-9]+ [0-9]+\\)");

const std::regex
SMTUtils::inf_("\\(_ [\\+\\-]oo ([0-9]+) [0-9]+\\)");

bool
SMTUtils::
isFpNumber(const std::string& fpNumberString) {
    std::smatch matches;
    if (std::regex_match(fpNumberString, matches, pattern_)) {
        return true;
    }
    if (std::regex_match(fpNumberString, matches, zero_)) {
        return true;
    }
    if (std::regex_match(fpNumberString, matches, inf_)) {
        return true;
    }
    return false;
}

std::tuple<bool, int32_t, uint64_t>
SMTUtils::
getFpNumberComponents(const std::string& fpNumberString) {
    assert(isFpNumber(fpNumberString));
    std::smatch matches;
    if (std::regex_match(fpNumberString, matches, pattern_)) {
        // Floating point number
        assert(matches.size() == 4);
        bool sign = matches[1].str() == "1";
        const std::string expString = matches[2];
        const std::string sigString = matches[3];
        assert(expString[0] == 'b' || expString[0] == 'x');
        assert(sigString[0] == 'b' || sigString[0] == 'x');
        const int32_t exp = std::stoi(expString.substr(1),
                                      0,
                                      (expString[0] == 'b' ? 2 : 16));
        const uint64_t sig = std::stoull(sigString.substr(1),
                                         0,
                                         (sigString[0] == 'b' ? 2 : 16));
        return std::make_tuple(sign, exp, sig);
    } else if (std::regex_match(fpNumberString, matches, zero_)) {
        assert(matches.size() == 2);
        const bool sign = matches[1].str() == "-";
        return std::make_tuple(sign, 0, 0);
    } else if (std::regex_match(fpNumberString, matches, inf_)) {
        assert(matches.size() == 3);
        const bool sign = matches[1].str() == "-";
        int bexp;
        switch (std::stoi(matches[2].str())) {
        case 4:
            bexp = 15;
            break;
        case 8:
            bexp = 255;
            break;
        case 12:
            bexp = 4095;
            break;
        default:
            assert(false);
        }
        return std::make_tuple(sign, bexp, 0);
    }
    assert(false);
    return std::make_tuple(false, 0, 0);
}

}  // namespace utils
