/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_UTILS_Z3UTILS_H_
#define SRC_UTILS_Z3UTILS_H_

#include <string>
#include <vector>

#include "src/net/Z3Net.h"
#include "src/net/NetTypeInfo.h"
#include "src/solver/SolverResult.h"

namespace utils {

class Z3Utils final {
 public:
    Z3Utils() = delete;

    static void initialize();
    static Z3_sort typeToSort(Z3_context context, net::NetTypeInfo t);
    static net::NetTypeInfo sortToType(Z3_context context, Z3_sort sort);
    static std::string toString(Z3_context context, Z3_ast ast);
    static solver::SolverResult z3ResultToSolverResult(Z3_lbool result);
    static std::vector<Z3_func_decl>
                mkEnumFuncDecl(Z3_context context,
                               const std::string& enumName,
                               const std::vector<std::string> &enumValues);

 private:
    static std::unordered_map<std::string, Z3_sort> enumName2sort_;
    static std::unordered_map<Z3_sort, std::string> sort2enumName_;
};

}  // namespace utils

#endif  // SRC_UTILS_Z3UTILS_H_
