/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author.
 */

#include <vector>
#include <string>
#include <algorithm>

#include "src/net/Z3SeqNet.h"
#include "src/net/Z3ComNet.h"
#include "src/context/Context.h"

namespace context {

const std::string Context::UNKNOWN_VALUE = "?";

Context::Context()
    : seqNetStore_(factory_.buildSeqNetStore()),
      comNetStore_(factory_.buildComNetStore()),
      booleanType_(net::NetTypeInfo::mkNativeType(net::NetType::Boolean)),
      int8Type_(net::NetTypeInfo::mkNativeType(net::NetType::Int, 8)),
      int16Type_(net::NetTypeInfo::mkNativeType(net::NetType::Int, 16)),
      int32Type_(net::NetTypeInfo::mkNativeType(net::NetType::Int, 32)),
      int64Type_(net::NetTypeInfo::mkNativeType(net::NetType::Int, 64)),
      uint8Type_(net::NetTypeInfo::mkNativeType(net::NetType::Uint, 8)),
      uint16Type_(net::NetTypeInfo::mkNativeType(net::NetType::Uint, 16)),
      uint32Type_(net::NetTypeInfo::mkNativeType(net::NetType::Uint, 32)),
      uint64Type_(net::NetTypeInfo::mkNativeType(net::NetType::Uint, 64)),
      float16Type_(net::NetTypeInfo::mkNativeType(net::NetType::Float, 16)),
      float32Type_(net::NetTypeInfo::mkNativeType(net::NetType::Float, 32)),
      float64Type_(net::NetTypeInfo::mkNativeType(net::NetType::Float, 64)),
      realType_(net::NetTypeInfo::mkNativeType(net::NetType::Real)),
      intType_(net::NetTypeInfo::mkNativeType(net::NetType::InfInt, 0)) // Infinite precision
{
    mkCircuit("circuit");
}

circuit::SeqCircuit<SEQNET>*
Context::mkCircuit(const std::string& name) {
    circuit_ = factory_.buildSeqCircuit(name);
    auto circuit = circuit_.get();
    makeContextAwareOf(circuit);
    return circuit;
}

circuit::SeqCircuit<SEQNET>*
Context::getCircuit() const {
    return circuit_.get();
}

engine::Bmc<SEQNET, COMNET>*
Context::mkEngineBmc(circuit::SeqCircuit<SEQNET>* circ) {
    bmcEngines_.push_back(
        std::unique_ptr<engine::Bmc<SEQNET, COMNET>>(
                new engine::Bmc<SEQNET, COMNET>(&factory_,
                                                seqNetStore_.get(),
                                                circ)));
    return bmcEngines_.back().get();
}

engine::BackwardReach<SEQNET, COMNET>*
Context::mkEngineBackwardReach(circuit::SeqCircuit<SEQNET>* circ) {
    brEngines_.push_back(
        std::unique_ptr<engine::BackwardReach<SEQNET, COMNET>>(
                new engine::BackwardReach<SEQNET, COMNET>(&factory_,
                                                          seqNetStore_.get(),
                                                          circ)));
    return brEngines_.back().get();
}

engine::Trace<SEQNET, COMNET>*
Context::mkTrace() {
    traces_.push_back(
        std::unique_ptr<engine::Trace<SEQNET, COMNET>>(
            new engine::Trace<SEQNET, COMNET>()
            )
    );
    return traces_.back().get();
}

engine::Simulator<SEQNET, COMNET>*
Context::mkSimulator(circuit::SeqCircuit<SEQNET>* circ) {
    simulators_.push_back(
        std::unique_ptr<engine::Simulator<SEQNET, COMNET>>(
           new engine::Simulator<SEQNET, COMNET>(*seqNetStore_,
                                                 *circ,
                                                 comNetStore_.get())));
    return simulators_.back().get();
}

unsigned
Context::mkUndef() {
    return storeAndReturn(SEQNET());
}

unsigned
Context::mkTrue() {
    return storeAndReturn(seqNetStore_->mkTrue());
}

unsigned
Context::mkFalse() {
    return storeAndReturn(seqNetStore_->mkFalse());
}

unsigned
Context::mkNumber(const std::string& name, net::NetTypeInfo* t) {
    return storeAndReturn(seqNetStore_->mkNumber(name, *t));
}

unsigned
Context::mkNot(unsigned x) {
    SEQNET xn = getNetFromUnsigned(x);
    return storeAndReturn(seqNetStore_->mkNot(xn));
}

unsigned
Context::mkMinus(unsigned x) {
    SEQNET xn = getNetFromUnsigned(x);
    return storeAndReturn(seqNetStore_->mkMinus(xn));
}

unsigned
Context::mkAnd(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkAnd(xn, yn));
}

unsigned
Context::mkOr(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkOr(xn, yn));
}

unsigned
Context::mkXor(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkXor(xn, yn));
}

unsigned
Context::mkIff(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkIff(xn, yn));
}

unsigned
Context::mkAdd(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkAdd(xn, yn));
}

unsigned
Context::mkSub(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkSub(xn, yn));
}

unsigned
Context::mkMul(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkMul(xn, yn));
}

unsigned
Context::mkDiv(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkDiv(xn, yn));
}

unsigned
Context::mkMod(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkMod(xn, yn));
}

unsigned
Context::mkEq(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkEq(xn, yn));
}

unsigned
Context::mkLeq(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    auto mkLeqIntSigned = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLeqIntSigned(xn, yn));
    };
    auto mkLeqIntUnsigned = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLeqIntUnsigned(xn, yn));
    };
    auto mkLeqReal = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLeqReal(xn, yn));
    };
    auto mkLeqFloat = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLeqFloat(xn, yn));
    };
    return mkRelOpHelper(xn, yn,
                         mkLeqIntSigned,
                         mkLeqIntUnsigned,
                         mkLeqReal,
                         mkLeqFloat);
}

unsigned
Context::mkLt(unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    auto mkLtIntSigned = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLtIntSigned(xn, yn));
    };
    auto mkLtIntUnsigned = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLtIntUnsigned(xn, yn));
    };
    auto mkLtReal = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLtReal(xn, yn));
    };
    auto mkLtFloat = [this] (SEQNET xn, SEQNET yn) {
        return storeAndReturn(seqNetStore_->mkLtFloat(xn, yn));
    };
    return mkRelOpHelper(xn, yn,
                         mkLtIntSigned,
                         mkLtIntUnsigned,
                         mkLtReal,
                         mkLtFloat);
}

template<class RelOpIntSigned, class RelOpIntUnsigned,
         class RelOpReal, class RelOpFloat>
unsigned
Context::mkRelOpHelper(SEQNET x, SEQNET y,
                       RelOpIntSigned relOpIntSigned,
                       RelOpIntUnsigned relOpIntUnsigned,
                       RelOpReal relOpReal,
                       RelOpFloat relOpFloat) {
    net::NetTypeInfo tx = seqNetStore_->getNetTypeInfo(x);
    net::NetTypeInfo ty = seqNetStore_->getNetTypeInfo(y);
    if (!tx.isNativeType() || !ty.isNativeType()) {
        throw exception::IntrepidException(
                    "Expected native types in relational op",
                    __FILE__, __LINE__);
    }
    net::NetType txt = tx.getNetType();
    net::NetType tyt = ty.getNetType();
    if (txt != tyt) {
        auto xIsNumber = seqNetStore_->isNumber(x);
        auto yIsNumber = seqNetStore_->isNumber(y);
        if (xIsNumber && yIsNumber) {
            throw exception::IntrepidException(
                        "Comparing two constants ?",
                        __FILE__, __LINE__);
        } else if (xIsNumber || yIsNumber) {
            if (xIsNumber) {
                std::swap(txt, tyt);
            }
        } else {
            /*
            std::cout << "x: " << seqNetStore_->toString(x) << std::endl;
            std::cout << "y: " << seqNetStore_->toString(y) << std::endl;
            throw exception::IntrepidException(
                "Incompatible input types to relational op",
                __FILE__, __LINE__);
            */
        }
    }
    switch (txt) {
    case net::NetType::Real:
    case net::NetType::InfInt:
        return relOpReal(x, y);
    case net::NetType::Float:
        return relOpFloat(x, y);
    case net::NetType::Int:
        return relOpIntSigned(x, y);
    case net::NetType::Uint:
        return relOpIntUnsigned(x, y);
    case net::NetType::Unknown:
    case net::NetType::Boolean:
    default:
        throw exception::IntrepidException("Unhandled type",
                                           __FILE__, __LINE__);
    }
    return mkUndef();
}

unsigned
Context::mkGeq(unsigned x, unsigned y) {
    return mkLeq(y, x);
}

unsigned
Context::mkGt(unsigned x, unsigned y) {
    return mkLt(y, x);
}

unsigned
Context::mkIte(unsigned i, unsigned t, unsigned e) {
    SEQNET in = getNetFromUnsigned(i);
    SEQNET tn = getNetFromUnsigned(t);
    SEQNET en = getNetFromUnsigned(e);
    return storeAndReturn(seqNetStore_->mkIte(in, tn, en));
}

unsigned
Context::mkGetBit(unsigned x, unsigned bit) {
    SEQNET xn = getNetFromUnsigned(x);
    return storeAndReturn(seqNetStore_->mkGetBit(xn, bit));
}

unsigned
Context::mkSetBit(unsigned x, unsigned bit, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkSetBit(xn, bit, yn));
}

unsigned
Context::mkEnumValue(const std::string& enumName,
                     const std::string& enumValue) {
    return storeAndReturn(seqNetStore_->mkEnumValue(enumName, enumValue));
}

unsigned
Context::mkNet(net::NetKind kind, unsigned x) {
    SEQNET xn = getNetFromUnsigned(x);
    return storeAndReturn(seqNetStore_->mkNet(kind, xn));
}

unsigned
Context::mkNet(net::NetKind kind, unsigned x, unsigned y) {
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkNet(kind, xn, yn));
}

unsigned
Context::mkNet(net::NetKind kind, unsigned z, unsigned x, unsigned y) {
    SEQNET zn = getNetFromUnsigned(z);
    SEQNET xn = getNetFromUnsigned(x);
    SEQNET yn = getNetFromUnsigned(y);
    return storeAndReturn(seqNetStore_->mkNet(kind, zn, xn, yn));
}

unsigned
Context::mkInput(circuit::SeqCircuit<SEQNET>* circuit,
                 const std::string& name,
                 net::NetTypeInfo* t) {
    const std::string nameInCirc = mangleName(name);
    SEQNET inputn = seqNetStore_->mkVariable(nameInCirc, *t);
    unsigned input = storeAndReturn(inputn);
    circuit->mkInput(inputn);
    return input;
}

void
Context::mkOutput(circuit::SeqCircuit<SEQNET>* circuit,
                  unsigned x) {
    SEQNET xn = getNetFromUnsigned(x);
    circuit->mkOutput(xn);
}

void
Context::pushAssumption(circuit::SeqCircuit<SEQNET>* circuit,
                      unsigned x) {
    SEQNET xn = getNetFromUnsigned(x);
    circuit->pushAssumption(xn);
}

void
Context::popAssumption(circuit::SeqCircuit<SEQNET>* circuit) {
    circuit->popAssumption();
}

unsigned
Context::mkLatch(circuit::SeqCircuit<SEQNET>* circuit,
                 const std::string& name, net::NetTypeInfo* t) {
    const std::string nameInCirc = mangleName(name);
    SEQNET latchn = seqNetStore_->mkVariable(nameInCirc, *t);
    unsigned latch = storeAndReturn(latchn);
    circuit->mkLatch(latchn);
    return latch;
}

void
Context::setLatchInitNext(circuit::SeqCircuit<SEQNET>* circuit,
                          unsigned l, unsigned i, unsigned n) {
    SEQNET ln = getNetFromUnsigned(l);
    SEQNET in = getNetFromUnsigned(i);
    SEQNET nn = getNetFromUnsigned(n);
    circuit->setLatchInitNext(ln, in, nn);
}

bool
Context::isUndef(unsigned n) const {
    SEQNET undef;
    return n == undef;
}

bool
Context::isTrue(unsigned n) const {
    return n == seqNetStore_->mkTrue();
}

bool
Context::isFalse(unsigned n) const {
    return n == seqNetStore_->mkFalse();
}

bool
Context::isNumber(unsigned n) const {
    SEQNET nn = getNetFromUnsigned(n);
    return seqNetStore_->isNumber(nn);
}

bool
Context::isNot(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isNot(xn);
}

bool
Context::isAnd(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isAnd(xn);
}

bool
Context::isOr(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isOr(xn);
}

bool
Context::isXor(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isXor(xn);
}

bool
Context::isIff(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isIff(xn);
}

bool
Context::isAdd(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isAdd(xn);
}

bool
Context::isSub(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isSub(xn);
}

bool
Context::isMinus(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isMinus(xn);
}

bool
Context::isMul(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isMul(xn);
}

bool
Context::isEq(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isEq(xn);
}

bool
Context::isLeq(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isLeq(xn);
}

bool
Context::isLt(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isLt(xn);
}

bool
Context::isGeq(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isGeq(xn);
}

bool
Context::isGt(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->isGt(xn);
}

bool
Context::isInput(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return circuit->isInput(xn);
}

bool
Context::isOutput(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return circuit->isOutput(xn);
}

bool
Context::isLatch(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return circuit->isLatch(xn);
}

net::NetTypeInfo
Context::getNetTypeInfo(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->getNetTypeInfo(xn);
}

net::NetTypeInfo
Context::getValueTypeInfo(unsigned x) const {
    COMNET xn = getValueFromUnsigned(x);
    return comNetStore_->getNetTypeInfo(xn);
}

unsigned
Context::getNofChildren(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->getNofChildren(xn);
}

unsigned
Context::getChild(unsigned x, unsigned i) {
    SEQNET xn = getNetFromUnsigned(x);
    return storeAndReturn(seqNetStore_->getChild(xn, i));
}

std::string
Context::toString(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->toString(xn);
}

unsigned
Context::getLatchInit(circuit::SeqCircuit<SEQNET>* circuit,
                      unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return circuit->getLatchInit(xn);
}

unsigned
Context::getLatchNext(circuit::SeqCircuit<SEQNET>* circuit,
                      unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return circuit->getLatchNext(xn);
}

std::vector<unsigned>
Context::getLatches(circuit::SeqCircuit<SEQNET>* circuit) {
    std::vector<unsigned> ret;
    for (auto latch : circuit->getLatches()) {
        ret.push_back(storeAndReturn(latch));
    }
    return ret;
}

/*
net::NetKind
Context::getNetKind(unsigned x) const {
    SEQNET xn = getNetFromUnsigned(x);
    return seqNetStore_->getNetKind(xn);
}
*/

unsigned
Context::getDefaultInitFromType(net::NetTypeInfo t) {
    return storeAndReturn(seqNetStore_->getDefaultInitFromType(t));
}

void
Context::adjustNumberTypes(unsigned* x, unsigned* y) {
    SEQNET xn = getNetFromUnsigned(*x);
    SEQNET yn = getNetFromUnsigned(*y);
    seqNetStore_->adjustNumberTypes(&xn, &yn);
    *x = storeAndReturn(xn);
    *y = storeAndReturn(yn);
}

unsigned
Context::mkCastToInt8Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkInt8Type()));
}

unsigned
Context::mkCastToInt16Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkInt16Type()));
}

unsigned
Context::mkCastToInt32Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkInt32Type()));
}

unsigned
Context::mkCastToInt64Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkInt64Type()));
}

unsigned
Context::mkCastToUint8Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkUint8Type()));
}

unsigned
Context::mkCastToUint16Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkUint16Type()));
}

unsigned
Context::mkCastToUint32Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkUint32Type()));
}

unsigned
Context::mkCastToUint64Type(unsigned x) {
  SEQNET xn = getNetFromUnsigned(x);
  return storeAndReturn(seqNetStore_->castToType(xn, *mkUint64Type()));
}

net::NetTypeInfo*
Context::mkBooleanType() {
    return &booleanType_;
}

net::NetTypeInfo*
Context::mkInt8Type() {
    return &int8Type_;
}

net::NetTypeInfo*
Context::mkInt16Type() {
    return &int16Type_;
}

net::NetTypeInfo*
Context::mkInt32Type() {
    return &int32Type_;
}

net::NetTypeInfo*
Context::mkInt64Type() {
    return &int64Type_;
}

net::NetTypeInfo*
Context::mkUint8Type() {
    return &uint8Type_;
}

net::NetTypeInfo*
Context::mkUint16Type() {
    return &uint16Type_;
}

net::NetTypeInfo*
Context::mkUint32Type() {
    return &uint32Type_;
}

net::NetTypeInfo*
Context::mkUint64Type() {
    return &uint64Type_;
}

net::NetTypeInfo*
Context::mkIntType() {
    return &intType_;
}

net::NetTypeInfo*
Context::mkRealType() {
    return &realType_;
}

net::NetTypeInfo*
Context::mkFloat16Type() {
    return &float16Type_;
}

net::NetTypeInfo*
Context::mkFloat32Type() {
    return &float32Type_;
}

net::NetTypeInfo*
Context::mkFloat64Type() {
    return &float64Type_;
}

net::NetTypeInfo*
Context::declareUserEnumType(const std::string& enumName,
                             const std::vector<std::string>& enumValues) {
    if (enumName2netTypeInfo_.find(enumName) != enumName2netTypeInfo_.end()) {
        throw exception::IntrepidException(
                    "Enum " + enumName + " already declared",
                    __FILE__, __LINE__);
    }
    auto it = enumName2netTypeInfo_.insert(
                        std::make_pair(enumName,
                                       seqNetStore_->mkUserEnum(
                                           enumName,
                                           enumValues))).first;
    return &(it->second);
}

net::NetTypeInfo*
Context::mkUserEnumType(const std::string& enumName) {
    auto it = enumName2netTypeInfo_.find(enumName);
    if (it == enumName2netTypeInfo_.end()) {
        throw exception::IntrepidException("Undeclared " + enumName,
                                           __FILE__, __LINE__);
    }
    return &(it->second);
}

unsigned
Context::storeAndReturn(SEQNET n) {
    unsigned ret = n;
    unsigned2net_.insert(std::make_pair(ret, n));
    return ret;
}

unsigned
Context::storeAndReturn(COMNET n) {
    unsigned ret = n;
    unsigned2value_.insert(std::make_pair(ret, n));
    return ret;
}

void
Context::makeContextAwareOf(circuit::SeqCircuit<SEQNET>* circuit) {
    auto storeAndReturnHelper = [this] (const std::vector<SEQNET>& nets) {
        for (auto net : nets) {
            storeAndReturn(net);
        }
    };
    storeAndReturnHelper(circuit->getInputs());
    storeAndReturnHelper(circuit->getOutputs());
    storeAndReturnHelper(circuit->getAssumptions());
}

SEQNET
Context::getNetFromUnsigned(unsigned x) const {
    if (isTrue(x)) {
        return seqNetStore_->mkTrue();
    } else if (isFalse(x)) {
        return seqNetStore_->mkFalse();
    }
    auto it = unsigned2net_.find(x);
    assert(it != unsigned2net_.end());
    return it->second;
}

COMNET
Context::getValueFromUnsigned(unsigned x) const {
    auto it = unsigned2value_.find(x);
    assert(it != unsigned2value_.end());
    return it->second;
}

void
Context::getValueAsString(COMNET value, std::string* output) {
    *output = UNKNOWN_VALUE;
    if (comNetStore_->isConcrete(value)) {
        std::string strValue;
        if (comNetStore_->getNetTypeInfo(value).isFloatingPoint()) {
            strValue = comNetStore_->toString(comNetStore_->mkRealFromFpa(value));
        }
        else {
            strValue = comNetStore_->toString(value);
        }
        *output = strValue;
    }
}

void
Context::getNetAsString(SEQNET value, std::string* output) {
    assert(output != nullptr);
    auto seqNetStore = factory_.buildSeqNetStore();
    *output = UNKNOWN_VALUE;
    *output = seqNetStore->toString(value);
}

void
Context::setTraceValue(engine::Trace<SEQNET, COMNET>* trace,
                       SEQNET net,
                       unsigned depth,
                       const char* value) {
    assert(trace != nullptr);
    COMNET val = COMNET();
    std::string strValue(value);
    std::transform(strValue.begin(), strValue.end(), strValue.begin(), ::toupper);
    if (strValue == "TRUE" || strValue == "T") {
        val = comNetStore_->mkTrue();
    } else if (strValue == "FALSE" || strValue == "F") {
        val = comNetStore_->mkFalse();
    } else if (strValue == UNKNOWN_VALUE) {
        // Do nothing
    } else {
        val = comNetStore_->mkNumber(value,
                                     seqNetStore_->getNetTypeInfo(net));
    }
    trace->setData(net, depth, val);
}

void
Context::pushNamespace(const char* name) {
    namespaces_.push_back(name);
}

void
Context::popNamespace() {
    assert(!namespaces_.empty());
    namespaces_.pop_back();
}

std::string
Context::mangleName(const std::string& name) const {
    std::string prefix;
    for (auto ns : namespaces_) {
        prefix += ns + ".";
    }
    return prefix + name;
}

unsigned
Context::mkSubstitute(unsigned term,
                      unsigned newTerm,
                      unsigned oldTerm) {
    SEQNET tn = getNetFromUnsigned(term);
    SEQNET nn = getNetFromUnsigned(newTerm);
    SEQNET on = getNetFromUnsigned(oldTerm);
    return storeAndReturn(seqNetStore_->mkSubstitute(tn, nn, on));
}

}  // namespace context
