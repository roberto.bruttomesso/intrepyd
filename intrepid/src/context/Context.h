/*
 * Copyright (c) 2016, Roberto Bruttomesso
 * All rights reserved.
 *
 * Use, modification, and redistribution of this file is
 * forbidden without the explicit authorization from the
 * author.
 */

#ifndef SRC_CONTEXT_CONTEXT_H_
#define SRC_CONTEXT_CONTEXT_H_

#include <memory>
#include <list>
#include <string>
#include <vector>

#include "src/IntrepidConfig.h"
#include "src/net/NetTypeInfo.h"
#include "src/net/NetKind.h"
#include "src/engine/Bmc.h"
#include "src/engine/BackwardReach.h"
#include "src/engine/Engine.h"

namespace context {

class Context {
 public:
    Context();
    virtual ~Context() {}

    circuit::SeqCircuit<SEQNET>* mkCircuit(const std::string& name);
    circuit::SeqCircuit<SEQNET>* getCircuit() const;
    engine::Bmc<SEQNET, COMNET>* mkEngineBmc(circuit::SeqCircuit<SEQNET>* circ);
    engine::BackwardReach<SEQNET, COMNET>*
        mkEngineBackwardReach(circuit::SeqCircuit<SEQNET>* circ);
    engine::Simulator<SEQNET, COMNET>*
        mkSimulator(circuit::SeqCircuit<SEQNET>* circ);
    engine::Trace<SEQNET, COMNET>* mkTrace();

    unsigned mkUndef();
    unsigned mkTrue();
    unsigned mkFalse();
    unsigned mkNumber(const std::string& name, net::NetTypeInfo* t);
    unsigned mkNot(unsigned x);
    unsigned mkMinus(unsigned x);
    unsigned mkAnd(unsigned x, unsigned y);
    unsigned mkOr(unsigned x, unsigned y);
    unsigned mkXor(unsigned x, unsigned y);
    unsigned mkIff(unsigned x, unsigned y);
    unsigned mkAdd(unsigned x, unsigned y);
    unsigned mkSub(unsigned x, unsigned y);
    unsigned mkMul(unsigned x, unsigned y);
    unsigned mkDiv(unsigned x, unsigned y);
    unsigned mkMod(unsigned x, unsigned y);
    unsigned mkEq(unsigned x, unsigned y);
    unsigned mkIte(unsigned i, unsigned t, unsigned e);
    unsigned mkLeq(unsigned x, unsigned y);
    unsigned mkLt(unsigned x, unsigned y);
    unsigned mkGeq(unsigned x, unsigned y);
    unsigned mkGt(unsigned x, unsigned y);
    unsigned mkGetBit(unsigned n, unsigned bit);
    unsigned mkSetBit(unsigned n, unsigned bit, unsigned y);

    unsigned mkEnumValue(const std::string& enumName,
                         const std::string& enumValue);

    unsigned mkSubstitute(unsigned term,
                          unsigned newTerm,
                          unsigned oldTerm);

    unsigned mkInput(circuit::SeqCircuit<SEQNET>* circuit,
                     const std::string& name,
                     net::NetTypeInfo* t);

    void mkOutput(circuit::SeqCircuit<SEQNET>* circuit,
                  unsigned x);

    void pushAssumption(circuit::SeqCircuit<SEQNET>* circuit,
                        unsigned x);

    void popAssumption(circuit::SeqCircuit<SEQNET>* circuit);

    unsigned mkLatch(circuit::SeqCircuit<SEQNET>* circuit,
                     const std::string& name,
                     net::NetTypeInfo* t);

    void setLatchInitNext(circuit::SeqCircuit<SEQNET>* circuit,
                          unsigned l, unsigned i, unsigned n);

    bool isUndef(unsigned n) const;
    bool isTrue(unsigned n) const;
    bool isFalse(unsigned n) const;
    bool isNumber(unsigned n) const;
    bool isNot(unsigned x) const;
    bool isMinus(unsigned x) const;
    bool isAnd(unsigned x) const;
    bool isOr(unsigned x) const;
    bool isXor(unsigned x) const;
    bool isIff(unsigned x) const;
    bool isSub(unsigned x) const;
    bool isAdd(unsigned x) const;
    bool isMul(unsigned x) const;
    bool isEq(unsigned x) const;
    bool isLeq(unsigned x) const;
    bool isLt(unsigned x) const;
    bool isGeq(unsigned x) const;
    bool isGt(unsigned x) const;

    bool isInput(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const;
    bool isOutput(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const;
    bool isLatch(circuit::SeqCircuit<SEQNET>* circuit, unsigned x) const;

    unsigned getNofChildren(unsigned x) const;
    unsigned getChild(unsigned x, unsigned i);
    std::string toString(unsigned x) const;

    unsigned getLatchInit(circuit::SeqCircuit<SEQNET>* circuit,
                          unsigned x) const;

    unsigned getLatchNext(circuit::SeqCircuit<SEQNET>* circuit,
                          unsigned x) const;

    std::vector<unsigned> getLatches(circuit::SeqCircuit<SEQNET>* circuit);

    unsigned mkNet(net::NetKind kind, unsigned x);
    unsigned mkNet(net::NetKind kind, unsigned x, unsigned y);
    unsigned mkNet(net::NetKind kind, unsigned z, unsigned x, unsigned y);

    // net::NetKind getNetKind(unsigned x) const;
    unsigned getDefaultInitFromType(net::NetTypeInfo t);
    void adjustNumberTypes(unsigned* x, unsigned* y);
    unsigned mkCastToInt8Type(unsigned x);
    unsigned mkCastToInt16Type(unsigned x);
    unsigned mkCastToInt32Type(unsigned x);
    unsigned mkCastToInt64Type(unsigned x);
    unsigned mkCastToUint8Type(unsigned x);
    unsigned mkCastToUint16Type(unsigned x);
    unsigned mkCastToUint32Type(unsigned x);
    unsigned mkCastToUint64Type(unsigned x);

    net::NetTypeInfo* mkBooleanType();
    net::NetTypeInfo* mkInt8Type();
    net::NetTypeInfo* mkInt16Type();
    net::NetTypeInfo* mkInt32Type();
    net::NetTypeInfo* mkInt64Type();
    net::NetTypeInfo* mkUint8Type();
    net::NetTypeInfo* mkUint16Type();
    net::NetTypeInfo* mkUint32Type();
    net::NetTypeInfo* mkUint64Type();
    net::NetTypeInfo* mkRealType();
    net::NetTypeInfo* mkIntType();
    net::NetTypeInfo* mkFloat16Type();
    net::NetTypeInfo* mkFloat32Type();
    net::NetTypeInfo* mkFloat64Type();
    net::NetTypeInfo* mkUserEnumType(const std::string& enumName);
    net::NetTypeInfo* declareUserEnumType(const std::string& enumName,
                                          const std::vector<std::string>& ev);
    net::NetTypeInfo getNetTypeInfo(unsigned x) const;
    net::NetTypeInfo getValueTypeInfo(unsigned x) const;

    SEQNET getNetFromUnsigned(unsigned x) const;
    COMNET getValueFromUnsigned(unsigned x) const;
    void getNetAsString(SEQNET n, std::string* output);
    void getValueAsString(COMNET n, std::string* output);

    void setTraceValue(engine::Trace<SEQNET, COMNET>* trace,
                       SEQNET net,
                       unsigned depth,
                       const char* value);

    unsigned storeAndReturn(COMNET n);

    void pushNamespace(const char* ns);
    void popNamespace();

 protected:
    std::string mangleName(const std::string& name) const;
    unsigned storeAndReturn(SEQNET n);
    void makeContextAwareOf(circuit::SeqCircuit<SEQNET>* circuit);

    template<class RelOpIntSigned, class RelOpIntUnsigned,
             class RelOpReal, class RelOpFloat>
    unsigned mkRelOpHelper(SEQNET x, SEQNET y,
                           RelOpIntSigned relOpIntSigned,
                           RelOpIntUnsigned relOpIntUnsigned,
                           RelOpReal relOpReal,
                           RelOpFloat relOpFloat);

    FACTORY factory_;

    std::unique_ptr<circuit::SeqCircuit<SEQNET>> circuit_;
    std::unordered_map<unsigned, SEQNET> unsigned2net_;
    std::unordered_map<unsigned, COMNET> unsigned2value_;
    std::unique_ptr<net::NetStore<SEQNET>> seqNetStore_;
    std::unique_ptr<net::NetStore<COMNET>> comNetStore_;
    std::unordered_map<std::string, net::NetTypeInfo> enumName2netTypeInfo_;
    std::vector<std::unique_ptr<engine::Bmc<SEQNET, COMNET>>> bmcEngines_;
    std::vector<std::unique_ptr<
                        engine::BackwardReach<SEQNET, COMNET>>> brEngines_;
    std::vector<std::unique_ptr<
                        engine::Simulator<SEQNET, COMNET>>> simulators_;
    std::vector<std::unique_ptr<
                        engine::Trace<SEQNET, COMNET>>> traces_;

    net::NetTypeInfo booleanType_;
    net::NetTypeInfo int8Type_;
    net::NetTypeInfo int16Type_;
    net::NetTypeInfo int32Type_;
    net::NetTypeInfo int64Type_;
    net::NetTypeInfo uint8Type_;
    net::NetTypeInfo uint16Type_;
    net::NetTypeInfo uint32Type_;
    net::NetTypeInfo uint64Type_;
    net::NetTypeInfo float16Type_;
    net::NetTypeInfo float32Type_;
    net::NetTypeInfo float64Type_;
    net::NetTypeInfo realType_;
    net::NetTypeInfo intType_;

    std::vector<std::string> namespaces_;

    static const std::string UNKNOWN_VALUE;
};

}  // namespace context

#endif  // SRC_CONTEXT_CONTEXT_H_
