/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_UNROLLER_CPP_
#define SRC_CIRCUIT_UNROLLER_CPP_

#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <string>

#include "src/net/NetType.h"
#include "src/net/NetKind.h"
#include "src/circuit/Unroller.h"
#include "src/net/NetStoreUtils.h"

namespace circuit {

template<class SeqNet, class ComNet>
void
Unroller<SeqNet, ComNet>::
setAdditionalNetsToWatch(const std::vector<SeqNet>& watch) {
    additionalNetsToWatch_.insert(additionalNetsToWatch_.end(), watch.begin(), watch.end());
}

template<class SeqNet, class ComNet>
void
Unroller<SeqNet, ComNet>::unroll(const unsigned depth,
                                 const Initialize init) {
    auto unrollOutputs = [this, init] (const unsigned depth) {
        for (auto seqOutput : seqCircuit_.getOutputs()) {
            ComNet comOutput = unrollRec(seqOutput,
                                         depth,
                                         init);
            comCircuit_->mkOutput(comOutput);
        }
    };

    auto unrollVector = [this, init] (const unsigned depth,
                                      const std::vector<SeqNet>& nets) {
        for (auto net : nets) {
            unrollRec(net, depth, init);
        }
    };

    // Watch latches, outputs, assumptions at every step
    for (unsigned d = lastUnrolledDepth_ + 1; d < depth; d++) {
        for (SeqNet latch : seqCircuit_.getLatches()) {
            unrollRec(latch, d, init);
        }
        unrollOutputs(d);
        unrollVector(d, seqCircuit_.getInputs());
        unrollVector(d, additionalNetsToWatch_);
    }

    for (unsigned d = 0; d < depth; d++) {
        unrollVector(d, seqCircuit_.getAssumptions());
    }

    // Watch outputs also at last depth
    unrollOutputs(depth);
    unrollVector(depth, seqCircuit_.getInputs());
    unrollVector(depth, seqCircuit_.getAssumptions());
    unrollVector(depth, additionalNetsToWatch_);

    lastUnrolledDepth_ = depth;
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::unrollRec(SeqNet seqNet,
                                    const unsigned depth,
                                    const Initialize init) {
    if (seqNetStore_.isFpaRounding(seqNet)) {
        return comNetStore_->mkFpaRounding();
    }

    net::TimedSeqNet<SeqNet> seqDepth = {seqNet, depth};

    if (unrollMap_.hasFwdMapping(seqDepth)) {
        // Previously translated net, return cached result
        return unrollMap_.getFwdMapping(seqDepth);
    }

    const unsigned nofChildren = seqNetStore_.getNofChildren(seqNet);
    ComNet result;

    switch (nofChildren) {
    case 0:
        result = mkComFromSeqZero(seqNet, depth, init);
        break;
    case 1:
        result = mkComFromSeqOne(seqNet, depth, init);
        break;
    case 2:
        result = mkComFromSeqTwo(seqNet, depth, init);
        break;
    case 3:
        result = mkComFromSeqThree(seqNet, depth, init);
        break;
    default:
        result = mkComFromSeqN(seqNet, depth, init);
    }

    assert(result != ComNet());

    unrollMap_.setMapping(seqDepth, result);
    return result;
}

template<class SeqNet, class ComNet>
bool
Unroller<SeqNet, ComNet>::hasNetAtTime(SeqNet n, unsigned t) const {
    net::TimedSeqNet<SeqNet> timedSeqNet = {n, t};
    return unrollMap_.hasFwdMapping(timedSeqNet);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::getNetAtTime(SeqNet n, unsigned t) const {
    net::TimedSeqNet<SeqNet> timedSeqNet = {n, t};
    assert(unrollMap_.hasFwdMapping(timedSeqNet));
    return unrollMap_.getFwdMapping(timedSeqNet);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::getComNumber(SeqNet n) {
    assert(seqNetStore_.isNumber(n));
    return mkComFromConstantOrNumber(n);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromLatch(SeqNet seqNet,
                                         const unsigned depth,
                                         const Initialize initStrat) {
    assert(seqCircuit_.isLatch(seqNet));
    if (depth == 0) {
        if (initStrat == Initialize::InlineInitialStates) {
            SeqNet init = seqCircuit_.getLatchInit(seqNet);
            if (init != SeqNet()) {
                return mkComFromSeqZero(init,
                                        depth,
                                        initStrat);
            }
        }
        const std::string name = seqNetStore_.toString(seqNet);
        const net::NetTypeInfo type = seqNetStore_.getNetTypeInfo(seqNet);
        const ComNet comNet = comNetStore_->mkVariable(
                    depthPrefixedName(depth, name), type);
        comCircuit_->mkInput(comNet);
        return comNet;
    }
    assert(depth > 0);
    // There is no recursion here, as previous depth is already done
    ComNet prev = unrollRec(seqCircuit_.getLatchNext(seqNet),
                            depth - 1,
                            initStrat);
    return prev;
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromConstantOrNumber(SeqNet seqNet) {
    return net::NetStoreUtils<SeqNet, ComNet>::
            translateConstantOrNumber(seqNet, seqNetStore_, comNetStore_);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromInput(SeqNet seqNet,
                                         const unsigned depth) {
    assert(seqCircuit_.isInput(seqNet));
    const net::NetTypeInfo type = seqNetStore_.getNetTypeInfo(seqNet);
    const std::string name = seqNetStore_.toString(seqNet);
    const ComNet comNet = comNetStore_->mkVariable(
                depthPrefixedName(depth, name), type);
    comCircuit_->mkInput(comNet);
    return comNet;
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromSeqZero(SeqNet seqNet,
                                           const unsigned depth,
                                           const Initialize initStrat) {
    ComNet result = ComNet();
    if (seqCircuit_.isLatch(seqNet)) {
        result = mkComFromLatch(seqNet, depth, initStrat);
    } else if (seqCircuit_.isInput(seqNet)) {
        result = mkComFromInput(seqNet, depth);
    } else {
        result = mkComFromConstantOrNumber(seqNet);
    }
    assert(result != ComNet());
    return result;
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromSeqOne(SeqNet seqNet,
                                          const unsigned depth,
                                          const Initialize initStrat) {
    // Recurse on child first
    ComNet child0 = unrollRec(seqNetStore_.getChild(seqNet, 0),
                              depth,
                              initStrat);
    auto kind = seqNetStore_.getNetKind(seqNet);
    if (std::get<0>(kind) == net::NetKind::Extract) {
        return comNetStore_->mkExtract(std::get<1>(kind), std::get<2>(kind), child0);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromSeqTwo(SeqNet seqNet,
                                          const unsigned depth,
                                          const Initialize initStrat) {
    // Recurse on children first
    ComNet child0 = unrollRec(seqNetStore_.getChild(seqNet, 0),
                              depth,
                              initStrat);
    // Recurse on children first
    ComNet child1 = unrollRec(seqNetStore_.getChild(seqNet, 1),
                              depth,
                              initStrat);
    auto kind = seqNetStore_.getNetKind(seqNet);
    if (comNetStore_->isFpaRounding(child0)) {
        return comNetStore_->mkNet(std::get<0>(kind), child1);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0, child1);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromSeqThree(SeqNet seqNet,
                                            const unsigned depth,
                                            const Initialize initStrat) {
    // Recurse on children first
    ComNet child0 = unrollRec(seqNetStore_.getChild(seqNet, 0),
                              depth,
                              initStrat);
    // Recurse on children first
    ComNet child1 = unrollRec(seqNetStore_.getChild(seqNet, 1),
                              depth,
                              initStrat);
    // Recurse on children first
    ComNet child2 = unrollRec(seqNetStore_.getChild(seqNet, 2),
                              depth,
                              initStrat);
    auto kind = seqNetStore_.getNetKind(seqNet);
    if (comNetStore_->isFpaRounding(child0)) {
        return comNetStore_->mkNet(std::get<0>(kind), child1, child2);
    }
    return comNetStore_->mkNet(std::get<0>(kind), child0, child1, child2);
}

template<class SeqNet, class ComNet>
ComNet
Unroller<SeqNet, ComNet>::mkComFromSeqN(SeqNet seqNet,
                                        const unsigned depth,
                                        const Initialize initStrat) {
    const unsigned nofChildren = seqNetStore_.getNofChildren(seqNet);
    assert(nofChildren > 3);
    std::vector<ComNet> unrolledChildren;
    for (unsigned child = 0;
         child < nofChildren;
         child++) {
        unrolledChildren.push_back(
                    unrollRec(
                        seqNetStore_.getChild(seqNet, child),
                                              depth,
                                              initStrat));
    }
    assert(nofChildren == unrolledChildren.size());
    auto kind = seqNetStore_.getNetKind(seqNet);
    ComNet result = comNetStore_->mkNet(std::get<0>(kind),
                                        unrolledChildren[0],
                                        unrolledChildren[1]);
    for (unsigned child = 2; child < nofChildren; child++) {
        result = comNetStore_->mkNet(std::get<0>(kind), result, unrolledChildren[child]);
    }
    return result;
}

template<class SeqNet, class ComNet>
std::string
Unroller<SeqNet, ComNet>::
depthPrefixedName(const unsigned depth, const std::string& name) {
    std::stringstream ss;
    ss << "@"
       << std::setfill('0')
       << std::setw(4)
       << std::to_string(depth) + "_" + name;
    return ss.str();
}

}  // namespace circuit

#endif  // SRC_CIRCUIT_UNROLLER_CPP_
