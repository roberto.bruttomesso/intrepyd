/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_UNROLLER_H_
#define SRC_CIRCUIT_UNROLLER_H_

#include <memory>
#include <vector>
#include <string>

#include "src/net/NetStore.h"
#include "src/circuit/SeqCircuit.h"
#include "src/circuit/Circuit.h"
#include "src/circuit/UnrollMap.h"
#include "src/circuit/UnrollerOptions.h"

namespace circuit {

template<class SeqNet, class ComNet>
class Unroller : public UnrollerOptions {
 public:
    Unroller(const net::NetStore<SeqNet>& seqNetStore,
             const SeqCircuit<SeqNet>& seqCircuit,
             net::NetStore<ComNet>* comNetStore,
             ComCircuit<ComNet>* comCircuit)
        : seqNetStore_(seqNetStore),
          seqCircuit_(seqCircuit),
          comNetStore_(comNetStore),
          comCircuit_(comCircuit),
          lastUnrolledDepth_(std::numeric_limits<unsigned>::max())
    {}

    virtual ~Unroller() {}

    void setAdditionalNetsToWatch(const std::vector<SeqNet>& watch);

    void unroll(const unsigned lastDepth,
                const Initialize init);

    ComNet getNetAtTime(SeqNet n, unsigned t) const;
    bool hasNetAtTime(SeqNet n, unsigned t) const;
    ComNet getComNumber(SeqNet n);

    static std::string depthPrefixedName(const unsigned depth,
                                         const std::string& name);

 protected:
    ComNet unrollRec(SeqNet seqNet,
                     const unsigned depth,
                     const Initialize init);

    ComNet mkComFromConstantOrNumber(SeqNet n);

    ComNet mkComFromInput(SeqNet n,
                          const unsigned depth);

    ComNet mkComFromSeqZero(SeqNet seqNet,
                            const unsigned depth,
                            const Initialize init);

    ComNet mkComFromSeqOne(SeqNet seqNet,
                           const unsigned depth,
                           const Initialize init);

    ComNet mkComFromSeqTwo(SeqNet seqNet,
                           const unsigned depth,
                           const Initialize init);

    ComNet mkComFromSeqThree(SeqNet seqNet,
                             const unsigned depth,
                             const Initialize init);

    ComNet mkComFromSeqN(SeqNet seqNet,
                         const unsigned depth,
                         const Initialize init);

    ComNet mkComFromLatch(SeqNet seqNet,
                          const unsigned depth,
                          const Initialize init);

    const net::NetStore<SeqNet>& seqNetStore_;
    const SeqCircuit<SeqNet>& seqCircuit_;
    net::NetStore<ComNet>* comNetStore_;
    ComCircuit<ComNet>* comCircuit_;
    UnrollMap<SeqNet, ComNet> unrollMap_;
    std::vector<SeqNet> additionalNetsToWatch_;
    unsigned lastUnrolledDepth_;
};

}  // namespace circuit

#include "src/circuit/Unroller.cpp"

#endif  // SRC_CIRCUIT_UNROLLER_H_
