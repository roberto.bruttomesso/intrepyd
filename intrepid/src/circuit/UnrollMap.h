/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_UNROLLMAP_H_
#define SRC_CIRCUIT_UNROLLMAP_H_

#include <vector>
#include <list>
#include <unordered_map>
#include <utility>

#include "src/net/TimedSeqNet.h"

#define MAP(SEQNET, COMNET) \
    std::unordered_map<SEQNET, std::vector<COMNET>>
#define MULTIMAP(SEQNET, COMNET) \
    std::unordered_multimap<COMNET, net::TimedSeqNet<SeqNet>>
#define RANGE(SEQNET, COMNET) \
    std::pair<typename MULTIMAP(SEQNET, COMNET)::const_iterator, \
              typename MULTIMAP(SEQNET, COMNET)::const_iterator>

namespace circuit {

template<class SeqNet, class ComNet>
class UnrollMap final {
 private:
    using Map = MAP(SeqNet, ComNet);
    using MultiMap = MULTIMAP(SeqNet, ComNet);
    using Range = RANGE(SeqNet, ComNet);

 public:
    void setMapping(const net::TimedSeqNet<SeqNet>& x, const ComNet& y);
    const ComNet& getFwdMapping(const net::TimedSeqNet<SeqNet>& x) const;
    Range getBwdMapping(const ComNet& y) const;
    bool hasFwdMapping(const net::TimedSeqNet<SeqNet>& x) const;
    bool hasBwdMapping(const ComNet& y) const;
    size_t size() const;
    void clear();

 private:
    Map seqToCom_;
    MultiMap comToSeq_;
};

}  // namespace circuit

#include "src/circuit/UnrollMap.cpp"

#endif  // SRC_CIRCUIT_UNROLLMAP_H_
