/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_UNROLLMAP_CPP_
#define SRC_CIRCUIT_UNROLLMAP_CPP_

#include <cassert>
#include <iostream>
#include <vector>

#include "src/circuit/UnrollMap.h"

namespace circuit {

template<class SeqNet, class ComNet>
void
UnrollMap<SeqNet, ComNet>::
setMapping(const net::TimedSeqNet<SeqNet>& x, const ComNet& y) {
    assert(!hasFwdMapping(x));
    std::vector<ComNet>& depthToCom = seqToCom_[x.getSeqNet()];
    if (depthToCom.size() <= x.getTime()) {
        depthToCom.resize(x.getTime() + 1, ComNet());
    }
    depthToCom[x.getTime()] = y;
    comToSeq_.insert({y, x});
    assert(hasFwdMapping(x));
    assert(hasBwdMapping(y));
}

template<class SeqNet, class ComNet>
const ComNet&
UnrollMap<SeqNet, ComNet>::
getFwdMapping(const net::TimedSeqNet<SeqNet>& x) const {
    assert(hasFwdMapping(x));
    return seqToCom_.at(x.getSeqNet())[x.getTime()];
}

template<class SeqNet, class ComNet>
typename UnrollMap<SeqNet, ComNet>::Range
UnrollMap<SeqNet, ComNet>::getBwdMapping(const ComNet& y) const {
    assert(hasBwdMapping(y));
    return comToSeq_.equal_range(y);
}

template<class SeqNet, class ComNet>
bool
UnrollMap<SeqNet, ComNet>::
hasFwdMapping(const net::TimedSeqNet<SeqNet>& x) const {
    auto it = seqToCom_.find(x.getSeqNet());
    if (it == seqToCom_.end()) {
        return false;
    }
    if (it->second.size() <= x.getTime()) {
        return false;
    }
    return (it->second)[x.getTime()] != ComNet();
}

template<class SeqNet, class ComNet>
bool
UnrollMap<SeqNet, ComNet>::hasBwdMapping(const ComNet& y) const {
    return comToSeq_.find(y) != comToSeq_.end();
}

template<class SeqNet, class ComNet>
size_t
UnrollMap<SeqNet, ComNet>::size() const {
    return comToSeq_.size();
}

template<class SeqNet, class ComNet>
void
UnrollMap<SeqNet, ComNet>::clear() {
    seqToCom_.clear();
    comToSeq_.clear();
}

}  // namespace circuit

#endif  // SRC_CIRCUIT_UNROLLMAP_CPP_
