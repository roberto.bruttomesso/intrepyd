/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_SEQCIRCUIT_CPP_
#define SRC_CIRCUIT_SEQCIRCUIT_CPP_

#include <vector>

#include "src/circuit/SeqCircuit.h"

namespace circuit {

template<class Net>
void
SeqCircuit<Net>::mkLatch(Net lout) {
    Latch latch(lout, Net(), Net());
    net2latch_.insert(std::make_pair(lout, latch));
    latches_.push_back(lout);
}

template<class Net>
void
SeqCircuit<Net>::setLatchInit(Net lout, Net init) {
    assert(net2latch_.find(lout) != net2latch_.end());
    assert(net2latch_.at(lout).init == Net());
    net2latch_.at(lout).init = init;
}

template<class Net>
void
SeqCircuit<Net>::setLatchNext(Net lout, Net next) {
    assert(net2latch_.find(lout) != net2latch_.end());
    assert(net2latch_.at(lout).next == Net());
    net2latch_.at(lout).next = next;
}

template<class Net>
void
SeqCircuit<Net>::setLatchInitNext(Net lout, Net init, Net next) {
    setLatchInit(lout, init);
    setLatchNext(lout, next);
}

template<class Net>
bool
SeqCircuit<Net>::isLatch(Net x) const {
    return net2latch_.find(x) != net2latch_.end();
}

template<class Net>
Net
SeqCircuit<Net>::getLatchInit(Net x) const {
    assert(isLatch(x));
    return net2latch_.at(x).init;
}

template<class Net>
Net
SeqCircuit<Net>::getLatchNext(Net x) const {
    assert(isLatch(x));
    return net2latch_.at(x).next;
}

template<class Net>
const std::vector<Net>&
SeqCircuit<Net>::getLatches() const {
    return latches_;
}

}  // namespace circuit

#endif  // SRC_CIRCUIT_SEQCIRCUIT_CPP_
