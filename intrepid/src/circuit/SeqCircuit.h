/* BSD 3-Clause License
 * 
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_CIRCUIT_SEQCIRCUIT_H_
#define SRC_CIRCUIT_SEQCIRCUIT_H_

#include <vector>
#include <memory>
#include <string>
#include <cassert>

#include "src/circuit/UnrollMap.h"
#include "src/circuit/SeqCircuit.h"
#include "src/circuit/ComCircuit.h"
#include "src/circuit/Circuit.h"
#include "src/net/NetType.h"

namespace circuit {

template<class Net>
class SeqCircuit : public Circuit<Net> {
 protected:
    explicit SeqCircuit(const std::string& name)
        : Circuit<Net>(name)
    {}

    struct Latch {
        Latch(Net l, Net i, Net n)
            : lout(l), init(i), next(n)
        {}
        Net lout;
        Net init;
        Net next;
    };

    struct LatchHash {
        size_t operator()(const Latch& l) const {
            return std::hash<Net>()(l.init)
                 + std::hash<Net>()(l.next);
        }
    };

    struct LatchEq {
        bool operator()(const Latch& x, const Latch& y) const {
            return x.init == y.init
                && x.next == y.next;
        }
    };

 public:
    void mkLatch(Net t);

    void setLatchNext(Net l, Net n);
    void setLatchInit(Net l, Net n);
    void setLatchInitNext(Net l, Net i, Net n);

    bool isLatch(Net x) const;

    Net getLatchInit(Net x) const;
    Net getLatchNext(Net x) const;
    const std::vector<Net>& getLatches() const;

 protected:
    std::unordered_map<Net, Latch> net2latch_;
    std::vector<Net> latches_;
};

}  // namespace circuit

#include "src/circuit/SeqCircuit.cpp"

#endif  // SRC_CIRCUIT_SEQCIRCUIT_H_
