%module api
%feature("autodoc", "3");

%{
#include "src/api/Intrepid.h"
%}

%exception {
    char* err;
    clear_exception();
    $action
    if ((err = check_exception())) {
        PyErr_SetString(PyExc_RuntimeError, err);
        return NULL;
    }
}

%include "src/api/Intrepid.h"
