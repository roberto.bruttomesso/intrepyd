#ifndef SRC_API_API_TRACER_H_
#define SRC_API_API_TRACER_H_

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <sstream>
#include <memory>
#include <vector>

#include "src/api/Intrepid.h"

namespace api {

class ApiTracer final {
public:
    ApiTracer();
    ~ApiTracer();

    ApiTracer(const ApiTracer&) = delete;
    ApiTracer operator=(const ApiTracer&) = delete;

    void beginApi(const std::string& name);
    void addArg(const Int_ctx& ctx);
    void addArg(const Int_trace& trace);
    void addArg(const Int_engine_bmc& bmc);
    void addArg(const Int_engine_br& br);
    void addArg(const Int_engine_ti& ti);
    void addArg(const Int_simulator& sim);
    void addArg(const Int_net& net);
    void addIntArg(unsigned value);
    void addStrArg(const std::string& str);
    void addArg(const Int_type& type);
    void addReturn(const Int_ctx& ctx);
    void addReturn(const Int_trace& trace);
    void addReturn(const Int_engine_bmc& bmc);
    void addReturn(const Int_engine_br& br);
    void addReturn(const Int_engine_ti& ti);
    void addReturn(const Int_simulator& sim);
    void addReturn(const Int_net& net);
    void addReturn(const Int_type& type);
    void endApi();

    void reset();

    void dumpToFile(const char* filename);
    void printToStdout();
    void printToStderr();

private:
    std::string net2name(const Int_net& net) const;

    std::unordered_map<Int_ctx, std::string> ctx2name_;
    std::unordered_map<Int_trace, std::string> trace2name_;
    std::unordered_map<Int_engine_bmc, std::string> bmc2name_;
    std::unordered_map<Int_engine_br, std::string> br2name_;
    std::unordered_map<Int_engine_ti, std::string> ti2name_;
    std::unordered_map<Int_simulator, std::string> sim2name_;
    std::unordered_map<Int_type, std::string> type2name_;
    std::unordered_set<Int_net> nets_;

    std::vector<std::string> args_;

    std::string currentType_;
    std::string currentReturn_;
    std::string currentName_;
    bool inApi_;
    bool skip_;

    std::ostringstream outStream_;

    static const std::string DEFAULT_FILENAME;
};

}  // namespace api

#endif  // SRC_API_API_TRACER_H_
