/* BSD 3-Clause License
 *
 * Copyright (c) 2016-2019, Roberto Bruttomesso
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <cstring>
#include <exception>
#include <regex>

#include "src/context/Context.h"
#include "src/api/Intrepid.h"
#include "src/api/ApiTracer.h"
#include "src/exception/IntrepidException.h"

#define TOVOID(X) static_cast<void*>(X)
#define TOCONTEXT(X) static_cast<context::Context*>(TOVOID(X))
#define TONETTYPEINFO(X) static_cast<net::NetTypeInfo*>(TOVOID(X))
#define TOBMC(X) static_cast<engine::Bmc<SEQNET, COMNET>*>(TOVOID(X))
#define TOBR(X) static_cast<engine::BackwardReach<SEQNET, COMNET>*>(TOVOID(X))
#define TOSIMULATOR(X) static_cast<engine::Simulator<SEQNET, COMNET>*>(TOVOID(X))
#define TOTRACE(X) \
            static_cast<engine::Trace<SEQNET, COMNET>*>(TOVOID(X))
#define TOINTCTX(X) static_cast<Int_ctx>(TOVOID(X))
#define TOINTTYPE(X) static_cast<Int_type>(TOVOID(X))
#define TOINTBMC(X) static_cast<Int_engine_bmc>(TOVOID(X))
#define TOINTBR(X) static_cast<Int_engine_br>(TOVOID(X))
#define TOINTSIMULATOR(X) static_cast<Int_simulator>(TOVOID(X))
#define TOINTTRACE(X) static_cast<Int_trace>(TOVOID(X))
#define TOINT(X) X ? 1 : 0

#define CATCH_EXCEPTIONS
#ifdef CATCH_EXCEPTIONS
#define TRY   try {
#define CATCH } catch (const exception::IntrepidException& e) {       \
                  throw_exception(e.getError());                      \
              } catch (const std::exception& e) {                     \
                  throw_exception(e.what());                          \
              }
#else
#define TRY
#define CATCH
#endif

#ifdef __cplusplus
extern "C" {
#endif

namespace {
    std::string valueStr;
    api::ApiTracer apiTracer;
}

static Int_engine_result engineResult2IntEngineResult(
        engine::EngineResult result) {
    switch (result) {
    case engine::EngineResult::Reachable:
        return INT_ENGINE_RESULT_REACHABLE;
    case engine::EngineResult::Unreachable:
        return INT_ENGINE_RESULT_UNREACHABLE;
    case engine::EngineResult::Unknown:
        return INT_ENGINE_RESULT_UNKNOWN;
    default:
        assert(false);
    }
    return INT_ENGINE_RESULT_UNKNOWN;
}

Int_ctx mk_ctx() {
    TRY
    void* ptr = new context::Context;
    Int_ctx result = TOINTCTX(ptr);
    apiTracer.reset();
    apiTracer.beginApi("mk_ctx");
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

void del_ctx(Int_ctx ctx) {
    TRY
    apiTracer.beginApi("del_ctx");
    apiTracer.addArg(ctx);
    apiTracer.endApi();
    void* ptr = ctx;
    delete TOCONTEXT(ptr);
    CATCH
}

constexpr int ERROR_MESSAGE_BUFFER_SIZE = 256;
static char error_message[ERROR_MESSAGE_BUFFER_SIZE];
static int error_status = 0;

void throw_exception(const char* msg) {
#ifdef _WIN32
#   define STRNCPY strncpy_s
#else
#   define STRNCPY strncpy
#endif
    STRNCPY(error_message, msg, ERROR_MESSAGE_BUFFER_SIZE - 1);
    error_status = 1;
}

void clear_exception() {
    error_status = 0;
}
char* check_exception() {
    if (error_status) {
        apiTracer.dumpToFile("trace.cpp");
        return error_message;
    }
    return NULL;
}

Int_engine_bmc mk_engine_bmc(Int_ctx ctx) {
    TRY
    if (ctx == nullptr) {
        throw_exception("Received NULL context");
        return nullptr;
    }
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    engine::Bmc<SEQNET, COMNET>* bmc =
        context->mkEngineBmc(circuit);
    Int_engine_bmc result = TOINTBMC(bmc);
    apiTracer.beginApi("mk_engine_bmc");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

void set_bmc_current_depth(Int_engine_bmc engine, unsigned depth) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->setCurrentDepth(depth);
    apiTracer.beginApi("set_bmc_current_depth");
    apiTracer.addArg(engine);
    apiTracer.addIntArg(depth);
    apiTracer.endApi();
    CATCH
}

void set_bmc_optimize(Int_engine_bmc engine) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->setOptimize(true);
    apiTracer.beginApi("set_bmc_optimize");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    CATCH
}

void set_bmc_allow_targets_at_any_depth(Int_engine_bmc engine) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->setAllowTargetsOnlyAtLastDepth(false);
    apiTracer.beginApi("set_bmc_allow_targets_at_any_depth");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    CATCH
}

void set_bmc_use_induction(Int_engine_bmc engine) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->setInduction(true);
    apiTracer.beginApi("set_bmc_use_induction");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    CATCH
}

void set_bmc_use_attack_path_axioms(Int_ctx ctx,
                                    Int_engine_bmc engine,
                                    Int_net source,
                                    Int_net target) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    context::Context* context = TOCONTEXT(ctx);
    bmc->setUseAttackPathAxioms(context->getNetFromUnsigned(source),
                                context->getNetFromUnsigned(target));
    apiTracer.beginApi("set_bmc_use_attack_path_axioms");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(source);
    apiTracer.addArg(target);
    apiTracer.endApi();
    CATCH
}

void bmc_add_target(Int_ctx ctx, Int_engine_bmc engine, Int_net target) {
    TRY
    apiTracer.beginApi("bmc_add_target");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(target);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    if (context->getNetTypeInfo(target) != *context->mkBooleanType()) {
        throw_exception("Expecting target of type Boolean in bmc_add_target");
        return;
    }
    bmc->addTarget(context->getNetFromUnsigned(target));
    CATCH
}

void bmc_add_watch(Int_ctx ctx, Int_engine_bmc engine, Int_net watch) {
    TRY
    apiTracer.beginApi("bmc_add_watch");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(watch);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->addWatchedNet(context->getNetFromUnsigned(watch));
    CATCH
}

Int_engine_result bmc_reach_targets(Int_engine_bmc engine) {
    TRY
    apiTracer.beginApi("bmc_reach_targets");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->prepareForSolving();
    engine::EngineResult result = bmc->findFirstReachableTarget();
    return engineResult2IntEngineResult(result);
    CATCH
    return INT_ENGINE_RESULT_UNKNOWN;
}

void bmc_remove_last_reached_targets(Int_engine_bmc engine) {
    TRY
    apiTracer.beginApi("bmc_remove_last_reached_targets");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    bmc->removeLastReachedTargets();
    CATCH
}

unsigned bmc_last_reached_targets_number(Int_engine_bmc engine) {
    TRY
    apiTracer.beginApi("bmc_last_reached_targets_number");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    return static_cast<unsigned>(bmc->getLastReachedTargets().size());
    CATCH
    return 0;
}

Int_net bmc_last_reached_target(Int_engine_bmc engine, unsigned n) {
    TRY
    apiTracer.beginApi("bmc_last_reached_target");
    apiTracer.addArg(engine);
    apiTracer.addIntArg(n);
    apiTracer.endApi();
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    if (n >= bmc->getLastReachedTargets().size()) {
        throw_exception("Target number out of range");
        return 0;
    }
    return bmc->getLastReachedTargets().at(n);
    CATCH
    return 0;
}

Int_trace bmc_get_trace(Int_ctx ctx, Int_engine_bmc engine, Int_net target) {
    TRY
    engine::Bmc<SEQNET, COMNET>* bmc = TOBMC(engine);
    context::Context* context = TOCONTEXT(ctx);
    SEQNET net = context->getNetFromUnsigned(target);
    Int_trace trace = TOINTTRACE(bmc->getTracePtrForTarget(net).get());
    apiTracer.beginApi("bmc_get_trace");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(target);
    apiTracer.addReturn(trace);
    apiTracer.endApi();
    return trace;
    CATCH
    return nullptr;
}

void br_remove_last_reached_targets(Int_engine_br engine) {
    TRY
    apiTracer.beginApi("br_remove_last_reached_targets");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    br->removeLastReachedTargets();
    CATCH
}

Int_engine_br mk_engine_br(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    engine::BackwardReach<SEQNET, COMNET>* br
        = context->mkEngineBackwardReach(circuit);
    Int_engine_br result = TOINTBR(br);
    apiTracer.beginApi("mk_engine_br");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

void br_add_target(Int_ctx ctx, Int_engine_br engine, Int_net target) {
    TRY
    apiTracer.beginApi("br_add_target");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(target);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    if (context->getNetTypeInfo(target) != *context->mkBooleanType()) {
        throw_exception("Expecting target of type Boolean in br_add_target");
        return;
    }
    br->addTarget(context->getNetFromUnsigned(target));
    CATCH
}

void br_add_watch(Int_ctx ctx, Int_engine_br engine, Int_net watch) {
    TRY
    apiTracer.beginApi("br_add_watch");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(watch);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    br->addWatchedNet(context->getNetFromUnsigned(watch));
    CATCH
}

Int_engine_result br_reach_targets(Int_engine_br engine) {
    TRY
    apiTracer.beginApi("br_reach_targets");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    br->prepareForSolving();
    engine::EngineResult result = br->findFirstReachableTarget();
    return engineResult2IntEngineResult(result);
    CATCH
    return INT_ENGINE_RESULT_UNKNOWN;
}

unsigned br_last_reached_targets_number(Int_engine_br engine) {
    TRY
    apiTracer.beginApi("br_last_reached_targets_number");
    apiTracer.addArg(engine);
    apiTracer.endApi();
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    return static_cast<unsigned>(br->getLastReachedTargets().size());
    CATCH
    return 0;
}

Int_net br_last_reached_target(Int_engine_br engine, unsigned n) {
    TRY
    apiTracer.beginApi("br_last_reached_target");
    apiTracer.addArg(engine);
    apiTracer.addIntArg(n);
    apiTracer.endApi();
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    if (n >= br->getLastReachedTargets().size()) {
        throw_exception("Target number out of range");
        return 0;
    }
    return br->getLastReachedTargets().at(n);
    CATCH
    return 0;
}

Int_trace br_get_trace(Int_ctx ctx, Int_engine_br engine, Int_net target) {
    TRY
    engine::BackwardReach<SEQNET, COMNET>* br = TOBR(engine);
    context::Context* context = TOCONTEXT(ctx);
    SEQNET net = context->getNetFromUnsigned(target);
    Int_trace trace = TOINTTRACE(br->getTracePtrForTarget(net).get());
    apiTracer.beginApi("br_get_trace");
    apiTracer.addArg(ctx);
    apiTracer.addArg(engine);
    apiTracer.addArg(target);
    apiTracer.addReturn(trace);
    apiTracer.endApi();
    return trace;
    CATCH
    return nullptr;
}

unsigned trace_get_max_depth(Int_trace trace) {
    TRY
    apiTracer.beginApi("trace_get_max_depth");
    apiTracer.addArg(trace);
    apiTracer.endApi();
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    return tr->getMaxDepth();
    CATCH
    return 0;
}

unsigned trace_prepare_value_for_net(Int_ctx ctx,
                                     Int_trace trace,
                                     Int_net net,
                                     unsigned depth) {
    TRY
    apiTracer.beginApi("trace_prepare_value_for_net");
    apiTracer.addArg(ctx);
    apiTracer.addArg(trace);
    apiTracer.addArg(net);
    apiTracer.addIntArg(depth);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    if (depth > tr->getMaxDepth()) {
        throw_exception("Depth out of range");
        return 0;
    }
    SEQNET seqNet = context->getNetFromUnsigned(net);
    COMNET value = tr->getData(seqNet, depth);
    context->storeAndReturn(value);
    std::string valueStrResult;
    context->getValueAsString(value, &valueStrResult);
    if (valueStrResult.empty()) {
        throw_exception("Could not retrieve value from trace");
        return 0;
    }
    if (valueStrResult[0] == '#') {
        if (valueStrResult.size() < 3) {
            throw_exception("Unexpected value from trace");
            return 0;
        }
        if (valueStrResult[1] != 'x') {
            throw_exception("Unexpected value from trace");
            return 0;
        }
        net::NetTypeInfo nti = context->getValueTypeInfo(value);
        net::NetType nt = nti.getNetType();
        const bool sign = nt == net::NetType::Int;
        const auto size = nti.getSize();
        // Turn into 0x prefix
        valueStrResult[0] = '0';
        unsigned v = std::stoul(valueStrResult, nullptr, 16);
        if (sign) {
            if (size == 8 && v > 127) {
                // Sign extend
                v |= 0xFFFFFF00;
            }
            else if (size == 16 && v > 32768) {
                // Sign extend
                v |= 0xFFFF0000;
            }
            v = ~v;
            v++;
            valueStr = std::to_string(-static_cast<int>(v));
        }
        else {
            valueStr = std::to_string(v);
        }
    }
    else if (valueStrResult == "true") {
        valueStr = 'T';
    }
    else if (valueStrResult == "false") {
        valueStr = 'F';
    }
    else {
        std::smatch matches;
        const std::regex ratPattern1("\\(\\/ ([0-9]+\\.?[0-9]*) ([0-9]+\\.?[0-9]*)\\)");
        const std::regex ratPattern2("\\(\\- \\(\\/ ([0-9]+\\.?[0-9]*) ([0-9]+\\.?[0-9]*)\\)\\)");
        const std::regex intPattern1("\\(\\- ([0-9]+)\\)");
        if (std::regex_match(valueStrResult, matches, ratPattern1)) {
            // a rational value
            assert(matches.size() == 3);
            const double num = std::stod(matches[1].str().c_str());
            const double den = std::stod(matches[2].str().c_str());
            valueStr = std::to_string(num / den);
        }
        else if (std::regex_match(valueStrResult, matches, ratPattern2)) {
            // a negative rational value
            assert(matches.size() == 3);
            const double num = std::stod(matches[1].str().c_str());
            const double den = std::stod(matches[2].str().c_str());
            valueStr = std::to_string(-num / den);
        }
        else if (valueStrResult.find("to_real_unspecified") != std::string::npos) {
            // a default value
            valueStr = "0.0";
        }
        else if (std::regex_match(valueStrResult, matches, intPattern1)) {
            // a negative integer value
            assert(matches.size() == 2);
            const auto num = std::stol(matches[1].str().c_str());
            valueStr = std::to_string(-num);
        }
        else {
            // It's a printable value
            valueStr = valueStrResult;
        }
    }
    return static_cast<unsigned>(valueStr.size());
    CATCH
    return 0;
}

unsigned prepare_value_for_net(Int_ctx ctx, Int_net net) {
    TRY
    apiTracer.beginApi("prepare_value_for_net");
    apiTracer.addArg(ctx);
    apiTracer.addArg(net);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    SEQNET seqNet = context->getNetFromUnsigned(net);
    context->getNetAsString(seqNet, &valueStr);
    return static_cast<unsigned>(valueStr.size());
    CATCH
    return 0;
}

char value_at(unsigned i) {
    apiTracer.beginApi("value_at");
    apiTracer.addIntArg(i);
    apiTracer.endApi();
    assert(i < valueStr.size());
    return valueStr[i];
}

Int_trace mk_trace(Int_ctx ctx) {
    TRY
    if (ctx == nullptr) {
        throw_exception("Received NULL context");
        return nullptr;
    }
    context::Context* context = TOCONTEXT(ctx);
    engine::Trace<SEQNET, COMNET>* trace = context->mkTrace();
    Int_trace result = TOINTTRACE(trace);
    apiTracer.beginApi("mk_trace");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

void trace_set_value(Int_ctx ctx, Int_trace trace, Int_net net,
                     unsigned depth, const char* value) {
    TRY
    apiTracer.beginApi("trace_set_value");
    apiTracer.addArg(ctx);
    apiTracer.addArg(trace);
    apiTracer.addArg(net);
    apiTracer.addIntArg(depth);
    apiTracer.addStrArg(value);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    SEQNET n = context->getNetFromUnsigned(net);
    context->setTraceValue(tr, n, depth, value);
    CATCH
}

unsigned trace_get_watched_nets_number(Int_trace trace) {
    TRY
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    apiTracer.beginApi("trace_get_watched_nets_number");
    apiTracer.addArg(trace);
    apiTracer.endApi();
    return tr->getNetsNumber();
    CATCH
    return 0;
}

Int_net trace_get_watched_net(Int_trace trace, unsigned i) {
    TRY
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    const Int_net result = tr->getNet(i);
    apiTracer.beginApi("trace_get_watched_net");
    apiTracer.addArg(trace);
    apiTracer.addArg(i);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_type mk_boolean_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkBooleanType();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_boolean_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_int8_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkInt8Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_int8_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_int16_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkInt16Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_int16_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_int32_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkInt32Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_int32_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_int64_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkInt64Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_int64_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_uint8_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkUint8Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_uint8_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_uint16_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkUint16Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_uint16_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_uint32_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkUint32Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_uint32_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_uint64_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkUint64Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_uint64_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_int_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkIntType();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_int_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_real_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkRealType();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_real_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_float16_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkFloat16Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_float16_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_float32_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkFloat32Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_float32_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_type mk_float64_type(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* type = context->mkFloat64Type();
    Int_type result = TOINTTYPE(type);
    apiTracer.beginApi("mk_float64_type");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

Int_net mk_undef(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkUndef();
    apiTracer.beginApi("mk_undef");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_true(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkTrue();
    apiTracer.beginApi("mk_true");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_false(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkFalse();
    apiTracer.beginApi("mk_false");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_number(Int_ctx ctx, const char* value, Int_type type) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* netTypeInfo = TONETTYPEINFO(type);
    Int_net result = context->mkNumber(value, netTypeInfo);
    apiTracer.beginApi("mk_number");
    apiTracer.addArg(ctx);
    apiTracer.addStrArg(value);
    apiTracer.addArg(type);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_not(Int_ctx ctx, Int_net x) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkNot(x);
    apiTracer.beginApi("mk_not");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_and(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkAnd(x, y);
    apiTracer.beginApi("mk_and");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_or(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkOr(x, y);
    apiTracer.beginApi("mk_or");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_xor(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkXor(x, y);
    apiTracer.beginApi("mk_xor");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_ite(Int_ctx ctx, Int_net i, Int_net t, Int_net e) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkIte(i, t, e);
    apiTracer.beginApi("mk_ite");
    apiTracer.addArg(ctx);
    apiTracer.addArg(i);
    apiTracer.addArg(t);
    apiTracer.addArg(e);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_iff(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkIff(x, y);
    apiTracer.beginApi("mk_iff");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_eq(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkEq(x, y);
    apiTracer.beginApi("mk_eq");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_leq(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkLeq(x, y);
    apiTracer.beginApi("mk_leq");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_lt(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkLt(x, y);
    apiTracer.beginApi("mk_lt");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_geq(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkGeq(x, y);
    apiTracer.beginApi("mk_geq");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_gt(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkGt(x, y);
    apiTracer.beginApi("mk_gt");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_neq(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkNot(context->mkEq(x, y));
    apiTracer.beginApi("mk_neq");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_minus(Int_ctx ctx, Int_net x) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkMinus(x);
    apiTracer.beginApi("mk_minus");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_add(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkAdd(x, y);
    apiTracer.beginApi("mk_add");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_sub(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkSub(x, y);
    apiTracer.beginApi("mk_sub");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_mul(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkMul(x, y);
    apiTracer.beginApi("mk_mul");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_div(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkDiv(x, y);
    apiTracer.beginApi("mk_div");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_mod(Int_ctx ctx, Int_net x, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkMod(x, y);
    apiTracer.beginApi("mk_mod");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addArg(y);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net get_bit(Int_ctx ctx, Int_net x, unsigned bit) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkGetBit(x, bit);
    apiTracer.beginApi("get_bit");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addIntArg(bit);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net set_bit(Int_ctx ctx, Int_net x, unsigned bit, Int_net y) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkSetBit(x, bit, y);
    apiTracer.beginApi("set_bit");
    apiTracer.addArg(ctx);
    apiTracer.addArg(x);
    apiTracer.addIntArg(bit);
    apiTracer.addArg(y);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_input(Int_ctx ctx, const char* name, Int_type type) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* netTypeInfo = TONETTYPEINFO(type);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    Int_net result = context->mkInput(circuit, name, netTypeInfo);
    apiTracer.beginApi("mk_input");
    apiTracer.addArg(ctx);
    apiTracer.addStrArg(name);
    apiTracer.addArg(type);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

void mk_output(Int_ctx ctx, Int_net net) {
    TRY
    apiTracer.beginApi("mk_output");
    apiTracer.addArg(ctx);
    apiTracer.addArg(net);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    context->mkOutput(circuit, net);
    CATCH
}

// Deprecated, will be removed
void mk_assumption(Int_ctx ctx, Int_net net) {
    TRY
    apiTracer.beginApi("mk_assumption");
    apiTracer.addArg(ctx);
    apiTracer.addArg(net);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    context->pushAssumption(circuit, net);
    CATCH
}

void push_assumption(Int_ctx ctx, Int_net net) {
    TRY
    apiTracer.beginApi("push_assumption");
    apiTracer.addArg(ctx);
    apiTracer.addArg(net);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    context->pushAssumption(circuit, net);
    CATCH
}

void pop_assumption(Int_ctx ctx) {
    TRY
    apiTracer.beginApi("pop_assumption");
    apiTracer.addArg(ctx);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    context->popAssumption(circuit);
    CATCH
}

Int_net mk_latch(Int_ctx ctx, const char* name, Int_type type) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    net::NetTypeInfo* netTypeInfo = TONETTYPEINFO(type);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    Int_net result = context->mkLatch(circuit, name, netTypeInfo);
    apiTracer.beginApi("mk_latch");
    apiTracer.addArg(ctx);
    apiTracer.addStrArg(name);
    apiTracer.addArg(type);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

void set_latch_init_next(Int_ctx ctx, Int_net latch,
                         Int_net init, Int_net next) {
    TRY
    apiTracer.beginApi("set_latch_init_next");
    apiTracer.addArg(ctx);
    apiTracer.addArg(latch);
    apiTracer.addArg(init);
    apiTracer.addArg(next);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    if (!context->isLatch(circuit, latch)) {
        throw_exception("Given net to set_latch_init_next is not a latch");
        return;
    }
    context->setLatchInitNext(circuit, latch, init, next);
    CATCH
}

Int_simulator mk_simulator(Int_ctx ctx) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    circuit::SeqCircuit<SEQNET>* circuit = context->getCircuit();
    engine::Simulator<SEQNET, COMNET>* simulator =
        context->mkSimulator(circuit);
    Int_simulator result = TOINTSIMULATOR(simulator);
    apiTracer.beginApi("mk_simulator");
    apiTracer.addArg(ctx);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return nullptr;
}

void simulator_add_watch(Int_ctx ctx,
                         Int_simulator simulator,
                         Int_net watch) {
    TRY
    apiTracer.beginApi("simulator_add_watch");
    apiTracer.addArg(ctx);
    apiTracer.addArg(simulator);
    apiTracer.addArg(watch);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    engine::Simulator<SEQNET, COMNET>* sim = TOSIMULATOR(simulator);
    sim->addWatchedNet(context->getNetFromUnsigned(watch));
    CATCH
}

void simulator_simulate(Int_simulator simulator,
                        Int_trace trace,
                        unsigned depth) {
    TRY
    apiTracer.beginApi("simulator_simulate");
    apiTracer.addArg(simulator);
    apiTracer.addArg(trace);
    apiTracer.addIntArg(depth);
    apiTracer.endApi();
    engine::Simulator<SEQNET, COMNET>* sim = TOSIMULATOR(simulator);
    engine::Trace<SEQNET, COMNET>* tr = TOTRACE(trace);
    sim->extendTrace(tr, depth);
    CATCH
}

void push_namespace(Int_ctx ctx, const char* ns) {
    TRY
    apiTracer.beginApi("push_namespace");
    apiTracer.addArg(ctx);
    apiTracer.addStrArg(ns);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    context->pushNamespace(ns);
    CATCH
}

void pop_namespace(Int_ctx ctx) {
    TRY
    apiTracer.beginApi("pop_namespace");
    apiTracer.addArg(ctx);
    apiTracer.endApi();
    context::Context* context = TOCONTEXT(ctx);
    context->popNamespace();
    CATCH
}

Int_net mk_substitute(Int_ctx ctx, Int_net term,
                      Int_net new_subterm,
                      Int_net old_subterm) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkSubstitute(term, new_subterm, old_subterm);
    apiTracer.beginApi("mk_substitute");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addArg(new_subterm);
    apiTracer.addArg(old_subterm);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_int8(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToInt8Type(term);
    apiTracer.beginApi("mk_cast_to_int8");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_int16(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToInt16Type(term);
    apiTracer.beginApi("mk_cast_to_int16");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_int32(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToInt32Type(term);
    apiTracer.beginApi("mk_cast_to_int32");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_int64(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToInt64Type(term);
    apiTracer.beginApi("mk_cast_to_int64");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_uint8(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToUint8Type(term);
    apiTracer.beginApi("mk_cast_to_uint8");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_uint16(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToUint16Type(term);
    apiTracer.beginApi("mk_cast_to_uint16");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_uint32(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToUint32Type(term);
    apiTracer.beginApi("mk_cast_to_uint32");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

Int_net mk_cast_to_uint64(Int_ctx ctx, Int_net term) {
    TRY
    context::Context* context = TOCONTEXT(ctx);
    Int_net result = context->mkCastToUint64Type(term);
    apiTracer.beginApi("mk_cast_to_uint64");
    apiTracer.addArg(ctx);
    apiTracer.addArg(term);
    apiTracer.addReturn(result);
    apiTracer.endApi();
    return result;
    CATCH
    return 0;
}

void apitrace_dump_to_file(const char* filename) {
    TRY
    apiTracer.dumpToFile(filename);
    CATCH
}

void apitrace_print_to_stdout() {
    apiTracer.printToStdout();
}

void apitrace_print_to_stderr() {
    apiTracer.printToStderr();
}

#ifdef __cplusplus
}  // extern "C"
#endif
