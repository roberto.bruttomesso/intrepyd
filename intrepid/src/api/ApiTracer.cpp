#include <cassert>
#include <fstream>
#include <iostream>

#include "src/exception/IntrepidException.h"
#include "src/api/ApiTracer.h"

namespace api {

const std::string ApiTracer::DEFAULT_FILENAME = "trace.cpp";

ApiTracer::ApiTracer()
    : inApi_(false) {
}

ApiTracer::~ApiTracer() {
}

void
ApiTracer::
dumpToFile(const char* filename) {
    std::ofstream outFile(filename);
    if (!outFile.good()) {
        throw exception::IntrepidException("Could not open " +
                                           std::string(filename),
                                           __FILE__,
                                           __LINE__);
    }
    outFile << outStream_.str();
    outFile.close();
}

void
ApiTracer::
printToStdout() {
    std::cout << outStream_.str();
}

void
ApiTracer::
printToStderr() {
    std::cerr << outStream_.str();
}

void
ApiTracer::
reset() {
    trace2name_.clear();
    bmc2name_.clear();
    br2name_.clear();
    ti2name_.clear();
    sim2name_.clear();
    type2name_.clear();
    nets_.clear();
}

std::string
ApiTracer::
net2name(const Int_net& net) const {
    return "net" + std::to_string(net);
}

void
ApiTracer::
beginApi(const std::string& name) {
    assert(!inApi_);
    assert(args_.empty());
    inApi_ = true;
    currentType_ = "void";
    currentName_ = name;
    skip_ = false;
}

void
ApiTracer::
addArg(const Int_ctx& ctx) {
    assert(inApi_);
    auto it = ctx2name_.find(ctx);
    assert(it != ctx2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_trace& trace) {
    assert(inApi_);
    auto it = trace2name_.find(trace);
    assert(it != trace2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_engine_bmc& bmc) {
    assert(inApi_);
    auto it = bmc2name_.find(bmc);
    assert(it != bmc2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_engine_br& br) {
    assert(inApi_);
    auto it = br2name_.find(br);
    assert(it != br2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_engine_ti& ti) {
    assert(inApi_);
    auto it = ti2name_.find(ti);
    assert(it != ti2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_simulator& sim) {
    assert(inApi_);
    auto it = sim2name_.find(sim);
    assert(it != sim2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addArg(const Int_net& net) {
    assert(inApi_);
    args_.push_back(net2name(net));
}

void
ApiTracer::
addIntArg(unsigned value) {
    assert(inApi_);
    args_.push_back(std::to_string(value));
}

void
ApiTracer::
addStrArg(const std::string& str) {
    assert(inApi_);
    args_.push_back("\"" + str + "\"");
}

void
ApiTracer::
addArg(const Int_type& type) {
    assert(inApi_);
    auto it = type2name_.find(type);
    assert(it != type2name_.end());
    args_.push_back(it->second);
}

void
ApiTracer::
addReturn(const Int_ctx& ctx) {
    assert(inApi_);
    const std::string newName = "ctx" + std::to_string(ctx2name_.size());
    ctx2name_.insert(std::make_pair(ctx, newName));
    currentReturn_ = newName;
    currentType_ = "Int_ctx";
}

void
ApiTracer::
addReturn(const Int_trace& trace) {
    assert(inApi_);
    auto it = trace2name_.find(trace);
    currentType_ = "Int_trace";
    if (it != trace2name_.end()) {
        skip_ = true;
        return;
    }
    const std::string newName = "trace" + std::to_string(trace2name_.size());
    trace2name_.insert(std::make_pair(trace, newName));
    currentReturn_ = newName;
}

void
ApiTracer::
addReturn(const Int_engine_bmc& bmc) {
    assert(inApi_);
#ifndef NDEBUG
    auto it = bmc2name_.find(bmc);
#endif
    assert(it == bmc2name_.end());
    const std::string newName = "bmc" + std::to_string(bmc2name_.size());
    bmc2name_.insert(std::make_pair(bmc, newName));
    currentReturn_ = newName;
    currentType_ = "Int_engine_bmc";
}

void
ApiTracer::
addReturn(const Int_engine_br& br) {
    assert(inApi_);
#ifndef NDEBUG
    auto it = br2name_.find(br);
#endif
    assert(it == br2name_.end());
    const std::string newName = "br" + std::to_string(br2name_.size());
    br2name_.insert(std::make_pair(br, newName));
    currentReturn_ = newName;
    currentType_ = "Int_engine_br";
}

void
ApiTracer::
addReturn(const Int_engine_ti& ti) {
    assert(inApi_);
#ifndef NDEBUG
    auto it = ti2name_.find(ti);
#endif
    assert(it == ti2name_.end());
    const std::string newName = "ti" + std::to_string(ti2name_.size());
    ti2name_.insert(std::make_pair(ti, newName));
    currentReturn_ = newName;
    currentType_ = "Int_engine_ti";
}

void
ApiTracer::
addReturn(const Int_simulator& sim) {
    assert(inApi_);
    auto it = sim2name_.find(sim);
    currentType_ = "Int_simulator";
    if (it != sim2name_.end()) {
        skip_ = true;
        return;
    }
    const std::string newName = "sim" + std::to_string(sim2name_.size());
    sim2name_.insert(std::make_pair(sim, newName));
    currentReturn_ = newName;
}

void
ApiTracer::
addReturn(const Int_net& net) {
    assert(inApi_);
    if (!nets_.insert(net).second) {
        skip_ = true;
        return;
    }
    currentReturn_ = net2name(net);
    currentType_ = "Int_net";
}

void
ApiTracer::
addReturn(const Int_type& type) {
    assert(inApi_);
    auto it = type2name_.find(type);
    currentType_ = "Int_type";
    if (it != type2name_.end()) {
        skip_ = true;
        return;
    }
    const std::string newName = "type" + std::to_string(type2name_.size());
    type2name_.insert(std::make_pair(type, newName));
    currentReturn_ = newName;
}

void
ApiTracer::
endApi() {
    if (!skip_) {
        if (currentType_ != "void") {
            outStream_ << currentType_ << " " << currentReturn_ << " = ";
        }
        outStream_ << currentName_ << "(";
        std::string sep;
        for (const auto& arg : args_) {
            outStream_ << sep << arg;
            sep = ", ";
        }
        outStream_ << ");" << std::endl;
    }
    inApi_ = false;
    args_.clear();
}

}  // namespace api
