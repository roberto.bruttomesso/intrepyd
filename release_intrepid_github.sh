#!/usr/bin/env bash

set -e

TMP_DIR=/tmp/intrepid
INCLUDE_DIR=$TMP_DIR/include
DIR=$(pwd)

# Initializing
rm -fr "$TMP_DIR"

# Checking out repo from github
cd /tmp
git clone git@github.com:formalmethods/intrepid.git

# Updating files in the whitelist
cd "$DIR"
WHITELIST="
CREDITS.md
Dockerfile
Manifest.in
README.md
README.rst
VERSION
libs
app
benchmarks
docker
docs
intrepyd
requirements.txt
setup.cfg
setup.py
.gitignore
"
for i in $WHITELIST
do
  echo "COPYING/UPDATING $i"
  cp -fr "$i" "$TMP_DIR"
done
mkdir -p "$INCLUDE_DIR"
cp intrepid/src/api/Intrepid.h "$INCLUDE_DIR"

# Adding, committing and pushing on github.com
cd "$TMP_DIR"
git add -A
git commit -am "Automatic Update"
git push

# Exiting
cd "$DIR"
